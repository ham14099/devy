﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchiV4
{
    // PowerAttibute myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    // PowerAttibute myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class PowerHour
    {
        public int Hour { get; set; }
        public double HourUnit { get; set; }
    }

    public class PowerDay
    {
        public int Day { get; set; }
        public double DayUnit { get; set; }
        public List<PowerHour> PowerHour { get; set; }
    }

    public class Detail
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public double MonthUnit { get; set; }
        public List<PowerDay> PowerDay { get; set; }
    }

    public class PowerAttibute
    {
        public int MemberID { get; set; }
        public List<Detail> Detail { get; set; }
    }


}
