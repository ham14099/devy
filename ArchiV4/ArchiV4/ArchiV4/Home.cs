﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ArchiV4
{
    public class Home:Frame
    {
        public enum ImgType : int
        {
            HOME = 0,
            HOTEL = 1,
            RES = 2
        }

        public int ClickToHome { get; set; } = 0;

        public int MemberID { get; set; }
        public int Status { get; set; }
        public int PowerOnline { get; set; }
        public int ImageIcon { get; set; }
        public string Name { get; set; }
        public bool PowerConsump { get; set; } = false;
        public int DNDMUR { get; set; } = 0;
        public bool DNDMURVisible { get; set; } = false;
        public event EventHandler ButtonClicked;

        public event EventHandler<int> OnlineHandler;
        public event EventHandler<bool> PowerOnlineHandler;
        public event EventHandler<int> DNDMURChangeHandler;
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Status);
            }
            catch (Exception ex)
            {
                OnOnline(Status);
            }
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }
        
        public void OnPowerOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnPowerOnline(PowerConsump);
            }
            catch (Exception ex)
            {
                OnPowerOnline(PowerConsump);
            }
        }
        public void OnDNDMURChangeHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnDNDMURChange(DNDMUR);
            }
            catch (Exception ex)
            {
                OnDNDMURChange(DNDMUR);
            }
        }
        protected virtual void OnPowerOnline(bool status)
        {
            PowerOnlineHandler?.Invoke(this, status);
        }
        protected virtual void OnDNDMURChange(int status)
        {
            DNDMURChangeHandler?.Invoke(this, status);
        }



        string[] imgList = new string[]
        {
            "home_64x64.png",//home
            "hotel_64x64.png",//hotel
            "condo_64x64.png",//res,condo
        };

        Label nameLabel = new Label
        {
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            TextColor = Color.FromHex("#11376d"),
            FontSize = 14,
            InputTransparent = true,
            Margin = new Thickness(10, 0, 10, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            MaxLines = 1,
            Style = (Style)Application.Current.Resources["FontText"]
        };
        ImageButton btn = new ImageButton
        {
            BackgroundColor = Color.White,
            HeightRequest = In4.ButtonWidth,
            WidthRequest = In4.ButtonWidth,
        };
        Image image = new Image
        {
            //Source = "building128x128.png",
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            Aspect = Aspect.AspectFit,
            //Margin = new Thickness(0, 5, 0, 0),
            WidthRequest = In4.ButtonWidth/2,
            HeightRequest = In4.ButtonWidth / 2
        };
        Frame onlineIFrame = new Frame
        {
            WidthRequest = 10,
            HeightRequest = 10,
            CornerRadius = 5,
            BackgroundColor = Color.FromHex("#3bb44a"),
            InputTransparent = true,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.End,
            Padding = 0,
            Margin = new Thickness(0,10,10,0),
            HasShadow = false,
        };
        Grid grid = new Grid
        {
            BackgroundColor = Color.Transparent,
            HeightRequest = In4.ButtonWidth,
            WidthRequest = In4.ButtonWidth
        };


        public Home()
        {
            //BorderColor = Color.FromHex("#11376d");

            BackgroundColor = Color.Transparent;
            Padding = 0;
            HasShadow = false;
            CornerRadius = 15;
            HeightRequest = In4.ButtonWidth;
            WidthRequest = In4.ButtonWidth;
            VerticalOptions = LayoutOptions.Center;
            HorizontalOptions = LayoutOptions.Center;

            nameLabel.Text = Name;
            btn.Clicked += Btn_Clicked;
            grid.Children.Add(btn);
            grid.Children.Add(image);
            grid.Children.Add(nameLabel);
            grid.Children.Add(onlineIFrame);
            Content = grid;
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetImageIcon(int type)
        {
            image.Source = imgList[type-1];
        }

        public void SetImageIcon(ImgType type)
        {
            image.Source = imgList[(int)type];
        }

        public void SetName(string name)
        {
            nameLabel.Text = name;
        }
         public enum PowerEnable:int
        {
            Online = 1,
            Offline = 0,
            Hidden = 2,

        }
        public void EnablePowerConsumption(bool enable)
        {
            PowerConsump = enable; 
            OnPowerOnlineHandler();
        }
        public void SetDNDMUR(int state)
        {
            DNDMUR = state;
            OnDNDMURChangeHandler();
        }
        public void IsOnline(int online)
        {
            Status = online;
            onlineIFrame.BackgroundColor = online == 1 ? Color.FromHex("#3bb44a") : Color.FromHex("#ff4646");
            btn.BackgroundColor = online == 1 ? Color.White : (Color)Application.Current.Resources["Active"];


            nameLabel.TextColor = online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];


            btn.IsEnabled = online == 1 ? true : false;
            OnOnlineHandler();
        }
    }
}
