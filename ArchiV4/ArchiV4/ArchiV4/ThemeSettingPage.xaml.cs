﻿using Newtonsoft.Json;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.PancakeView;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThemeSettingPage : ContentPage
    {
        int currentTextTheme = 0;
        int currentTheme = 0;
        List<ThemeColorButton> themeColorButtonsList = new List<ThemeColorButton>();
        private SKBitmap bitmap = new SKBitmap(300, 300);
        SKCanvasView canvasView;

        public ThemeSettingPage()
        {
            InitializeComponent();

            Log.Warning("bitmap h", "" + bitmap.Height);
            Log.Warning("bitmap w", "" + bitmap.Width);

            // Load the bitmap from a resource
            string resourceID = "ArchiV4.Media.colorwheel.png";
            Assembly assembly = GetType().GetTypeInfo().Assembly;

            using (Stream stream = assembly.GetManifestResourceStream(resourceID))
            {
                bitmap = SKBitmap.Decode(stream);
            }

            // Create the SKCanvasView and set the PaintSurface handler
            canvasView = new SKCanvasView() { Scale = 1, HeightRequest = bitmap.Height / In4.ScreenDensity, WidthRequest = bitmap.Width / In4.ScreenDensity, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };
            canvasView.EnableTouchEvents = true;
            canvasView.PaintSurface += OnCanvasViewPaintSurface;
            canvasView.Touch += CanvasView_Touch;
            ColorPickPanel.Children.Add(canvasView);
            ColorPickerArea.Border = new Border { Color = Color.FromHex("#494949"), DrawingStyle = BorderDrawingStyle.Outside, Thickness = 1 };
            ColorPickerArea.CornerRadius = canvasView.HeightRequest / 2;
            ColorPickerArea.HeightRequest = canvasView.HeightRequest;
            ColorPickerArea.WidthRequest = canvasView.WidthRequest;

            currentTheme = In4.appSetting.theme;
            currentTextTheme = In4.appSetting.texttheme;
            for (int i = 0; i < In4.ColorsTemplate.Count; i++)
            {
                ThemeColorButton themeColorButton = new ThemeColorButton { TabIndex = i, Border = new Border { Thickness = (currentTheme == i ? 4 : 1), Color = Color.FromHex("#505050"), DrawingStyle = BorderDrawingStyle.Outside } };
                themeColorButton.SetColor(In4.ColorsTemplate[i][0], In4.ColorsTemplate[i][1], In4.ColorsTemplate[i][2], In4.ColorsTemplate[i][3], In4.ColorsTemplate[i][4], In4.ColorsTemplate[i][5]);
                themeColorButton.ButtonClicked += ThemeColorButton_ButtonClicked;
                themeColorButtonsList.Add(themeColorButton);
                ColorPalate.Children.Add(themeColorButtonsList[i]);
            }

            if (Device.OS == TargetPlatform.iOS)
            {
                container.Margin = new Thickness(0, 20, 0, 0);
            }

            if (In4.appSetting.texttheme == 0)
            {
                darkBtn.BackgroundColor = Color.FromHex("#505050");
                darkBtn.TextColor = Color.White;
                lightBtn.BackgroundColor = Color.White;
                lightBtn.TextColor = Color.FromHex("#505050");
            }
            else
            {
                lightBtn.BackgroundColor = Color.FromHex("#505050");
                lightBtn.TextColor = Color.White;
                darkBtn.BackgroundColor = Color.White;
                darkBtn.TextColor = Color.FromHex("#505050");
            }
        }

        private void CanvasView_Touch(object sender, SKTouchEventArgs e)
        {
            switch (e.ActionType)
            {
                case SKTouchAction.Pressed:
                    /*if (!inProgressPaths.ContainsKey(args.Id))
                    {
                        SKPath path = new SKPath();
                        path.MoveTo(ConvertToPixel(args.Location));
                        inProgressPaths.Add(args.Id, path);
                        canvasView.InvalidateSurface();
                    }*/
                    try
                    {
                        //var aa = (sender as SKCanvasView);
                        var loc = ConvertToPixel(e.Location);
                        Log.Warning("Pressed X", "" + loc.X);
                        Log.Warning("Pressed Y", "" + loc.Y);
                        SetPicker((double)loc.X,(double)loc.Y);
                        int rr = bitmap.GetPixel((int)loc.X, (int)loc.Y).Red;
                        int gg = bitmap.GetPixel((int)loc.X, (int)loc.Y).Green;
                        int bb = bitmap.GetPixel((int)loc.X, (int)loc.Y).Blue;
                        Log.Warning("Pressed R", "" + rr);
                        Log.Warning("Pressed G", "" + gg);
                        Log.Warning("Pressed B", "" + bb);
                        pick.BackgroundColor = Color.FromRgb(rr, gg, bb);
                        canvasView.InvalidateSurface();
                    }
                    catch (Exception error)
                    {

                        Log.Warning("error CanvasView_Touch", "" + error);
                    }
                    break;
                case SKTouchAction.Moved:
                    try
                    {
                        //var aa = (sender as SKCanvasView);
                        var loc = ConvertToPixel(e.Location);
                        Log.Warning("Moved X", "" + loc.X);
                        Log.Warning("Moved Y", "" + loc.Y);
                        SetPicker((double)loc.X, (double)loc.Y);
                        int rr = bitmap.GetPixel((int)loc.X, (int)loc.Y).Red;
                        int gg = bitmap.GetPixel((int)loc.X, (int)loc.Y).Green;
                        int bb = bitmap.GetPixel((int)loc.X, (int)loc.Y).Blue;
                        Log.Warning("Moved R", "" + rr);
                        Log.Warning("Moved G", "" + gg);
                        Log.Warning("Moved B", "" + bb);
                        pick.BackgroundColor = Color.FromRgb(rr, gg, bb);
                        canvasView.InvalidateSurface();
                    }
                    catch (Exception error)
                    {

                        Log.Warning("error CanvasView_Touch", "" + error);
                    }
                    break;
                case SKTouchAction.Released:
                    /*if (inProgressPaths.ContainsKey(args.Id))
                    {
                        completedPaths.Add(inProgressPaths[args.Id]);
                        inProgressPaths.Remove(args.Id);
                        canvasView.InvalidateSurface();
                    }*/

                    break;
            }
        }
        SKPoint ConvertToPixel(SKPoint pt)
        {
           /* Log.Warning(" pt.X", "" + pt.X);
            Log.Warning(" pt.Y", "" + pt.Y);
            Log.Warning(" canvasView.CanvasSize.Width", "" + canvasView.CanvasSize.Width);
            Log.Warning(" canvasView.CanvasSize.Height", "" + canvasView.CanvasSize.Height);
            Log.Warning(" canvasView.Width", "" + canvasView.Width);
            Log.Warning(" canvasView.Height", "" + canvasView.Height);*/
            /*return new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
                               (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));*/
            return pt;
        }

        private void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear();

            float x = (info.Width - bitmap.Width) / 2;
            float y = (info.Height - bitmap.Height) / 2;

            canvas.DrawBitmap(bitmap, x, y);//x,y

            //Log.Warning("info Width", ""+ info.Width);
            //Log.Warning("info Height", ""+ info.Height);
            //Log.Warning("bitmap Width", ""+ bitmap.Width);
            //Log.Warning("bitmap Height", ""+ bitmap.Height);
        }
        void SetPicker(double x, double y)
        {

            pick.TranslationX = (x / In4.ScreenDensity) - pick.Width / 2;
            pick.TranslationY = (y / In4.ScreenDensity) - pick.Width / 2;
        }

        private void ThemeColorButton_ButtonClicked(object sender, EventArgs e)
        {
            themeColorButtonsList[currentTheme].SetPress(false);
            var v = sender as ThemeColorButton;
            //DisplayAlert("0="+ In4.ColorsTemplate[v.TabIndex][0], "1="+In4.ColorsTemplate[v.TabIndex][1], "2="+In4.ColorsTemplate[v.TabIndex][2]);
            currentTheme = v.TabIndex;
            v.SetPress(true);
            headerPreview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][0]);
            bgHeaderPreview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][1]);
            bodyPrview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][2]);
            header2Preview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][0]);
            body2Prview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][2]);
            addIconFramePreview.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][0]);
            h3.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][3]);
            rFrame1.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][3]);
            l5Frame.BackgroundColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][3]);

            //n1.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //n2.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            n3.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);

            r1.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //r2.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //r3.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);


            //l1.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //l2.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //l3.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //l4.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            l5.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);
            //l6.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);

            roomName.TextColor = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);

            /*Application.Current.Resources["Header"] = Color.FromHex(In4.ColorsTemplate[v.TabIndex][0]);
            Application.Current.Resources["Body"] = Color.FromHex(In4.ColorsTemplate[v.TabIndex][1]);
            Application.Current.Resources["Active"] = Color.FromHex(In4.ColorsTemplate[v.TabIndex][2]);
            Application.Current.Resources["TextItemActive"] = Color.FromHex(In4.ColorsTemplate[v.TabIndex][3]);
            Application.Current.Resources["TextItem"] = Color.FromHex(In4.ColorsTemplate[v.TabIndex][4]);*/
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private void darkBtn_Clicked(object sender, EventArgs e)
        {
            /*Application.Current.Resources["GearImg"] = "gear_light64x64.png";
            Application.Current.Resources["BackImg"] = "back64x64.png";
            Application.Current.Resources["AddImg"] = "plus_64x64.png";
            Application.Current.Resources["TextHeader"] = Color.FromHex("#ffffff");*/

            darkBtn.BackgroundColor = Color.FromHex("#505050");
            darkBtn.TextColor = Color.White;
            lightBtn.BackgroundColor = Color.White;
            lightBtn.TextColor = Color.FromHex("#505050");

            cat.TextColor = Color.FromHex("#ffffff");
            settingIconPreview.Source = "gear_light64x64.png";
            addIconPreview.Source = "plus_64x64.png";
            backIconPreview.Source = "back64x64.png";
            homeNamePreview.TextColor = Color.FromHex("#ffffff");
            currentTextTheme = 0;

        }

        private void lightBtn_Clicked(object sender, EventArgs e)
        {
            /*Application.Current.Resources["GearImg"] = "gear_dark64x64.png";
            Application.Current.Resources["BackImg"] = "back_dark64x64.png";
            Application.Current.Resources["AddImg"] = "plus_256x256.png";
            Application.Current.Resources["TextHeader"] = Color.FromHex("#505050");*/

            lightBtn.BackgroundColor = Color.FromHex("#505050");
            lightBtn.TextColor = Color.White;
            darkBtn.BackgroundColor = Color.White;
            darkBtn.TextColor = Color.FromHex("#505050");

            cat.TextColor = Color.FromHex("#505050");
            settingIconPreview.Source = "gear_dark64x64.png";
            addIconPreview.Source = "plus_256x256.png";
            backIconPreview.Source = "back_dark64x64.png";
            homeNamePreview.TextColor = Color.FromHex("#505050");
            currentTextTheme = 1;
        }

        private void saveBtn_Clicked(object sender, EventArgs e)
        {
            SaveCurrentSetting();
        }

        public async void SaveCurrentSetting()
        {
            var filenameSetting = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomesetting.txt");

            string json = @"{'settingv':'" + In4.SettingVersion + "','theme':" + currentTheme + " ,'texttheme': " + currentTextTheme + "}";
            Setting setting = JsonConvert.DeserializeObject<Setting>(json);

            Log.Warning("savefile", "savefile");
            Log.Warning("savefile", "settingv:" + setting.settingv);
            Log.Warning("savefile", "theme:" + setting.theme);
            Log.Warning("savefile", "texttheme:" + setting.texttheme);
            In4.InitSetting(setting);

            string content = json.ToString();
            File.WriteAllText(filenameSetting, content, Encoding.UTF8);
            await DisplayAlert("Save", "Restart application to take more effect", "Ok");
            await Navigation.PopModalAsync();
        }
        
    }
}