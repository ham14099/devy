﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ArchiV4
{
    public static class In4
    { 
        public static string AppVersion = "1.2.5";
        public static string RoomName { get; set; }
        public static string SettingVersion { get; set; }
        public static bool ShowDebug { get; set; }
        public static double ScreenWidth { get; set; }
        public static double ScreenDensity { get; set; }
        public static DeviceIdiom DevicePlatform { get; set; }
        public static TargetPlatform DeviceOS { get; set; }
        public static double OneOfPercent { get; set; }
        public static double SceneOneOfPercent { get; set; }
        public static double ButtonWidth { get; set; }
        public static double TriButtonWidth { get; set; }
        public static double SceneButtonWidth { get; set; }
        public static double IconSize { get; set; }
        public static double CurtainSize { get; set; }
        public static double FontSizeName { get; set; }
        public static double FontSizeSpeed { get; set; }
        public static double FontSizeLight { get; set; }
        public static double FontSizeTemp { get; set; }
        public static double QuickPowerSize { get; set; }
        public static int maxForRow = 2;
        public static int sceneMaxForRow = 3;
        private static int gridSpacing = 10;

        public static Setting appSetting;

        public static List<string[]> ColorsTemplate = new List<string[]>();

        public static void In4Init()
        {
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            var width = mainDisplayInfo.Width;
            var density = mainDisplayInfo.Density;
            ScreenWidth = width;
            ScreenDensity = density;

            DevicePlatform = DeviceInfo.Idiom;
            DeviceOS = Device.OS;
            //--- init theme color set template palate ---

            /*
                1 - [Header]
                2 - [Body]
                3 - [Active] [home offline]
                4 - [TextItemActive] [home offline]
                //5 - [TextItem(nonActive)] [home online text]
                5 - [TextItem(room Name)]
            */
            //----------------------------------- 1 ------- 2 ------- 3 -------- 4 -------- 5 -----------
            ColorsTemplate.Add(new string[] { "#c9a97f", "#c9a97f", "#f6eee0", "#e8d9c4", "#7d7462", "#7d7462" });
            ColorsTemplate.Add(new string[] { "#f8f8f8", "#ffffff", "#f0f0f0", "#e5e5e5", "#505050", "#505050" });
            ColorsTemplate.Add(new string[] { "#f8f8f8", "#ffffff", "#f0f0f0", "#45443f", "#808080", "#45443f" });
            ColorsTemplate.Add(new string[] { "#40b0b5", "#40b0b5", "#a8e1d3", "#e3b6af", "#ffffff", "#11376d" });
            ColorsTemplate.Add(new string[] { "#66b2e4", "#66b2e4", "#405592", "#f49b45", "#ffffff", "#f49b45" });
            ColorsTemplate.Add(new string[] { "#347798", "#347798", "#6296ba", "#45443f", "#ffddaa", "#45443f" });
            ColorsTemplate.Add(new string[] { "#c2222c", "#c2222c", "#a5dfc6", "#20565c", "#ffffff", "#20565c" });
            ColorsTemplate.Add(new string[] { "#e58874", "#e58874", "#fc9b86", "#564a4a", "#ffffff", "#564a4a" });
            ColorsTemplate.Add(new string[] { "#03989e", "#03989e", "#c4d7d1", "#f0bc68", "#03989e", "#03989e" });
            //---------------------------------------------------------------------------------------------
            if (DeviceInfo.Idiom == DeviceIdiom.Tablet)
            {
                gridSpacing = 10;
                maxForRow = 5;
                ButtonWidth = ((ScreenWidth / ScreenDensity) - (((maxForRow + 1) * gridSpacing) + 20)) / maxForRow;
                OneOfPercent = (double)ButtonWidth / 100;

                sceneMaxForRow = 5;
                SceneButtonWidth = ((ScreenWidth / ScreenDensity) - (((sceneMaxForRow - 1) * gridSpacing) + 20)) / sceneMaxForRow;
                SceneOneOfPercent = (double)SceneButtonWidth / 100;

                //Application.Current.Resources["IndicatorSize"] = 2.5;
            }
            else
            {
                gridSpacing = 10;
                ButtonWidth = ((ScreenWidth / ScreenDensity) - (((maxForRow + 1) * gridSpacing) + 20)) / maxForRow;
                OneOfPercent = (double)ButtonWidth / 100;

                SceneButtonWidth = ((ScreenWidth / ScreenDensity) - (((sceneMaxForRow - 1) * gridSpacing) + 20)) / sceneMaxForRow;
                SceneOneOfPercent = (double)SceneButtonWidth / 100;

                //Application.Current.Resources["IndicatorSize"] = 0.5;
            }

            IconSize = OneOfPercent * 35;
            CurtainSize = 70;
            FontSizeName = OneOfPercent * 14;
            FontSizeSpeed = OneOfPercent * 16;
            FontSizeTemp = OneOfPercent * 26;
            QuickPowerSize = OneOfPercent * 40;
            FontSizeLight = OneOfPercent * 12;
        }

        public static void InitSetting(Setting setting)
        {
            appSetting = setting;
            Application.Current.Resources["Header"] = Color.FromHex(ColorsTemplate[setting.theme][0]);
            Application.Current.Resources["BGHeader"] = Color.FromHex(ColorsTemplate[setting.theme][1]);
            Application.Current.Resources["Body"] = Color.FromHex(ColorsTemplate[setting.theme][2]);
            Application.Current.Resources["Active"] = Color.FromHex(ColorsTemplate[setting.theme][3]);
            Application.Current.Resources["TextItemActive"] = Color.FromHex(ColorsTemplate[setting.theme][4]);
            Application.Current.Resources["TextItem"] = Color.FromHex(ColorsTemplate[setting.theme][5]);

            if (setting.texttheme == 0)//--- dark theme (light text)
            {
                Application.Current.Resources["GearImg"] = "gear_light64x64.png";
                Application.Current.Resources["BackImg"] = "back64x64.png";
                Application.Current.Resources["ElecImg"] = "power_light64x64.png";
                Application.Current.Resources["AddImg"] = "plus_64x64.png";
                Application.Current.Resources["TextHeader"] = Color.FromHex("#ffffff");
            }
            else if (setting.texttheme == 1)//--- dark theme (light text)
            {
                Application.Current.Resources["GearImg"] = "gear_dark64x64.png";
                Application.Current.Resources["BackImg"] = "back_dark64x64.png";
                Application.Current.Resources["ElecImg"] = "power_dark64x64.png";
                Application.Current.Resources["AddImg"] = "plus_256x256.png";
                Application.Current.Resources["TextHeader"] = Color.FromHex("#505050");
            }            
        }
    }
}
