﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new Startup();

            MainPage = new NavigationPage(new Startup())
            {
                BarTextColor = Color.FromHex("#494949"),
                BarBackgroundColor = Color.White
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
            if (!Provider.Client.IsConnected)
            {
                if (!Provider.Connect())
                {
                    //Environment.Exit(0);
                    Log.Warning("OnResume", "Exit");
                }
                else
                {
                    Log.Warning("OnResume", "Continue");
                }
            }
            
        }
    }
}
