﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CurtainPopupControl : PopupPage
    {
        Curtain Curtains;
        public CurtainPopupControl(Curtain curtain)
        {
            InitializeComponent();
            Curtains = curtain;
            curName.Text = curtain.Name;
            SetStatus1(curtain.Status1);
            SetStatus2(curtain.Status2);
            curtain.Curtain1StatusHandler += Curtain_Curtain1StatusHandler;
            curtain.Curtain2StatusHandler += Curtain_Curtain2StatusHandler;
            curtain.OnlineHandler += DevicesOnlineHandler;
        }
        private void DevicesOnlineHandler(object sender, int e)
        {
            controlPanel.IsEnabled = e == 1 ? true : false;

            cur1Stop.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");
            cur1Open.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");
            cur1Close.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");

            cur2Stop.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");
            cur2Open.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");
            cur2Close.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d9d9d9");

            switch (Curtains.Status1)
            {
                case 0: cur1Stop.BackgroundColor = Color.FromHex("#f0f0f0"); break;
                case 1: cur1Open.BackgroundColor = Color.FromHex("#f0f0f0"); break;
                case 2: cur1Close.BackgroundColor = Color.FromHex("#f0f0f0"); break;
            }
            switch (Curtains.Status2)
            {
                case 0: cur2Stop.BackgroundColor = Color.FromHex("#f0f0f0"); break;
                case 1: cur2Open.BackgroundColor = Color.FromHex("#f0f0f0"); break;
                case 2: cur2Close.BackgroundColor = Color.FromHex("#f0f0f0"); break;
            }

            //mainPancake.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d0d0d0");
        }
        private void Curtain_Curtain2StatusHandler(object sender, int e)
        {
            SetStatus2(e);
        }

        private void Curtain_Curtain1StatusHandler(object sender, int e)
        {
            SetStatus1(e);
        }

        private void SetStatus1(int status1)
        {
            Curtains.Status1 = status1;
            switch (status1)
            {
                case 0:
                    cur1Stop.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    cur1Open.BackgroundColor = Color.White;
                    cur1Close.BackgroundColor = Color.White;
                    break;
                case 1:
                    cur1Stop.BackgroundColor = Color.White;
                    cur1Open.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    cur1Close.BackgroundColor = Color.White;
                    break;
                case 2:
                    cur1Stop.BackgroundColor = Color.White;
                    cur1Open.BackgroundColor = Color.White;
                    cur1Close.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    break;
            }
        }
        private void SetStatus1AndSend(int status1)
        {
            SetStatus1(status1);
            Provider.SendDeviceControl(Curtains.MemberID, Curtains.DeviceID, 1, Curtains.Status1);
        }
        private void SetStatus2(int status2)
        {
            Curtains.Status2 = status2;
            switch (status2)
            {
                case 0:
                    cur2Stop.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    cur2Open.BackgroundColor = Color.White;
                    cur2Close.BackgroundColor = Color.White;
                    break;
                case 1:
                    cur2Stop.BackgroundColor = Color.White;
                    cur2Open.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    cur2Close.BackgroundColor = Color.White;
                    break;
                case 2:
                    cur2Stop.BackgroundColor = Color.White;
                    cur2Open.BackgroundColor = Color.White;
                    cur2Close.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    break;
            }
        }
        private void SetStatus2AndSend(int status2)
        {
            SetStatus2(status2);
            Provider.SendDeviceControl(Curtains.MemberID, Curtains.DeviceID, 2, Curtains.Status2);
        }

        private async void closeBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void cur1Close_Clicked(object sender, EventArgs e)
        {
            SetStatus1AndSend(2);
        }

        private void cur1Stop_Clicked(object sender, EventArgs e)
        {
            SetStatus1AndSend(0);
        }

        private void cur1Open_Clicked(object sender, EventArgs e)
        {
            SetStatus1AndSend(1);
        }

        private void cur2Close_Clicked(object sender, EventArgs e)
        {
            SetStatus2AndSend(2);
        }

        private void cur2Stop_Clicked(object sender, EventArgs e)
        {
            SetStatus2AndSend(0);
        }

        private void cur2Open_Clicked(object sender, EventArgs e)
        {
            SetStatus2AndSend(1);
            
        }
    }
}