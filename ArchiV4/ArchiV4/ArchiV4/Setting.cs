﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchiV4
{
    public class Setting
    {
        public string settingv { get; set; }
        public int theme { get; set; }
        public int texttheme { get; set; }
    }
}
