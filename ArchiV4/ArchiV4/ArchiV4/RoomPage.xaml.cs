﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoomPage : ContentPage
    {
        public RoomPage(int index)
        {
            InitializeComponent();
            roomname.Text = Provider.Homes[index].Name;
            int col = 0, row = 0, k = 0, max = 2;
            max = (DeviceInfo.Idiom == DeviceIdiom.Tablet ? 4 : 2);
            for (int columnC = 0; columnC < max; columnC++)
            {
                gridlayout.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (k = 0; k < Provider.ROOM_LIST[index].Count; k++)
            {
                if (k % max == 0 && k > 1)
                {
                    row++;
                    gridlayout.RowDefinitions.Add(new RowDefinition());
                }

                if (col == max)
                {
                    col = 0;
                }


                Image img = new Image
                {
                    Source = "living128x128.png",
                    //HeightRequest = (In4.ButtonWidth/2),
                    InputTransparent = true,
                    Margin = new Thickness(5, 10, 0, 0),
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center,
                    Aspect = Aspect.AspectFit,
                    HeightRequest = (In4.ButtonWidth / 2)
                };
                ImageButton btn = new ImageButton
                {
                    BackgroundColor = Color.White,
                    HeightRequest = (In4.ButtonWidth),
                    TabIndex = k
                };
                Label roomname = new Label
                {
                    Text = Provider.ROOM_LIST[index][k].Name,
                    InputTransparent = true,
                    Margin = new Thickness(10, 0, 10, 10),
                    Style = (Style)Application.Current.Resources["FontText"],
                    LineBreakMode = LineBreakMode.TailTruncation,
                    MaxLines = 2,
                    FontSize = 14,
                    VerticalOptions = LayoutOptions.End,
                    HorizontalOptions = LayoutOptions.Center,
                    TextColor = Color.FromHex("#16a7dc")
                };
                Frame frame = new Frame
                {
                    BackgroundColor = Color.White,
                    Padding = 0,
                    CornerRadius = 5,
                    HasShadow = false
                };
                Grid grid = new Grid
                {
                    BackgroundColor = Color.Transparent,
                    HeightRequest = (In4.ButtonWidth)
                };
                btn.Clicked += async (sender, arg) =>
                {
                    await Navigation.PushModalAsync(new TabbedPage1(Provider.Homes[index], btn.TabIndex));
                    //DisplayAlert("Alert", "Room : "+ Provider.ROOM_LIST[index][btn.TabIndex].Name, "OK");
                };
                grid.Children.Add(btn);
                grid.Children.Add(img);
                grid.Children.Add(roomname);
                frame.Content = grid;
                gridlayout.Children.Add(frame, col, row);
                col++;
            }
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}