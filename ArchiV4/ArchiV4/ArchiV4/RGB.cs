﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;
using GradientStop = Xamarin.Forms.PancakeView.GradientStop;
using GradientStopCollection = Xamarin.Forms.PancakeView.GradientStopCollection;
namespace ArchiV4
{
    public class RGB : PancakeView
    {
        public int MemberID { get; set; }
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public ARGB RGBA { get; set; }
        public int Room { get; set; }

        public event EventHandler ButtonClicked;
        public event EventHandler QuickClicked;

        //--- EventHandler Delegate -----------------------------------------
        public event EventHandler<ARGB> RGBHandler;
        public event EventHandler<int> RGBSatusHandler;
        public event EventHandler<int> OnlineHandler;
        //public event EventHandler<Aircondition> ProcessCompleted;
        public void OnRGBHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Temperature]");
                OnRGB(RGBA);
            }
            catch (Exception ex)
            {
                OnRGB(RGBA);
            }
        }
        public void OnRGBStatusHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnRGBStatus(Status);
            }
            catch (Exception ex)
            {
                OnRGBStatus(Status);
            }
        }
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }
        protected virtual void OnRGB(ARGB argb)
        {
            RGBHandler?.Invoke(this, argb);
        }
        protected virtual void OnRGBStatus(int status)
        {
            RGBSatusHandler?.Invoke(this, status);
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }

        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        /*Image bgBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth,
            WidthRequest = In4.SceneButtonWidth,
            Source = "btnsquare128x128_nin.png",
            Aspect = Aspect.AspectFill,
            InputTransparent = true,
            BackgroundColor = Color.Transparent,
        };*/


        PancakeView colorPreview = new PancakeView
        {
            CornerRadius = In4.SceneOneOfPercent * 22,
            WidthRequest = In4.SceneOneOfPercent * 44,
            HeightRequest = In4.SceneOneOfPercent * 44,
            BackgroundColor = Color.White,
            InputTransparent = true,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
           // Margin = new Thickness(0, -10, 0, 0)
            //Margin = new Thickness(20, (In4.SceneOneOfPercent * 40), 0, 0),
        };
       /* PancakeView colorPreviewBG = new PancakeView
        {
            CornerRadius = 25,
            WidthRequest = 50,
            HeightRequest = 50,
            BackgroundColor = Color.White,
            InputTransparent = true,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            
            //Margin = new Thickness(20, (In4.SceneOneOfPercent * 40), 0, 0),
        };*/
        Image bgPreview = new Image
        {
            HeightRequest = In4.SceneOneOfPercent * 50,
            WidthRequest = In4.SceneOneOfPercent * 50,
            Source = "colorwheel64x64.png",
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            //Margin = new Thickness(0, -10, 0, 0)
        };
        Label rgbname = new Label
        {
            Text = "",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.FromHex("#494949"),
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Grid quickGrid = new Grid
        {
            RowSpacing = 0,
            ColumnSpacing = 0,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.End,
            WidthRequest = 30,
            HeightRequest = 30,
        };

        PancakeView quickPancake = new PancakeView
        {
            Padding = 0,
            BackgroundColor = Color.FromHex("#56efb1"),
            HeightRequest = 10,
            WidthRequest = 20,
            CornerRadius = 5,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true
        };

        ImageButton quickBtn = new ImageButton
        {
            BackgroundColor = Color.Transparent,
            HeightRequest = 30,
            WidthRequest = 30,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };

        public RGB()
        {
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;

            colorPreview.Border = new Border
            {
                Color = Color.FromHex("#FFFFFF"),
                Thickness = 2,
                DrawingStyle = BorderDrawingStyle.Inside
            };
            /*colorPreviewBG.Border = new Border
            {
                Color = Color.FromHex("#b0b0b0"),
                Thickness = 5,
                DrawingStyle = BorderDrawingStyle.Outside,
                
            };
            colorPreviewBG.BackgroundGradientStartPoint = new Point(0, 0);
            colorPreviewBG.BackgroundGradientEndPoint = new Point(1, 1);
            colorPreviewBG.BackgroundGradientStops = new GradientStopCollection()
            {
                new GradientStop { Color = Color.FromHex("#b0b0b0"), Offset = 0 },
                new GradientStop { Color = Color.FromHex("#707070"), Offset = 0.4f },
                new GradientStop { Color = Color.FromHex("#505050"), Offset = 0.6f },
                new GradientStop { Color = Color.FromHex("#707070"), Offset = 0.7f },
                new GradientStop { Color = Color.FromHex("#b0b0b0"), Offset = 1 },
            };*/
            //colorPreviewBG.BackgroundGradientAngle = 270.0;
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            //gridFrm.Children.Add(bgBtn);
            gridFrm.Children.Add(bgPreview);
            //gridFrm.Children.Add(colorPreviewBG);
            gridFrm.Children.Add(colorPreview);

            gridFrm.Children.Add(rgbname);

            quickBtn.Clicked += QuickBtn_Clicked;
            quickGrid.Children.Add(quickBtn);
            quickGrid.Children.Add(quickPancake);
            gridFrm.Children.Add(quickGrid);


            Content = gridFrm;
        }

        private void QuickBtn_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = QuickClicked;
            handler?.Invoke(this, e);
        }

        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            rgbname.Text = name;

        }
        public void SetStatus(int status)
        {
            Status = status;

            OnRGBStatusHandler();
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
        public void SetRGB(int rgb)
        {
            ARGB argb = new ARGB( rgb);
            RGBA = argb.GetRGB();
            colorPreview.BackgroundColor = Color.FromRgb(RGBA.R, RGBA.G, RGBA.B);
            //SetName(""+ RGBA.R+" "+ RGBA.G+" "+ RGBA.B+" ");
            OnRGBHandler();
        }
        public void SetOnline(int online)
        {
            Online = online;
            //buttonFrm.BackgroundColor = Color.FromHex("#E8D9C4");
            buttonFrm.BackgroundColor = Online == 1 ? Color.White : (Color)Application.Current.Resources["Active"];
            rgbname.TextColor = Online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];
            gridFrm.IsEnabled = Online == 1 ? true : false;

            OnOnlineHandler();
        }

    }
    public class ARGB{

        public int R { get; set; } = 0;
        public int G { get; set; } = 0;
        public int B { get; set; } = 0;
        public int A { get; set; } = 0;
        public ARGB(int rgb)
        {
            int r, g, b;
            r = (rgb / 1000000);
            R = r - 100;
            g = (rgb % 1000000)/1000;
            G = g - 100;
            b = rgb % 1000;
            B = b - 100;
        } 
        public ARGB()
        {

        }

        public ARGB GetRGB()
        {
            return new ARGB {R = this.R,G = this.G,B = this.B };
        }
    }
}
