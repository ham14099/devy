﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace ArchiV4
{
    public class Power
    {
        public int MemberID { get; set; }
        //public int DeviceID { get; set; }
        public int Online { get; set; }
        public int DeviceID { get; set; }

        public double Voltage { get; set; }
        public double Current { get; set; }
        public double Watt { get; set; }
        public double Energy { get; set; }
        public double PowerFactor { get; set; }
        public double Frequency { get; set; }




        public double[] EnergyTime = new double[] 
        { 
            0,0,0,0,0,0,
            0,0,0,0,0,0,
            0,0,0,0,0,0,
            0,0,0,0,0,0
        };

        //--- EventHandler Delegate -----------------------------------------
        public event EventHandler<int> OnlineHandler;
        public event EventHandler<double> VoltageHandler;
        public event EventHandler<double> CurrentHandler;
        public event EventHandler<double> WattHandler;
        public event EventHandler<double> EnergyHandler;
        public event EventHandler<double> PowerFactorHandler;
        public event EventHandler<double> FrequencyHandler;
        //public event EventHandler<Aircondition> ProcessCompleted;
        private void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Temperature]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }private void OnVoltageHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Temperature]");
                OnVoltage(Voltage);
            }
            catch (Exception ex)
            {
                OnVoltage(Voltage);
            }
        }
        private void OnCurrentHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Speed]");
                OnCurrent(Current);
            }
            catch (Exception ex)
            {
                OnCurrent(Current);
            }
        }
        private void OnWattHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnWatt(Watt);
            }
            catch (Exception ex)
            {
                OnWatt(Watt);
            }
        }
        private void OnEnergyHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnEnergy(Energy);
            }
            catch (Exception ex)
            {
                OnEnergy(Energy);
            }
        }
        private void OnPowerFactorHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnPowerFactor(PowerFactor);
            }
            catch (Exception ex)
            {
                OnPowerFactor(PowerFactor);
            }
        }
        private void OnFrequencyHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnFrequency(Frequency);
            }
            catch (Exception ex)
            {
                OnFrequency(Frequency);
            }
        }
        protected virtual void OnOnline(int Onlines)
        {
            OnlineHandler?.Invoke(this, Onlines);
        }
        protected virtual void OnVoltage(double Voltage)
        {
            VoltageHandler?.Invoke(this, Voltage);
        }
        protected virtual void OnCurrent(double Current)
        {
            CurrentHandler?.Invoke(this, Current);
        }
        protected virtual void OnWatt(double Watt)
        {
            WattHandler?.Invoke(this, Watt);
        }
        protected virtual void OnEnergy(double energys)
        {
            EnergyHandler?.Invoke(this, energys);
        }
        protected virtual void OnPowerFactor(double PowerFactor)
        {
            PowerFactorHandler?.Invoke(this, PowerFactor);
        }
        protected virtual void OnFrequency(double Frequencys)
        {
            FrequencyHandler?.Invoke(this, Frequencys);
        }


        public void SetOnline(int online)
        {
            Online = online;
            OnOnlineHandler();
        }
        public void SetVoltage(double voltage)
        {
            Voltage = voltage/10;

            OnVoltageHandler();
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
        public void SetCurrent(double current)
        {
            Current = current/1000;
            OnCurrentHandler();
        }
        public void SetWatt(double watt)
        {
            Watt = watt/10;

            OnWattHandler();
        }
        
        public void SetEnergy(double energy)
        {
            Energy = energy / 100;
            OnEnergyHandler();
        }
        public void SetPowerFactor(double powerFactor)
        {
            PowerFactor = powerFactor/100;

            OnPowerFactorHandler();
        }
        public void SetFrequency(double frequency)
        {
            Frequency = frequency/10;

            OnFrequencyHandler();
        }

    }
}
