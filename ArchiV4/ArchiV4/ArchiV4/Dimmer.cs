﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class Dimmer: PancakeView
    {
        public int MemberID { get; set; }
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Dimvalue { get; set; }
        public int Room { get; set; }
        public event EventHandler ButtonClicked;
        public event EventHandler QuickClicked;

        public event EventHandler<int> DimChangeHandler;
        public void OnDimChangeHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnDimChange(Dimvalue);
            }
            catch (Exception ex)
            {
                OnDimChange(Dimvalue);
            }
        }
        protected virtual void OnDimChange(int dim)
        {
            DimChangeHandler?.Invoke(this, dim);
        }

        public event EventHandler<int> OnlineHandler;
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }
        public void SetOnline(int online)
        {
            Online = online;
            buttonFrm.BackgroundColor = Online == 1 ? Color.White : (Color)Application.Current.Resources["Active"];
            lightname.TextColor = Online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];
            gridFrm.IsEnabled = Online == 1 ? true : false;
            OnOnlineHandler();
        }
        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        Grid bulbGrid = new Grid
        {
            HeightRequest = In4.SceneButtonWidth / 2,
            WidthRequest = In4.SceneButtonWidth / 2,
            InputTransparent = true,
            //Margin = new Thickness(0, 20, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.End,
        };
        Image bulbBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth / 3,
            WidthRequest = In4.SceneButtonWidth / 3,
            Source = "bulb_64x64.png",
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(0, 10, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };
        Image bulbLightBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth / 3,
            WidthRequest = In4.SceneButtonWidth / 3,
            Source = "lightbulb_64x64.png",
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(0, 10, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };
        Image lightAuraBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth,
            WidthRequest = In4.SceneButtonWidth,
            Source = "light_aura_64x64.png",
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(0, 0, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };

        Label dimlbl = new Label
        {
            Text = "0%",
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
            InputTransparent = true,
            Margin = new Thickness(15, 15, 0, 0),
            TextColor = Color.FromHex("#ffc100"),
            FontSize = 24,
            Style = (Style)Application.Current.Resources["FontText"],
        };
        Label lightname = new Label
        {
            Text = "Name",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.FromHex("#494949"),
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Grid quickGrid = new Grid
        {
            RowSpacing = 0,
            ColumnSpacing = 0,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.End,
            WidthRequest = 30,
            HeightRequest = 30,
        };

        PancakeView quickPancake = new PancakeView
        {
            Padding = 0,
            BackgroundColor = Color.FromHex("#56efb1"),
            HeightRequest = 10,
            WidthRequest = 20,
            CornerRadius = 5,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true
        };

        ImageButton quickBtn = new ImageButton
        {
            BackgroundColor = Color.Transparent,
            HeightRequest = 30,
            WidthRequest = 30,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };

        public Dimmer()
        {
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            bulbGrid.Children.Add(lightAuraBtn);
            bulbGrid.Children.Add(bulbBtn);
            bulbGrid.Children.Add(bulbLightBtn);
            gridFrm.Children.Add(bulbGrid);
            gridFrm.Children.Add(dimlbl);
            gridFrm.Children.Add(lightname);

            quickBtn.Clicked += QuickBtn_Clicked; ;
            quickGrid.Children.Add(quickBtn);
            quickGrid.Children.Add(quickPancake);
            gridFrm.Children.Add(quickGrid);
            Content = gridFrm;

        }

        private void QuickBtn_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = QuickClicked;
            handler?.Invoke(this, e);
        }

        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            lightname.Text = name;

        }
        public void SetDim(int dim)
        {
            Dimvalue = dim;
            dimlbl.Text = dim + "%";
            if (dim>0)
            {
                bulbBtn.Source = "lightbulb_white_64x64.png";

                quickPancake.BackgroundColor = Color.FromHex("#56efb1");

            }
            else
            {
                bulbBtn.Source = "bulb_64x64.png";

                quickPancake.BackgroundColor = Color.FromHex("#ff7256");
                //buttonFrm.BackgroundColor = Color.FromHex("#E8D9C4");
            }
            double opa = (double)((double)1 / (double)100) * (double)dim;
            bulbLightBtn.Opacity = opa==0?0:(0.5 +(opa/2));
            lightAuraBtn.Opacity = opa==0?0:(opa / 2)+0.3;//dim==0?0:32+(opa/1.5);
            //lightAuraBtn.WidthRequest = 100 + (dim / 3);
            //lightAuraBtn.HeightRequest = 100 + (dim / 3);
            //lightname.Text = opa + "";
            OnDimChangeHandler();
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }

        //public void IsPush(bool push)
        //{
        //    bgBtn.Source = (push == true ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        //}
    }
}
