﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class HomeControllerContent:ContentPage
    {
        public string Name { get; set; }
        public int MemberID { get; set; }
        public int Online { get; set; }
        private int RoomQuantity = 0;

        public List<RoomLayout> roomLayouts = new List<RoomLayout>();

        

        public void SetOnline(int online)
        {
            pancake.BackgroundColor = Color.Red;
            //OnOnlineHandler();
        }

        PancakeView pancake = new PancakeView
        {
            CornerRadius = new CornerRadius(20,20,0,0),
            BackgroundColor = Color.FromHex("#f8f8f8"),
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BorderColor = Color.FromHex("#f8f8f8"),
            BorderThickness = 2
        };
        ScrollView scrollView = new ScrollView
        {
            Orientation = ScrollOrientation.Vertical,
        };
        StackLayout mainStackLayout = new StackLayout
        {
            Orientation = StackOrientation.Vertical,
            BackgroundColor = Color.White
        };
        StackLayout stackLayout = new StackLayout
        {
            Orientation = StackOrientation.Vertical,
            BackgroundColor = Color.Transparent,
        };
        StackLayout header = new StackLayout
        {
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.Fill,
            Padding = 0,
            Orientation = StackOrientation.Horizontal
        };
        Label homeName = new Label
        {
            TextColor = Color.FromHex("#0094fb"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            FontSize = 24,
            Style = (Style)Application.Current.Resources["FontText"],
            MaxLines = 1,
            Margin = new Thickness(5,0,5,0),
            LineBreakMode = LineBreakMode.TailTruncation
        };

        PancakeView backBtnFrame = new PancakeView
        {
            WidthRequest = 50,
            HeightRequest = 50,
            HorizontalOptions = LayoutOptions.Start,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            Padding = 0,
            CornerRadius = 25,
            BackgroundColor = Color.Transparent
        };
        Grid backBtnGrid = new Grid
        {
            WidthRequest = 50,
            HeightRequest = 50,
            Padding = 0,
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };
        ImageButton backBtn = new ImageButton
        {
            BackgroundColor = Color.White,
            WidthRequest = 50,
            HeightRequest = 50,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };
        Image backBtnIcon = new Image
        {
            Source = "back_dark64x64.png",
            Aspect = Aspect.AspectFit,
            HeightRequest = 25,
            WidthRequest = 25,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true
        };

        //--- loading session ---
        Grid gridMain = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };

        ActivityIndicator indicator = new ActivityIndicator
        {
            Color = Color.FromHex("#0da6ca"),
            IsVisible = false,
            IsRunning = false
        };
        public HomeControllerContent(Home home)
        {
            backBtn.Clicked += delegate
            {
                Navigation.PopModalAsync();
            };
            backBtnGrid.Children.Add(backBtn);
            backBtnGrid.Children.Add(backBtnIcon);
            backBtnFrame.Content = backBtnGrid;
            if (In4.DeviceOS == TargetPlatform.iOS)
            {
                header.Margin = new Thickness(0, 20, 0, 0);
            }
            header.Children.Add(backBtnFrame);
            header.Children.Add(homeName);

            Padding = new Thickness(0,0,0,10);
            mainStackLayout.Children.Add(header);
            scrollView.Content = stackLayout;

            pancake.Content = scrollView;

            gridMain.Children.Add(pancake);
            gridMain.Children.Add(indicator);

            mainStackLayout.Children.Add(gridMain);
            Content = mainStackLayout;
            home.OnlineHandler += Home_OnlineHandler;

        }

        private void Home_OnlineHandler(object sender, int e)
        {
            pancake.BackgroundColor = e==1? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            pancake.IsEnabled = e == 1 ? true : false;
            indicator.IsVisible = e == 0 ? true : false;
            indicator.IsRunning = e == 0 ? true : false;
        }

        public void SetName(string name)
        {
            homeName.Text = name;
        }

        public void AddRoom(RoomLayout roomLayout)
        {
            /*if (RoomQuantity==0)
            {
                roomLayout.ShowSeperator(false);
            }*/
            roomLayouts.Add(roomLayout);
            

            stackLayout.Children.Add(roomLayout);
            RoomQuantity++;
        }
        
    }
}
