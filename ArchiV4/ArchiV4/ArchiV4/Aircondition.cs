﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class Aircondition:PancakeView
    {
        public int MemberID { get; set; }
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Temperature { get; set; }
        public int TempMin { get; set; } = 18;
        public int TempMax { get; set; } = 28;
        public int Speed { get; set; }
        public int Room { get; set; }

        public event EventHandler ButtonClicked;
        public event EventHandler QuickClicked;

        //--- EventHandler Delegate -----------------------------------------
        public event EventHandler<int> AirTemperatureHandler;
        public event EventHandler<int> AirSpeedHandler;
        public event EventHandler<int> AirSatusHandler;
        public event EventHandler<int> OnlineHandler;
        //public event EventHandler<Aircondition> ProcessCompleted;
        public void OnAirTemperatureHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Temperature]");
                OnAirTemperature(Temperature);
            }
            catch (Exception ex)
            {
                OnAirTemperature(Temperature);
            }
        }
        public void OnAirSpeedHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Speed]");
                OnAirSpeed(Speed);
            }
            catch (Exception ex)
            {
                OnAirSpeed(Speed);
            }
        }
        public void OnAirStatusHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnAirStatus(Status);
            }
            catch (Exception ex)
            {
                OnAirStatus(Status);
            }
        }
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }
        protected virtual void OnAirTemperature(int temperature)
        {
            AirTemperatureHandler?.Invoke(this, temperature);
        }
        protected virtual void OnAirSpeed(int speed)
        {
            AirSpeedHandler?.Invoke(this, speed);
        }
        protected virtual void OnAirStatus(int status)
        {
            AirSatusHandler?.Invoke(this, status);
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }
        /*public void StartProcess()
        {

            try
            {
                Console.WriteLine("Air event");

                // some code here..

                OnProcessCompleted(this);
            }
            catch (Exception ex)
            {
                OnProcessCompleted(this);
            }
        }*/

        /*protected virtual void OnProcessCompleted(Aircondition aircondition)
        {
            ProcessCompleted?.Invoke(this, aircondition);
        }*/
        //--- End of EventHandler Delegate -----------------------------------------


        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        Image bgBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth,
            WidthRequest = In4.SceneButtonWidth,
            Source = "btnsquare128x128_nin.png",
            Aspect = Aspect.AspectFill,
            InputTransparent = true,
            BackgroundColor = Color.Transparent,
        };

        Label temp = new Label
        {
            Text = 25 + "°",
            FontSize = In4.SceneOneOfPercent*24,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
            Margin = new Thickness(20, 15, 0, 0),
            InputTransparent = true,
            TextColor = Color.FromHex("#0094fb"),
            Style = (Style)Application.Current.Resources["FontText"],
        };
        Label speedlbl = new Label
        {
            //Text = ""+ home.Rooms[index].Airconditions[k].Speed,
            FontSize = In4.SceneOneOfPercent*18,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
            Margin = new Thickness(20, (In4.SceneOneOfPercent*40), 0, 0),
            InputTransparent = true,
            TextColor = Color.FromHex("#05e1a4"),
            Style = (Style)Application.Current.Resources["FontText"],
        };


        Label airname = new Label
        {
            Text = "",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.FromHex("#494949"),
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Grid quickGrid = new Grid
        {
            RowSpacing = 0,
            ColumnSpacing = 0,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.End,
            WidthRequest = 30,
            HeightRequest = 30,
        };

        PancakeView quickPancake = new PancakeView
        {
            Padding = 0,
            BackgroundColor = Color.FromHex("#56efb1"),
            HeightRequest = 10,
            WidthRequest = 20,
            CornerRadius = 5,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true
        };

        ImageButton quickBtn = new ImageButton
        {
            BackgroundColor = Color.Transparent,
            HeightRequest = 30,
            WidthRequest = 30,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };

        public Aircondition()
        {
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            //gridFrm.Children.Add(bgBtn);
            gridFrm.Children.Add(temp);
            gridFrm.Children.Add(speedlbl);
            gridFrm.Children.Add(airname);

            quickBtn.Clicked += QuickBtn_Clicked;
            quickGrid.Children.Add(quickBtn);
            quickGrid.Children.Add(quickPancake);
            gridFrm.Children.Add(quickGrid);

            
            Content = gridFrm;
        }

        private void QuickBtn_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = QuickClicked;
            handler?.Invoke(this, e);
        }

        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            airname.Text = name;
            
        }
        public void SetStatus(int status)
        {
            Status = status;
            if (status == 0)
            {
                speedlbl.Text = "OFF";
                speedlbl.TextColor = Color.FromHex("#FF9A36");
                quickPancake.BackgroundColor = Color.FromHex("#ff7256");
            }
            else
            {
                SetSpeed(Speed);
                quickPancake.BackgroundColor = Color.FromHex("#56efb1");
                //buttonFrm.BackgroundColor = Color.White;
            }
            OnAirStatusHandler();
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
        public void SetTemp(int tempVal)
        {
            Temperature = tempVal;
            temp.Text = tempVal + "°";
            OnAirTemperatureHandler();
        }
        public void SetOnline(int online)
        {
            Online = online;
            //buttonFrm.BackgroundColor = Color.FromHex("#E8D9C4");
            buttonFrm.BackgroundColor = Online==1?Color.White: (Color)Application.Current.Resources["Active"];
            airname.TextColor = Online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];
            gridFrm.IsEnabled = Online == 1 ? true : false;
            
            OnOnlineHandler();
        }
        public void SetSpeed(int speedVal)
        {
            Speed = speedVal;
            if(Status == 0)
            {
                speedlbl.Text = "OFF";
                speedlbl.TextColor = Color.FromHex("#FF9A36");
            }
            else if(Speed == 1)
            {
                speedlbl.Text = "LOW";
                speedlbl.TextColor = Color.FromHex("05e1a4");
            }
            else if (Speed == 2)
            {
                speedlbl.Text = "MED";
                speedlbl.TextColor = Color.FromHex("05e1a4");
            }
            else if (Speed == 3)
            {
                speedlbl.Text = "HIGH";
                speedlbl.TextColor = Color.FromHex("05e1a4");
            }
            OnAirSpeedHandler();
            //speedlbl.Text = speedVal + "%";
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }

        public void IsPush(bool push)
        {
            bgBtn.Source = (push == true ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
    }
}
