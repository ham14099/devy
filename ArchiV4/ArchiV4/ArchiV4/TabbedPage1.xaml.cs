﻿using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;
using TabbedPage = Xamarin.Forms.TabbedPage;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
        Home Homes;
        int ind;

        int currentair_power = 0;
        int currentair = 0;
        int currenttemp = 0;
        int currentSpeed = 0;

        int currentCurtain = 0;
        int currentCurtain1Status = 0;
        int currentCurtain2Status = 0;

        int currentdim = 0;
        int currentdimVal = 0;

        static double dimWidth = 300;
        static double acWidth = 300;
        static double ac_stepper = acWidth / 13;
        double d_stepper = dimWidth / 100;

        /*List<AirButton> buttonsAir = new List<AirButton>();
        List<CurtainButton> buttonsCur = new List<CurtainButton>();
        List<DimButton> buttonsDim = new List<DimButton>();
        List<ToggleButton> buttonsLight = new List<ToggleButton>();
        */

        Dictionary<int, int> Air_DeviceID_Index = new Dictionary<int, int>();
        Dictionary<int, int> Cur_DeviceID_Index = new Dictionary<int, int>();
        Dictionary<int, int> Dim_DeviceID_Index = new Dictionary<int, int>();
        Dictionary<int, int> Light_DeviceID_Index = new Dictionary<int, int>();

        public TabbedPage1(Home home, int index)
        {
            InitializeComponent();
            Homes = home;
            ind = index;
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom).DisableSwipePaging();
            //this.CurrentPage = Children[3];
            this.CurrentPageChanged += (object sender, EventArgs e) => {

                var i = this.Children.IndexOf(this.CurrentPage);

                /*gridpage.Icon = "icon_light1.png";
                scenepage.Icon = "icon_scene1.png";
                settingpage.Icon = "icon_menu1.png";
                */
                /*t1.Title = "O";
                t2.Title = "O";
                t3.Title = "O";
                
                switch (i)
                {
                    case 0:
                        t1.Title = "*";
                        //gridpage.Icon = "icon_light2.png";
                        break;
                    case 1:
                        t2.Title = "*";
                        //scenepage.Icon = "icon_scene2.png";
                        break;
                    case 2:
                        t3.Title = "*";
                        // settingpage.Icon = "icon_menu2.png";
                        break;
                }*/
            };

            string roomname = Provider.ROOM_LIST[home.TabIndex][index].Name;
            acRoomname.Text = roomname;
            curRoomname.Text = roomname;
            dimRoomname.Text = roomname;
            lightRoomname.Text = roomname;


            //---air

            if (Provider.AIR_LIST[home.TabIndex].Count > 0)
            {
                acNameLbl.Text = "" + Provider.AIR_LIST[home.TabIndex][0].Name;//aircondition.Name;
                SetSpeed(Provider.AIR_LIST[home.TabIndex][0].Speed);
                currentair_power = Provider.AIR_LIST[home.TabIndex][currentair].Status;
                currenttemp = Provider.AIR_LIST[home.TabIndex][currentair].Temperature;
                currentSpeed = Provider.AIR_LIST[home.TabIndex][currentair].Speed;

                SetTemp(currenttemp);
                SetStatus(currentair_power);
                //tempLbl.Text = 25 + "°";
                acSlider.Value = currenttemp - 17;
                tempValLbl.Text = currenttemp + "°";
                for (int k = 0; k < Provider.AIR_LIST[home.TabIndex].Count; k++)
                {
                    ac_layout.Children.Add(Provider.AIR_LIST[home.TabIndex][k]);
                }
            }
            //--- curtain ---
            if (Provider.CURTAIN_LIST[home.TabIndex].Count > 0)
            {
                curtainNameLbl.Text = "" + Provider.CURTAIN_LIST[home.TabIndex][0].Name;//Curtains.Name;
                currentCurtain = 0;
                currentCurtain1Status = Provider.CURTAIN_LIST[home.TabIndex][0].Status1;
                currentCurtain2Status = Provider.CURTAIN_LIST[home.TabIndex][0].Status2;
                SetCurtain1(currentCurtain1Status);
                SetCurtain2(currentCurtain2Status);
                for (int k = 0; k < Provider.CURTAIN_LIST[home.TabIndex].Count; k++)
                {
                    cur_layout.Children.Add(Provider.CURTAIN_LIST[home.TabIndex][k]);
                }
            }

            //--- Dimmer ---
            if (Provider.DIMMER_LIST[home.TabIndex].Count > 0)
            {
                dimNameLbl.Text = "" + Provider.DIMMER_LIST[home.TabIndex][0].Name;//Dim.Name;
                currentdim = 0;
                currentdimVal = Provider.DIMMER_LIST[home.TabIndex][0].Dimvalue;
                dimSlider.Value = currentdimVal;
                dimValLbl.Text = currentdimVal + "%";


                for (int k = 0; k < Provider.DIMMER_LIST[home.TabIndex].Count; k++)
                {
                    Dim_DeviceID_Index.Add(Provider.DIMMER_LIST[home.TabIndex][k].DeviceID, k);
                    
                    dim_layout.Children.Add(Provider.DIMMER_LIST[home.TabIndex][k]);
                }
            }

            if (Provider.LIGHT_LIST[home.TabIndex].Count > 0)
            {
                //lightNameLbl.Text = "" + home.Rooms[index].Lights[0].Name;//Curtains.Name;

                Grid grid = new Grid
                {
                    ColumnSpacing = 10,
                    RowSpacing = 10
                };
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                grid.ColumnDefinitions.Add(new ColumnDefinition());

                if (In4.DevicePlatform == DeviceIdiom.Tablet)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                }
                int col = 0, row = 0;

                for (int k = 0; k < Provider.LIGHT_LIST[home.TabIndex].Count; k++)
                {
                    Light_DeviceID_Index.Add(Provider.LIGHT_LIST[home.TabIndex][k].DeviceID, k);
                    if (k % In4.sceneMaxForRow == 0 && k > 1)
                    {
                        row++;
                        grid.RowDefinitions.Add(new RowDefinition());
                    }

                    if (col == In4.sceneMaxForRow)
                    {
                        col = 0;
                    }
                    grid.Children.Add(Provider.LIGHT_LIST[home.TabIndex][k], col, row);
                    col++;
                }
                light_layout.Children.Add(grid);
            }
            //Provider.Client.OnDeviceControl += Client_OnDeviceControl;
        }

        private void Client_OnDeviceControl(IOTClient client, DeviceControlResult deviceControlResult, IOTClient.OnDeviceControlType commandType)
        {
            
        }

        private void SetCurtain1(int status)
        {
            switch (status)
            {
                case 0:
                    currentCurtain1Status = 0;
                    /*cur1_open.TextColor = Color.FromHex("#ffffff");
                    cur1_stop.TextColor = Color.FromHex("#2e84d5");
                    cur1_close.TextColor = Color.FromHex("#ffffff");*/
                    c1open.Source = "btnsquare128x128_nin.png";
                    c1stop.Source = "btnsquare128x128.png";
                    c1close.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID,Homes.Rooms[ind].Airconditions[currentair].DeviceID,3,1);
                    break;
                case 1:
                    currentCurtain1Status = 1;
                    /*cur1_open.TextColor = Color.FromHex("#2e84d5");
                    cur1_stop.TextColor = Color.FromHex("#ffffff");
                    cur1_close.TextColor = Color.FromHex("#ffffff");*/
                    c1open.Source = "btnsquare128x128.png";
                    c1stop.Source = "btnsquare128x128_nin.png";
                    c1close.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 2);
                    break;
                case 2:
                    currentCurtain1Status = 2;
                    /*cur1_open.TextColor = Color.FromHex("#ffffff");
                    cur1_stop.TextColor = Color.FromHex("#ffffff");
                    cur1_close.TextColor = Color.FromHex("#2e84d5");*/
                    c1open.Source = "btnsquare128x128_nin.png";
                    c1stop.Source = "btnsquare128x128_nin.png";
                    c1close.Source = "btnsquare128x128.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 3);
                    break;
            }
        }

        private void SetCurtain2(int status)
        {
            switch (status)
            {
                case 0:
                    currentCurtain2Status = 0;
                    /*cur2_open.TextColor = Color.FromHex("#ffffff");
                    cur2_stop.TextColor = Color.FromHex("#2e84d5");
                    cur2_close.TextColor = Color.FromHex("#ffffff");*/
                    c2open.Source = "btnsquare128x128_nin.png";
                    c2stop.Source = "btnsquare128x128.png";
                    c2close.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID,Homes.Rooms[ind].Airconditions[currentair].DeviceID,3,1);
                    break;
                case 1:
                    currentCurtain2Status = 1;
                    /*cur2_open.TextColor = Color.FromHex("#2e84d5");
                    cur2_stop.TextColor = Color.FromHex("#ffffff");
                    cur2_close.TextColor = Color.FromHex("#ffffff");*/
                    c2open.Source = "btnsquare128x128.png";
                    c2stop.Source = "btnsquare128x128_nin.png";
                    c2close.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 2);
                    break;
                case 2:
                    currentCurtain2Status = 2;
                    /*cur2_open.TextColor = Color.FromHex("#ffffff");
                    cur2_stop.TextColor = Color.FromHex("#ffffff");
                    cur2_close.TextColor = Color.FromHex("#2e84d5");*/
                    c2open.Source = "btnsquare128x128_nin.png";
                    c2stop.Source = "btnsquare128x128_nin.png";
                    c2close.Source = "btnsquare128x128.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 3);
                    break;
            }
        }

        private void ButtonFrm_Clicked2(object sender, EventArgs e)
        {
            /*currentCurtain = (sender as CurtainButton).TabIndex;
            //currentdimVal = Homes.Rooms[ind].Curtains[currentdim].Dim;
            SetCurtain1(Homes.Rooms[ind].Curtains[currentCurtain].Status1);
            SetCurtain2(Homes.Rooms[ind].Curtains[currentCurtain].Status2);
            curtainNameLbl.Text = Homes.Rooms[ind].Curtains[currentCurtain].Name;
            SetDim(currentdimVal);

            for (int p = 0; p < Homes.Rooms[ind].Curtains.Count; p++)
            {
                buttonsCur[p].IsPush(p == (sender as CurtainButton).TabIndex ? true : false);
            }*/
        }

        private void ButtonFrm_Clicked1(object sender, EventArgs e)
        {
            /*currentdim = (sender as DimButton).TabIndex;
            currentdimVal = Homes.Rooms[ind].Dimmers[currentdim].Dim;
            dimNameLbl.Text = Homes.Rooms[ind].Dimmers[currentdim].Name;
            SetDim(currentdimVal);

            for (int p = 0; p < Homes.Rooms[ind].Dimmers.Count; p++)
            {
                buttonsDim[p].IsPush(p == (sender as DimButton).TabIndex ? true : false);
            }*/
        }

        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            /*currentair = (sender as AirButton).TabIndex;
            currentair_power = Homes.Rooms[ind].Airconditions[currentair].Status;
            currenttemp = Homes.Rooms[ind].Airconditions[currentair].Temp;
            currentSpeed = Homes.Rooms[ind].Airconditions[currentair].Speed;

            SetSpeed(currentSpeed);
            SetTemp(currenttemp);
            SetStatus(currentair_power);

            currentair_power = Homes.Rooms[ind].Airconditions[currentair].Status;


            for (int p = 0; p < Homes.Rooms[ind].Airconditions.Count; p++)
            {
                buttonsAir[p].IsPush(p == (sender as AirButton).TabIndex ? true : false);
            }*/
        }

        private void SetDim(int dim)
        {
            dimSlider.Value = currentdimVal;
            dimValLbl.Text = currentdimVal + "%";
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private void air_spdH_Clicked(object sender, EventArgs e)
        {
            SetSpeed(3);
            Provider.AIR_LIST[Homes.TabIndex][currentair].Speed = 3;
            Provider.SendDeviceControl(Homes.MemberID, Provider.AIR_LIST[Homes.TabIndex][currentair].DeviceID, 3, 3);
        }

        private void air_spdM_Clicked(object sender, EventArgs e)
        {
            SetSpeed(2);
            Provider.AIR_LIST[Homes.TabIndex][currentair].Speed = 2;
            Provider.SendDeviceControl(Homes.MemberID, Provider.AIR_LIST[Homes.TabIndex][currentair].DeviceID, 3, 2);
        }

        private void air_spdL_Clicked(object sender, EventArgs e)
        {
            SetSpeed(1);
            Provider.AIR_LIST[Homes.TabIndex][currentair].Speed = 1;
            Provider.SendDeviceControl(Homes.MemberID, Provider.AIR_LIST[Homes.TabIndex][currentair].DeviceID, 3, 1);
        }

        private void SetTemp(int temp)
        {
            currenttemp = temp;
            //acSlideCover.WidthRequest = acWidth - (temp-15 * ac_stepper);
            acSlider.Value = temp - 17;
            tempValLbl.Text = ((int)temp) + "°";
        }

        private void SetStatus(int status)
        {
            currentair_power = status;
            acPowerBtn.Source = status == 1 ? "on_64x64.png" : "off_64x64.png";
        }

        private void SetSpeed(int speed)
        {
            switch (speed)
            {
                case 1:
                    currentSpeed = 1;
                    air_spdH.TextColor = Color.FromHex("#ffffff");
                    air_spdM.TextColor = Color.FromHex("#ffffff");
                    air_spdL.TextColor = Color.FromHex("#2e84d5");
                    ac_sp_bgH.Source = "btnsquare128x128_nin.png";
                    ac_sp_bgM.Source = "btnsquare128x128_nin.png";
                    ac_sp_bgL.Source = "btnsquare128x128.png";
                    //Provider.SendDeviceControl(Homes.MemberID,Homes.Rooms[ind].Airconditions[currentair].DeviceID,3,1);
                    break;
                case 2:
                    currentSpeed = 2;
                    air_spdH.TextColor = Color.FromHex("#ffffff");
                    air_spdM.TextColor = Color.FromHex("#2e84d5");
                    air_spdL.TextColor = Color.FromHex("#ffffff");
                    ac_sp_bgH.Source = "btnsquare128x128_nin.png";
                    ac_sp_bgM.Source = "btnsquare128x128.png";
                    ac_sp_bgL.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 2);
                    break;
                case 3:
                    currentSpeed = 3;
                    air_spdH.TextColor = Color.FromHex("#2e84d5");
                    air_spdM.TextColor = Color.FromHex("#ffffff");
                    air_spdL.TextColor = Color.FromHex("#ffffff");
                    ac_sp_bgH.Source = "btnsquare128x128.png";
                    ac_sp_bgM.Source = "btnsquare128x128_nin.png";
                    ac_sp_bgL.Source = "btnsquare128x128_nin.png";
                    //Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Airconditions[currentair].DeviceID, 3, 3);
                    break;
            }
        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            int val = (int)(sender as Slider).Value;
            dimSlideCover.WidthRequest = dimWidth - (val * d_stepper);
            dimValLbl.Text = val + "%";

        }

        private void Slider_DragCompleted(object sender, EventArgs e)
        {
            /*int val = (int)dimSlider.Value;
            buttonsDim[currentdim].SetDim(val);
            Homes.Rooms[ind].Dimmers[currentdim].Dim = val;
            Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Dimmers[currentdim].DeviceID, 1, val);*/
        }

        private void acSlider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            acSlideCover.WidthRequest = acWidth - ((sender as Slider).Value * ac_stepper);
            tempValLbl.Text = ((int)(sender as Slider).Value + 17) + "°";
        }

        private void acSlider_DragCompleted(object sender, EventArgs e)
        {
            Provider.AIR_LIST[Homes.TabIndex][currentair].Temperature = ((int)acSlider.Value + 17);
            Provider.SendDeviceControl(Homes.MemberID, Provider.AIR_LIST[Homes.TabIndex][currentair].DeviceID, 2, ((int)acSlider.Value + 17));
        }

        private void acPower_Clicked(object sender, EventArgs e)
        {
            currentair_power ^= 1;
            Provider.AIR_LIST[Homes.TabIndex][currentair].Status = currentair_power;
            Provider.SendDeviceControl(Homes.MemberID, Provider.AIR_LIST[Homes.TabIndex][currentair].DeviceID, 1, currentair_power);
            acPowerBtn.Source = currentair_power == 1 ? "on_64x64.png" : "off_64x64.png";
        }

        private void cur1_open_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 1, 1);
            SetCurtain1(1);
            Homes.Rooms[ind].Curtains[currentCurtain].Status1 = 1;*/
        }

        private void cur1_stop_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 1, 0);
            SetCurtain1(0);
            Homes.Rooms[ind].Curtains[currentCurtain].Status1 = 0;*/
        }

        private void cur1_close_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 1, 2);
            SetCurtain1(2);
            Homes.Rooms[ind].Curtains[currentCurtain].Status1 = 2;*/
        }

        private void cur2_open_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 2, 1);
            SetCurtain2(1);
            Homes.Rooms[ind].Curtains[currentCurtain].Status2 = 1;*/
        }

        private void cur2_stop_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 2, 0);
            SetCurtain2(0);
            Homes.Rooms[ind].Curtains[currentCurtain].Status2 = 0;*/
        }

        private void cur2_close_Clicked(object sender, EventArgs e)
        {
            /*Provider.SendDeviceControl(Homes.MemberID, Homes.Rooms[ind].Curtains[currentCurtain].DeviceID, 2, 2);
            SetCurtain2(2);
            Homes.Rooms[ind].Curtains[currentCurtain].Status2 = 2;*/
        }
    }
}