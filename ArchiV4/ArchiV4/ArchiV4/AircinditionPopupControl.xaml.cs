﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AircinditionPopupControl : PopupPage
    {
        static double dimWidth = 300;
        static double acWidth = 300;
        double ac_stepper = 0;
        double d_stepper = dimWidth / 100;
        Aircondition Airs;
        int Tmax = 0;
        int Tmin = 0;
        public AircinditionPopupControl(Aircondition air)
        {
            InitializeComponent();
            airName.Text = air.Name;
            Airs = air;
            Tmax = (air.TempMax - air.TempMin) + 1;
            Tmin = air.TempMin;
            acSlider.Maximum = Tmax;
            ac_stepper = acWidth / Tmax;
            SetTemperature(air.Temperature);
            //acSlider.Value = air.Temperature - (air.TempMin-1);
            //acSlideCover.WidthRequest = acWidth - (acSlider.Value * ac_stepper);
            SetSpeed(air.Speed);
            powerIcon.Source = air.Status == 1 ? "on_64x64.png" : "off_64x64.png";
            //air.ProcessCompleted += Air_ProcessCompleted;
            air.AirTemperatureHandler += Air_AirTemperatureHandler;
            air.AirSpeedHandler += Air_AirSpeedHandler;
            air.AirSatusHandler += Air_AirSatusHandler;
            air.OnlineHandler += DevicesOnlineHandler;
        }

        private void DevicesOnlineHandler(object sender, int e)
        {
            controlPanel.IsEnabled = e == 1 ? true : false;

            spdLow.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            spdMed.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            spdHigh.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            powerBtn.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            tempValLbl.TextColor = e == 1 ? Color.FromHex("#0094fb") : Color.FromHex("#d9d9d9");
            acSlideCover.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");

            //mainPancake.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d0d0d0");
        }

        private void Air_AirSatusHandler(object sender, int e)
        {
            SetStatus(e);
        }

        private void Air_AirSpeedHandler(object sender, int e)
        {
            SetSpeed(e);
        }

        private void Air_AirTemperatureHandler(object sender, int e)
        {
            SetTemperature(e);
        }


        private async void closeBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void acSlider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            acSlideCover.WidthRequest = acWidth - ((sender as Slider).Value * ac_stepper);
            tempValLbl.Text = ((int)(sender as Slider).Value + (Tmin-1)) + "°";
        }

        private void acSlider_DragCompleted(object sender, EventArgs e)
        {
            Airs.Temperature = ((int)acSlider.Value + Tmax);
            Provider.SendDeviceControl(Airs.MemberID, Airs.DeviceID, 2, ((int)acSlider.Value + (Tmin - 1)));
        }

        private void spdLow_Clicked(object sender, EventArgs e)
        {
            SetSpeed(1);
            Provider.SendDeviceControl(Airs.MemberID,Airs.DeviceID,3,Airs.Speed);
        }

        private void spdMed_Clicked(object sender, EventArgs e)
        {
            SetSpeed(2);
            Provider.SendDeviceControl(Airs.MemberID, Airs.DeviceID, 3, Airs.Speed);
        }

        private void spdHigh_Clicked(object sender, EventArgs e)
        {
            SetSpeed(3);
            Provider.SendDeviceControl(Airs.MemberID, Airs.DeviceID, 3, Airs.Speed);
        }

        private void SetStatus(int status)
        {
            Airs.Status = status;
            powerIcon.Source = Airs.Status == 1 ? "on_64x64.png" : "off_64x64.png";
        }
        private void SetTemperature(int temperatue)
        {
            tempValLbl.Text = temperatue+"°";
            acSlider.Value = temperatue - (Airs.TempMin - 1);
            acSlideCover.WidthRequest = acWidth - (acSlider.Value * ac_stepper);
        }

        private void SetSpeed(int speed)
        {
            switch (speed)
            {
                case 1:
                    spdLow.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    spdMed.BackgroundColor = Color.White;
                    spdHigh.BackgroundColor = Color.White;
                    Airs.Speed = speed;
                    break;
                case 2:
                    spdLow.BackgroundColor = Color.White;
                    spdMed.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    spdHigh.BackgroundColor = Color.White;
                    Airs.Speed = speed;
                    break;
                case 3:
                    spdLow.BackgroundColor = Color.White;
                    spdMed.BackgroundColor = Color.White;
                    spdHigh.BackgroundColor = Color.FromHex("#f0f0f0");//---
                    Airs.Speed = speed;
                    break;


            //    case 1:
            //        spdLow.TextColor = Color.FromHex("#0094fb");
            //        spdMed.TextColor = Color.FromHex("#494949");
            //        spdHigh.TextColor = Color.FromHex("#494949");
            //        Airs.Speed = speed;
            //        break;
            //    case 2:
            //        spdLow.TextColor = Color.FromHex("#494949");
            //        spdMed.TextColor = Color.FromHex("#0094fb");
            //        spdHigh.TextColor = Color.FromHex("#494949");
            //        Airs.Speed = speed;
            //        break;
            //    case 3:
            //        spdLow.TextColor = Color.FromHex("#494949");
            //        spdMed.TextColor = Color.FromHex("#494949");
            //        spdHigh.TextColor = Color.FromHex("#0094fb");
            //        Airs.Speed = speed;
            //        break;
            //
            }
        }

        private void powerBtn_Clicked(object sender, EventArgs e)
        {
            Airs.Status ^= 1;
            powerIcon.Source = Airs.Status == 1 ? "on_64x64.png" : "off_64x64.png";
            Provider.SendDeviceControl(Airs.MemberID, Airs.DeviceID, 1, Airs.Status);
        }
    }
}