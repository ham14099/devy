﻿using NucksooIOTCommon;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Markup;
using Xamarin.Forms.PancakeView;
using ZXing.Net.Mobile.Forms;
using static NucksooTCP.TCPClient;
using DeviceInfo = Xamarin.Essentials.DeviceInfo;

namespace ArchiV4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        static int col = 0, row = 0, k = 0, max = 2, ind = 0;
        int init = 0;
        int airCnt = 0;
        int curCnt = 0;
        int dimCnt = 0;
        int lightCnt = 0;
        int rgbCnt = 0;
        int sceneCnt = 0;
        int powerCnt = 0;
        string[] data;
        static int exitApp = 0;
        int timerConnect = 0;
        int reTimerConnect = 0;

        int Mymember = 0;

        int addPress = 0;


        ZXingScannerPage scan = new ZXingScannerPage()
        {
            HasTorch = true,
            IsTorchOn = true
        };
        string oldResult = "";

        public MainPage()
        {
            InitializeComponent();
            header.WidthRequest = (In4.ScreenWidth / In4.ScreenDensity) - 80;


            Provider.Client.OnLogin += Client_OnLogin;
            Provider.Client.OnFriendInformation += Client_OnFriendInformation;
            Provider.Client.OnDeviceControl += Client_OnDeviceControl;
            Provider.Client.OnFriendStatus += Client_OnFriendStatus;
            Provider.Client.OnDisconnect += Client_OnDisconnect;
            Provider.Client.OnConnect += Client_OnConnect;

            if (Device.OS == TargetPlatform.iOS)
            {
                container.Margin = new Thickness(0, 20, 0, 0);
            }

            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
            if (In4.DevicePlatform == DeviceIdiom.Tablet)
            {
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                max = 5;
            }
            //In4.In4Init();
            //--- check txt file config ---
            var filename = "";
            string dataread = "";
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                    using (var reader = new StreamReader(filename))
                    {
                        dataread = reader.ReadToEnd();
                        data = dataread.Split(',');
                        if (data.Length > 4)
                        {
                            //Log.Warning("main","lng >4:"+ dataread);
                            Thread thread = new Thread(() =>
                            {
                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    Log.Warning("main", "pre cnct");
                                    var res = Provider.Connect(data);
                                    Log.Warning("main", "cnct");
                                    if (res == true)
                                    {
                                        Log.Warning("main", "cncted");
                                        //Provider.reqFriendInfo();
                                    }
                                });
                            });
                            thread.Start();
                            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                            {
                                timerConnect++;
                                Log.Warning("timerConnect", "=*=*=*=*=*=" + timerConnect);
                                if (timerConnect < 20)
                                {
                                    Log.Warning("timerConnect", "=" + timerConnect);
                                    //update the count down timer with 1 second here 
                                    return true;
                                }
                                else if (timerConnect == 101)
                                {
                                    Log.Warning("timerConnect", "==============" + timerConnect);
                                    timerConnect = 0;
                                    return false;
                                }
                                else
                                {
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        await DisplayAlert("Connection", "Timeout", "Close");
                                        Log.Warning("timerConnect", "************" + timerConnect);
                                        timerConnect = 0;
                                        //reThread.Abort();
                                        exitApp = 1;
                                        Environment.Exit(0);
                                    });
                                    //DisplayAlert("Connection","Timeout","Close");
                                    //Log.Warning("timerConnect", "************" + timerConnect);
                                    //timerConnect = 0;
                                    //thread.Abort();
                                }
                                return false;
                            });
                            
                            




                        }
                        else if (dataread.Trim()=="")
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                DisplayAlert("title", "invalid data", "CLOSE");
                                await Navigation.PushModalAsync(new LoginPage());
                            });
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                DisplayAlert("title", "found but invalid", "CLOSE");
                                await Navigation.PushModalAsync(new LoginPage());
                            });
                        }
                    }
                });
            }
            catch (FileNotFoundException e)
            {
                Log.Warning("Error(1X0001)", "Error : " + e.Message);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //DisplayAlert("title", "not found", "OK");
                    await Navigation.PushModalAsync(new LoginPage());
                    //await Navigation.PushModalAsync(new HomePage());
                });
            }
            catch (Exception error)
            {
                Log.Warning("Error(1X0002)", "Error : " + error);
            }            
        }

        private async void Client_OnConnect(IOTClient client)
        {
            timerConnect = 100;
            reTimerConnect = 100;
            Log.Warning("on connect","on connecthandler");
            //await DisplayAlert("Event", "OnConnect:" + client.IsConnected, "Close");
            if (!client.IsConnected)
            {
                await DisplayAlert("Event", "cant Connect", "Close");
                //MessagingDialog("Error", "Can't Connect to server", ExitOnHandler);
            }
            else
            {
                indi.IsRunning = false;
                indi.IsVisible = false;
            }
        }

        private async void Client_OnDisconnect(IOTClient client, ConnectionStatusType disconnecType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                Log.Warning("disconnect", "disconnected:"+ disconnecType.ToString());
                try
                {
                    indi.IsRunning = true;
                    indi.IsVisible = true;
                    //DisplayAlert("Disconnect", "Disconnected code:" + exitApp + ":" + data[1] + ":" + data[2] + ":" + data[3] + ":" + data[4] + ":", "Ok");
                    if (exitApp == 0)
                    {

                        //--- try to connect
                        /*if (Provider.Client.IsConnected)
                        {
                            Provider.DisConnect();
                        }
                        */


                        //var current = Connectivity.NetworkAccess;

                        //Thread.Sleep(100);
                        //DisplayAlert("NetworkAccess", "State : "+ current, "Close");


                        //if (current == NetworkAccess.None)
                        //{
                            //await DisplayAlert("NetworkAccess", "None", "Close");
                            /*reTimerConnect = 0;
                            //reThread.Abort();
                            exitApp = 1;
                            Environment.Exit(0);*/
                        //}
                        //if (!Provider.Client.IsConnected)
                        if (false)
                        {
                            //var res = Provider.Connect(data);
                            //Log.Warning("connect", "try to connect");
                            //if (res == true)
                            //{
                            //    Log.Warning("connect", "connected again");
                            //    //Provider.reqFriendInfo();

                            //}
                            Thread reThread = new Thread(() =>
                            {
                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    Log.Warning("remain", "pre cnct:" + data[1]);
                                    //var res = Provider.Connect(data);
                                    //--- force exit ---
                                    reTimerConnect = 0;
                                    //reThread.Abort();
                                    exitApp = 1;
                                    Environment.Exit(0);
                                    //---
                                    Log.Warning("remain", "cnct");
                                    /*if (res == true)
                                    {
                                        Log.Warning("remain", "cncted");
                                        //Provider.reqFriendInfo();
                                    }*/
                                });
                            });
                            reThread.Start();
                            /*Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                            {
                                reTimerConnect++;
                                Log.Warning("retimerConnect", "=*=*=*=*=*=" + reTimerConnect);
                                if (reTimerConnect < 20)
                                {
                                    Log.Warning("retimerConnect", "=" + reTimerConnect);
                                    //update the count down timer with 1 second here 
                                    return true;
                                }
                                else if (reTimerConnect == 101)
                                {
                                    Log.Warning("retimerConnect", "==============" + reTimerConnect);
                                    reTimerConnect = 0;
                                    return false;
                                }
                                else
                                {
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        await DisplayAlert("Connection", "reTimeout", "Close");
                                        Log.Warning("retimerConnect", "************" + reTimerConnect);
                                        reTimerConnect = 0;
                                        //reThread.Abort();
                                        exitApp = 1;
                                        Environment.Exit(0);
                                    });
                                    
                                }
                                return false;
                            });*/
                        }
                        //Log.Warning("main", "lng >4:" + dataread);
                        
                        //var res = Provider.Connect(data);
                        //Log.Warning("connect", "try to connect");
                        //if (res == true)
                        //{
                        //    Log.Warning("connect", "connected again");
                        //    //Provider.reqFriendInfo();

                        //}
                        //DisplayAlert("Disconnect", "Connect again", "Ok");
                    }
                    else
                    {
                        //DisplayAlert("Disconnect", "Disconnected:" + disconnecType.ToString(), "Ok");
                    }
                }
                catch (Exception er)
                {
                    Log.Warning("OnDisconnect error", "" + er);
                }
                
                
            }); 
        }

        private void Client_OnFriendStatus(IOTClient client, FriendStatus friendStatus)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    Provider.Homes[Provider.MEMBER_ID_INDEX[friendStatus.MemberID]].IsOnline(friendStatus.Status == OnlineStatus.Online ? 1 : 0);
                    if (friendStatus.Status == OnlineStatus.Online)
                    {
                        Provider.reqFriendInfo();
                    }
                }
                catch (Exception error)
                {
                    Log.Warning("Error [friend status]",""+error);
                }
            });
                
        }

        private void Client_OnDeviceControl(IOTClient client, DeviceControlResult deviceControlResult, IOTClient.OnDeviceControlType commandType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    int idx = Provider.MEMBER_ID_INDEX[deviceControlResult.Member];
                    //Log.Warning("oncontrol","m="+ deviceControlResult.Member+",d="+ deviceControlResult.Device+",c="+ deviceControlResult.Ctrl+",v="+ deviceControlResult.V);
                    switch (Provider.DEVICE_DEVICEID_TYPE[idx][deviceControlResult.Device])
                    {
                        case 1:
                            try
                            {

                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetStatus((int)deviceControlResult.V);
                                        break;
                                    case 2:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetTemp((int)deviceControlResult.V);
                                        break;
                                    case 3:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetSpeed((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                        case 2:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetStatus1((int)deviceControlResult.V);
                                        break;
                                    case 2:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetStatus2((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception error)
                            {

                            }
                            break;
                        case 3:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.DIMMER_LIST[idx][Provider.DIM_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.DIMMER_LIST[idx][Provider.DIM_ID_INDEX[idx][deviceControlResult.Device]].SetDim((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception error)
                            {

                            }
                            break;
                        case 4:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.LIGHT_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.LIGHT_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetStatus((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                        case 5:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.RGB_LIST[idx][Provider.RGB_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.RGB_LIST[idx][Provider.RGB_ID_INDEX[idx][deviceControlResult.Device]].SetRGB((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                        case 13:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.Homes[idx].DNDMURVisible = true;
                                        break;
                                    case 1:
                                        Provider.Homes[idx].SetDNDMUR((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                        case 20:
                            try
                            {
                               // Log.Warning("type","type 20 d=:"+ (int)deviceControlResult.V);
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        //Provider.POWER_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        Provider.Homes[idx].EnablePowerConsumption(true);
                                        break;
                                    case 1:
                                        //Log.Warning("ctrl", "ctrl 1");
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetVoltage(deviceControlResult.V);
                                        break;
                                    case 2:
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetCurrent(deviceControlResult.V);
                                        break;
                                    case 3:
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetWatt(deviceControlResult.V);
                                        break;
                                    case 4:
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetPowerFactor(deviceControlResult.V);
                                        break;
                                    case 5:
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetEnergy(deviceControlResult.V);
                                        break;
                                    case 6:
                                        Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetFrequency(deviceControlResult.V);
                                        //Log.Warning("fq", "type 20 d=:" + (int)deviceControlResult.V);
                                        break;
                                }
                                /*if (deviceControlResult.Ctrl>9)
                                {
                                    Provider.POWER_LIST[idx][Provider.POWER_ID_INDEX[idx][deviceControlResult.Device]].SetEnergy(deviceControlResult.V);
                                }*/
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                    }
                }
                catch (KeyNotFoundException knfEx) {
                    //Log.Warning("KeyNotFoundException(on control)", ""+knfEx);
                }
                catch (Exception error)
                {
                    Log.Warning("error (on control)", "" + error);

                }
                
            });
        }
        HomeControllerContent homeControllers;
        int openSetting = 0;
        private async void settingBtn_Clicked(object sender, EventArgs e)
        {
            //DisplayAlert("Alert", "Setting", "OK");
            if (openSetting == 0)
            { 
                openSetting = 1;
                await Navigation.PushModalAsync(new SettingPage());
                openSetting = 0;
            }
            
        }

        private async void NewHome_Clicked(object sender, EventArgs e)
        {
            if (addPress == 0)
            {
                addPress = 1;
                GoToScan();
            }
            


        }
        int tor = 0;
        async void GoToScan()
        {
            oldResult = "";
            bool fSupport = true;
            bool fPermis = true;
            int abortPress = 0;

            //await Navigation.PushModalAsync(scan);
            try
            {
                 await Flashlight.TurnOffAsync();
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                fSupport = false;
            }
            catch (PermissionException pEx)
            {
                fPermis = false;
            }
            catch (Exception ex)
            {
                fPermis = false;
            }

            var customOverlay = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start
            };
            StackLayout qrStack = new StackLayout
            {
                VerticalOptions = LayoutOptions.End,
            };

            Grid layoutGrid = new Grid();

            PancakeView flashPancake = new PancakeView
            {
                CornerRadius = 25,
                HeightRequest = 50,
                WidthRequest = 50,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Start,
                Margin = new Thickness(10,10,0,0),
                IsVisible = fSupport,
                IsEnabled = fPermis
            };
            Grid flashGrid = new Grid();
            ImageButton flashBtn = new ImageButton
            {
                HeightRequest = 50,
                WidthRequest = 50,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.White
            };
            Image flashImg = new Image
            {
                Source = "flashlight_off_64x64.png",
                Aspect = Aspect.AspectFit,
                InputTransparent = true,
                HeightRequest = 50,
                WidthRequest = 50,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            };
            flashGrid.Children.Add(flashBtn);
            flashGrid.Children.Add(flashImg);

            flashPancake.Content = flashGrid;

            PancakeView abortPancake = new PancakeView
            {
                CornerRadius = 20,
                HeightRequest = 40,
                WidthRequest = 40,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.End,
                Margin = new Thickness(0, 10, 10, 0),
            };
            Grid abortGrid = new Grid
            {

            };
            Image abortImg = new Image
            {
                Source = "X_64x64.png",
                Aspect = Aspect.AspectFit,
                InputTransparent = true,
                HeightRequest = 40,
                WidthRequest = 40,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            };
            ImageButton abort = new ImageButton
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HeightRequest = 40,
                WidthRequest = 40,
                BackgroundColor = Color.White,
                //BackgroundColor = Color.FromHex("#ff7161"),
                Opacity = 0.5
            };
            abortGrid.Children.Add(abort);
            abortGrid.Children.Add(abortImg);
            abortPancake.Content = abortGrid;

            abort.Clicked += async (sender, arg) =>
            {
                if (abortPress == 0)
                {
                    abortPress = 1;
                    addPress = 0;
                    await Navigation.PopModalAsync();
                }
                
            };
            flashBtn.Clicked += async (sender, arg) =>
            {

                scan.ToggleTorch(); 
                tor ^= 1;
                if (tor == 1)
                {
                    flashImg.Source = "flashlight_on_64x64.png";
                    flashBtn.BackgroundColor = Color.White;
                }
                else
                {
                    flashImg.Source = "flashlight_off_64x64.png"; 
                    flashBtn.BackgroundColor = Color.Gray;
                }
                
            };

            layoutGrid.Children.Add(flashPancake);
            layoutGrid.Children.Add(abortPancake);

            if (In4.DeviceOS == TargetPlatform.iOS)
            {
                layoutGrid.Margin = new Thickness(0,20,0,0);
            }

            qrStack.Children.Add(layoutGrid);
            customOverlay.Children.Add(qrStack);

            scan = new ZXingScannerPage(customOverlay: customOverlay);
            scan.Focus();
            scan.OnScanResult += (result) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (oldResult != result.Text)
                    {
                        oldResult = result.Text.ToString();
                        try
                        {
                            //string[] data = oldResult.Split(',');
                            Thread t = new Thread(delegate () {
                                try
                                {
                                    string html = string.Empty;
                                    string url = @"https://archismarthome.com/api/mapfriend.php?m=" + Mymember + "&d=" + oldResult;

                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                                    request.AutomaticDecompression = DecompressionMethods.GZip;

                                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                                    using (Stream stream = response.GetResponseStream())
                                    using (StreamReader reader = new StreamReader(stream))
                                    {
                                        html = reader.ReadToEnd();

                                    }
                                    Log.Warning("api res",html);
                                    if (html != "Already friendship."&&html != "Token not match.")
                                    {
                                        Log.Warning("api res", "Token not match or Already friendship");

                                        Device.BeginInvokeOnMainThread(async () =>
                                        {
                                            Thread.Sleep(20);
                                            //await DisplayAlert("res", "" + html, "Ok");
                                            //--- clear all data ---
                                            col = 0;
                                            row = 0;
                                            k = 0;
                                            ind = 0;
                                            init = 0;
                                            airCnt = 0;
                                            curCnt = 0;
                                            dimCnt = 0;
                                            lightCnt = 0;
                                            rgbCnt = 0;
                                            sceneCnt = 0; 
                                            powerCnt = 0;

                                            Provider.Homes.Clear();

                                            Provider.timerConnect = 0;

                                            Provider.MEMBER_ID_INDEX.Clear();
                                            Provider.DEVICE_DEVICEID_TYPE.Clear();
                                            Provider.ROOM_LIST.Clear();
                                            Provider.POWER_LIST.Clear();

                                            Provider.HomeControllers.Clear();
                                            Provider.AIR_ID_INDEX.Clear();
                                            Provider.CURTAIN_ID_INDEX.Clear();
                                            Provider.DIM_ID_INDEX.Clear();
                                            Provider.LIGHT_ID_INDEX.Clear();
                                            Provider.POWER_ID_INDEX.Clear();

                                            Provider.AIR_LIST.Clear();
                                            Provider.CURTAIN_LIST.Clear();
                                            Provider.DIMMER_LIST.Clear();
                                            Provider.LIGHT_LIST.Clear();


                                            for (int r = 0; r < gridLayout.RowDefinitions.Count; r++)
                                            {
                                                gridLayout.RowDefinitions.RemoveAt(0);
                                            }
                                            for (int c = 0; c < gridLayout.ColumnDefinitions.Count; c++)
                                            {
                                                gridLayout.ColumnDefinitions.RemoveAt(0);
                                            }

                                            /* max = 2;
                                             gridLayout.RowDefinitions.Add(new RowDefinition());
                                             gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                             gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                             if (In4.DevicePlatform == DeviceIdiom.Tablet)
                                             {
                                                 gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                                 gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                                 gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                                 max = 5;
                                             }
                                            */
                                            Provider.reqFriendInfo();

                                            //zXingScanner.IsAnalyzing = false;
                                            //zXingScanner.IsScanning = false;
                                            
                                            scan.IsAnalyzing = false;
                                            scan.IsScanning = false;

                                            await Navigation.PopModalAsync();

                                        });
                                    }
                                    else
                                    {
                                        Device.BeginInvokeOnMainThread(async () =>
                                        {
                                            await PopupNavigation.Instance.PushAsync(new DialogBox("Qr code", "Qr code Error."));
                                        });
                                        Log.Warning("api ress", "not match");
                                    }


                                    //-------------------
                                }
                                catch (Exception error)
                                {
                                    Log.Warning("error api", "" + error);
                                }
                            });
                            t.Start();
                            /*await Task.Run(async () =>
                            {
                                try
                                {
                                    string html = string.Empty;
                                    string url = @"https://archismarthome.com/api/mapfriend.php?m=" + Mymember + "&d=" + oldResult;

                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                                    request.AutomaticDecompression = DecompressionMethods.GZip;

                                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                                    using (Stream stream = response.GetResponseStream())
                                    using (StreamReader reader = new StreamReader(stream))
                                    {
                                        html = reader.ReadToEnd();

                                    }
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        Thread.Sleep(20);
                                        //await DisplayAlert("res", "" + html, "Ok");
                                        //--- clear all data ---
                                        col = 0;
                                        row = 0;
                                        k = 0;
                                        ind = 0;
                                        init = 0;
                                        airCnt = 0;
                                        curCnt = 0;
                                        dimCnt = 0;
                                        lightCnt = 0;
                                    
                                        Provider.Homes.Clear();

                                        Provider.timerConnect = 0;

                                        Provider.MEMBER_ID_INDEX.Clear();
                                        Provider.DEVICE_DEVICEID_TYPE.Clear();
                                        Provider.ROOM_LIST.Clear();

                                        Provider.HomeControllers.Clear();
                                        Provider.AIR_ID_INDEX.Clear();
                                        Provider.CURTAIN_ID_INDEX.Clear();
                                        Provider.DIM_ID_INDEX.Clear();
                                        Provider.LIGHT_ID_INDEX.Clear();

                                        Provider.AIR_LIST.Clear();
                                        Provider.CURTAIN_LIST.Clear();
                                        Provider.DIMMER_LIST.Clear();
                                        Provider.LIGHT_LIST.Clear();


                                        for (int r = 0; r < gridLayout.RowDefinitions.Count; r++)
                                        {
                                            gridLayout.RowDefinitions.RemoveAt(0);
                                        }
                                        for (int c = 0; c < gridLayout.ColumnDefinitions.Count; c++)
                                        {
                                            gridLayout.ColumnDefinitions.RemoveAt(0);
                                        }
                                        max = 2;
                                        gridLayout.RowDefinitions.Add(new RowDefinition());
                                        gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                        gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                        if (In4.DevicePlatform == DeviceIdiom.Tablet)
                                        {
                                            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                                            max = 5;
                                        }

                                        Provider.reqFriendInfo();

                                        scan.IsAnalyzing = false;
                                        scan.IsScanning = false;

                                        await Navigation.PopModalAsync();

                                    });


                                    //-------------------
                                }
                                catch (Exception error)
                                {
                                    Log.Warning("error api",""+error);
                                }
                                
                            });*/
                        }
                        catch (Exception error)
                        {
                            Log.Warning("Error scanner:", "" + error.Message);
                            //PopupNavigation.Instance.PushAsync(new Dialogs.MessageDialog("Error", ""+ error.Message));
                            //await DisplayAlert("Error qrrrrr", "Error:" + error.Message, "Close");
                        }

                        scan.IsAnalyzing = true;
                        scan.IsScanning = true;
                    }
                    //await Navigation.PopModalAsync();
                    //scan.IsScanning = true;
                    //scan.IsAnalyzing = true;
                });
            };

            await Navigation.PushModalAsync(scan);
            addPress = 0;
        }

        private void Client_OnFriendInformation(IOTClient client, FriendInformationResult friendInformation)
        {
            Log.Warning("frnd", "");
            try
            {
                var info = (FriendInformation)friendInformation;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (init == 0)//--- init first times ----
                    {    
                        try
                        {
                            foreach (var memberKV in info.Member)
                            {
                                airCnt = 0;
                                curCnt = 0;
                                dimCnt = 0;
                                lightCnt = 0;
                                rgbCnt = 0;
                                powerCnt = 0;
                                var member = memberKV.Value;
                                //btn.Text = member.Name;
                                Log.Warning("Member",""+ member.Name);
                                Home home = new Home
                                {
                                    Name = member.Name,
                                    MemberID = memberKV.Key,
                                    Status = member.Status == OnlineStatus.Online ? 1 : 0,
                                    //ImageIcon = Convert.ToInt32(member.Img),
                                    ImageIcon = (member.Img==""?1: Convert.ToInt32(member.Img)),
                                    TabIndex = k
                                };
                                home.SetName(member.Name);
                                home.IsOnline(home.Status);
                                home.SetImageIcon(home.ImageIcon);
                                home.ButtonClicked += async (sender, arg) =>
                                {
                                    //await Navigation.PushModalAsync(new ContentPage());
                                    //await Navigation.PushModalAsync(new RoomPage(home.TabIndex));
                                    //await Navigation.PushModalAsync(new ControllerPage(home.TabIndex));
                                    if (home.ClickToHome == 0)
                                    {
                                        home.ClickToHome = 1;
                                        await Navigation.PushModalAsync(new RoomCategory(home.TabIndex));
                                        home.ClickToHome = 0;
                                    }
                                    
                                    //await Navigation.PushModalAsync(Provider.HomeControllers[home.TabIndex]);
                                    //DisplayAlert("Alert", "Add", "OK");
                                };
                                Provider.MEMBER_ID_INDEX.Add(memberKV.Key, k);

                                Dictionary<int, int> DEV_TYPE = new Dictionary<int, int>();

                                List<Room> ROOM = new List<Room>();

                                List<Power> POWER = new List<Power>();
                                Dictionary<int, int> POWER_IDX = new Dictionary<int, int>();

                                List<Aircondition> AIR_DEV = new List<Aircondition>();
                                Dictionary<int, int> AIR_IDX = new Dictionary<int, int>();

                                List<Curtain> CUR_DEV = new List<Curtain>();
                                Dictionary<int, int> Cur_IDX = new Dictionary<int, int>();

                                List<Dimmer> DIM_DEV = new List<Dimmer>();
                                Dictionary<int, int> DIM_IDX = new Dictionary<int, int>();

                                List<Light> LIGHT_DEV = new List<Light>();
                                Dictionary<int, int> LIGHT_IDX = new Dictionary<int, int>();
                                
                                List<RGB> RGB_DEV = new List<RGB>();
                                Dictionary<int, int> RGB_IDX = new Dictionary<int, int>();

                                List<SceneButton> SCENE_DEV = new List<SceneButton>();
                                Dictionary<int, int> SCENE_IDX = new Dictionary<int, int>();

                                

                                foreach (var deviceKV in memberKV.Value.Device)
                                {

                                    DEV_TYPE.Add(deviceKV.Key, deviceKV.Value.DeviceStyleID);
                                    switch (deviceKV.Value.DeviceStyleID)
                                    {
                                        case 1:
                                            Aircondition air = new Aircondition
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };
                                            air.SetName(air.Name);
                                            
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        air.Online = (int)control.Value;
                                                        air.SetOnline(air.Online);
                                                        break;
                                                    case 1:
                                                        air.Status = (int)control.Value;
                                                        air.SetStatus(air.Status);
                                                        break;
                                                    case 2:
                                                        air.Temperature = (int)control.Value;
                                                        air.SetTemp(air.Temperature);
                                                        break;
                                                    case 3:
                                                        air.Speed = (int)control.Value;
                                                        air.SetSpeed(air.Speed);
                                                        break;
                                                    case 4:
                                                        air.TempMin = (int)control.Value;
                                                        break;
                                                    case 5:
                                                        air.TempMax = (int)control.Value;
                                                        break;
                                                    case 100:
                                                        air.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            air.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new AircinditionPopupControl(air));
                                            };
                                            air.QuickClicked += async (obj, arg) =>
                                            {
                                                air.Status ^= 1;
                                                Provider.SendDeviceControl(home.MemberID, air.DeviceID, 1, air.Status);
                                                air.SetStatus(air.Status);
                                            };
                                            AIR_DEV.Add(air);
                                            AIR_IDX.Add(deviceKV.Key, airCnt);
                                            airCnt++;
                                            break;
                                        case 2:
                                            Curtain curtain = new Curtain
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };
                                            curtain.SetName(curtain.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        curtain.Online = (int)control.Value;
                                                        curtain.SetOnline(curtain.Online);
                                                        break;
                                                    case 1:
                                                        curtain.Status1 = (int)control.Value;
                                                        curtain.SetStatus1(curtain.Status1);
                                                        break;
                                                    case 2:
                                                        curtain.Status2 = (int)control.Value;
                                                        curtain.SetStatus2(curtain.Status2);
                                                        break;
                                                    case 100:
                                                        curtain.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            curtain.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new CurtainPopupControl(curtain));
                                            };
                                            CUR_DEV.Add(curtain);
                                            Cur_IDX.Add(deviceKV.Key, curCnt);
                                            curCnt++;
                                            break;
                                        case 3:
                                            Dimmer dimmer = new Dimmer
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };

                                            dimmer.SetName(dimmer.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        dimmer.Online = (int)control.Value;
                                                        dimmer.SetOnline(dimmer.Online);
                                                        break;
                                                    case 1:
                                                        dimmer.Dimvalue = (int)control.Value;
                                                        dimmer.SetDim(dimmer.Dimvalue);
                                                        break;
                                                    case 100:
                                                        dimmer.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            dimmer.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new DimmerPopupControl(dimmer));
                                            };
                                            dimmer.QuickClicked += async (obj, arg) =>
                                            {
                                                dimmer.Dimvalue = dimmer.Dimvalue == 0 ? 100 : 0;
                                                Provider.SendDeviceControl(home.MemberID, dimmer.DeviceID, 1, dimmer.Dimvalue);
                                                dimmer.SetDim(dimmer.Dimvalue);
                                            };
                                            DIM_DEV.Add(dimmer);
                                            DIM_IDX.Add(deviceKV.Key, dimCnt);
                                            dimCnt++;
                                            break;
                                        case 4:
                                            Light light = new Light
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                            };
                                            light.ButtonClicked += delegate
                                            {
                                                light.Status ^= 1;
                                                light.SetStatus(light.Status);
                                                Provider.SendDeviceControl(home.MemberID, light.DeviceID, 1, light.Status);
                                            };
                                            light.SetName(light.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        light.Online = (int)control.Value;
                                                        light.SetOnline(light.Online);
                                                        break;
                                                    case 1:
                                                        light.Status = (int)control.Value;
                                                        light.SetStatus(light.Status);
                                                        break;
                                                    case 100:
                                                        light.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            LIGHT_DEV.Add(light);
                                            LIGHT_IDX.Add(deviceKV.Key, lightCnt);
                                            lightCnt++;
                                            break;
                                        case 5:
                                            RGB rgb = new RGB
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };
                                            rgb.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new RGBPopupControl(rgb));
                                            };
                                            rgb.SetName(rgb.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        rgb.Online = (int)control.Value;
                                                        rgb.SetOnline(rgb.Online);
                                                        break;
                                                    case 1:
                                                        rgb.Status = (int)control.Value;
                                                        rgb.SetRGB(rgb.Status);
                                                        break;
                                                    case 100:
                                                        rgb.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            RGB_DEV.Add(rgb);
                                            RGB_IDX.Add(deviceKV.Key, rgbCnt);
                                            rgbCnt++;
                                            break;

                                        case 10:
                                            SceneButton scene = new SceneButton
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                //DeviceID = deviceKV.Key,
                                                DeviceID = 5000
                                            };
                                            
                                            scene.SetName(scene.Name);

                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        //scene.Online = (int)control.Value;
                                                        //scene.SetEnable(control.Value==1?true:false);
                                                        break;
                                                    case 1:
                                                        scene.SceneID = (int)control.Value;
                                                        //--- which master or scene ---
                                                        break;
                                                    case 2:
                                                        //scene.SetPress(control.Value==1?true:false);
                                                        //--- status of master
                                                        scene.Status = (int)control.Value;
                                                        break;
                                                    case 3:
                                                        scene.SetIcon((int)control.Value);
                                                        break;
                                                    case 4:
                                                        scene.Type = (int)control.Value;
                                                        break;
                                                    case 100:
                                                        scene.Room = (int)control.Value;
                                                        break;
                                                }
                                            }

                                            scene.ButtonClicked += delegate
                                            {
                                                //Provider.SendDeviceControl(home.MemberID, 5000, 1, scene.SceneID);
                                                if (scene.Type == 1)//-- master
                                                {
                                                    Provider.SendDeviceControl(home.MemberID, scene.DeviceID, (scene.Status==1?1:2), scene.SceneID);//--- same device = 5000
                                                }
                                                else if(scene.Type == 2)//-- scene
                                                {
                                                    Provider.SendDeviceControl(home.MemberID, scene.DeviceID, 3, scene.SceneID);//--- same device = 5000
                                                }

                                            };
                                            SCENE_DEV.Add(scene);
                                            SCENE_IDX.Add(deviceKV.Key, sceneCnt);
                                            sceneCnt++;
                                            break;
                                        case 13:
                                            
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        home.DNDMURVisible = true;
                                                        break;
                                                    case 1:
                                                        home.SetDNDMUR((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 20:
                                            Power power = new Power {MemberID = home.MemberID, DeviceID = deviceKV.Key };
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        power.Online = (int)control.Value;
                                                        home.EnablePowerConsumption((power.Online==1?true:false));
                                                        power.SetOnline(power.Online);
                                                        break;
                                                    case 1:
                                                        //power.Voltage = control.Value;
                                                        power.SetVoltage(control.Value);
                                                        break;
                                                    case 2:
                                                        //power.Current = control.Value;
                                                        power.SetCurrent(control.Value);
                                                        break;
                                                    case 3:
                                                        //power.Watt = control.Value;
                                                        power.SetWatt(control.Value);
                                                        break;
                                                    case 4:
                                                        //power.PowerFactor = control.Value;
                                                        power.SetPowerFactor(control.Value);
                                                        break;
                                                    case 5:
                                                        //power.Energy = control.Value;
                                                        power.SetEnergy(control.Value);
                                                        break;
                                                    case 6:
                                                        //power.Frequency = control.Value;
                                                        power.SetFrequency(control.Value);
                                                        break;
                                                }
                                                /*if (controlKV.Key>9)
                                                {
                                                    //power.Energy = control.Value;
                                                    power.SetEnergy(control.Value);
                                                }*/
                                                
                                            }
                                            POWER.Add(power);
                                            POWER_IDX.Add(power.DeviceID, powerCnt);
                                            powerCnt++;
                                            break;
                                        case 100:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                ROOM.Add(new Room { RoomID = (int)controlKV.Key, Name = control.Label ,RoomIcon = (int)control.Value});
                                            }
                                            break;
                                    }
                                }

                                //--- Add to provider ---
                                Provider.DEVICE_DEVICEID_TYPE.Add(DEV_TYPE);

                                Provider.AIR_LIST.Add(AIR_DEV);
                                Provider.AIR_ID_INDEX.Add(AIR_IDX);

                                Provider.CURTAIN_LIST.Add(CUR_DEV);
                                Provider.CURTAIN_ID_INDEX.Add(Cur_IDX);

                                Provider.DIMMER_LIST.Add(DIM_DEV);
                                Provider.DIM_ID_INDEX.Add(DIM_IDX);

                                Provider.LIGHT_LIST.Add(LIGHT_DEV);
                                Provider.LIGHT_ID_INDEX.Add(LIGHT_IDX);

                                Provider.RGB_LIST.Add(RGB_DEV);
                                Provider.RGB_ID_INDEX.Add(RGB_IDX);

                                Provider.Scene_LIST.Add(SCENE_DEV);
                                Provider.Scene_ID_INDEX.Add(SCENE_IDX);

                                Provider.ROOM_LIST.Add(ROOM);

                                Provider.POWER_LIST.Add(POWER); ;
                                Provider.POWER_ID_INDEX.Add(POWER_IDX); 

                                if (k % max == 0 && k > 1)
                                {
                                    row++;
                                    //gridLayout.RowDefinitions.Add(new RowDefinition());
                                }

                                if (col == max)
                                {
                                    col = 0;
                                }

                                Provider.Homes.Add(home);
                                gridLayout.Children.Add(home, col, row);
                                k++;
                                col++;
                                
                            }


                            Provider.Analyze();
                        }
                        catch(Exception er){
                            Log.Warning("errror on friend", ""+er);
                        }
                        init = 1;
                    }
                    else//--- update after offline ---
                    {
                        try
                        {
                            k = 0;
                            foreach (var memberKV in info.Member)
                            {
                                var member = memberKV.Value;
                                //btn.Text = member.Name;
                                Log.Warning("Member", "" + member.Name);
                                Home home = new Home
                                {
                                    Name = member.Name,
                                    MemberID = memberKV.Key,
                                    Status = member.Status == OnlineStatus.Online ? 1 : 0,
                                    //ImageIcon = Convert.ToInt32(member.Img),
                                    ImageIcon = (member.Img == "" ? 1 : Convert.ToInt32(member.Img)),
                                    TabIndex = k
                                };


                                foreach (var deviceKV in memberKV.Value.Device)
                                {

                                    switch (deviceKV.Value.DeviceStyleID)
                                    {
                                        case 1:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        Provider.AIR_LIST[home.TabIndex][Provider.AIR_ID_INDEX[home.TabIndex][deviceKV.Key]].SetOnline((int)control.Value);
                                                        break;
                                                    case 1:
                                                        Provider.AIR_LIST[home.TabIndex][Provider.AIR_ID_INDEX[home.TabIndex][deviceKV.Key]].SetStatus((int)control.Value);
                                                        break;
                                                    case 2:
                                                        Provider.AIR_LIST[home.TabIndex][Provider.AIR_ID_INDEX[home.TabIndex][deviceKV.Key]].SetTemp((int)control.Value);
                                                        break;
                                                    case 3:
                                                        Provider.AIR_LIST[home.TabIndex][Provider.AIR_ID_INDEX[home.TabIndex][deviceKV.Key]].SetSpeed((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 2:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        Provider.CURTAIN_LIST[home.TabIndex][Provider.CURTAIN_ID_INDEX[home.TabIndex][deviceKV.Key]].SetOnline((int)control.Value);
                                                        break;
                                                    case 1:
                                                        Provider.CURTAIN_LIST[home.TabIndex][Provider.CURTAIN_ID_INDEX[home.TabIndex][deviceKV.Key]].SetStatus1((int)control.Value);
                                                        break;
                                                    case 2:
                                                        Provider.CURTAIN_LIST[home.TabIndex][Provider.CURTAIN_ID_INDEX[home.TabIndex][deviceKV.Key]].SetStatus2((int)control.Value);
                                                        break;
                                                }
                                            }
                                            
                                            break;
                                        case 3:
                                            

                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        Provider.DIMMER_LIST[home.TabIndex][Provider.DIM_ID_INDEX[home.TabIndex][deviceKV.Key]].SetOnline((int)control.Value);
                                                        break;
                                                    case 1:
                                                        Provider.DIMMER_LIST[home.TabIndex][Provider.DIM_ID_INDEX[home.TabIndex][deviceKV.Key]].SetDim((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 4:
                                            
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        Provider.LIGHT_LIST[home.TabIndex][Provider.LIGHT_ID_INDEX[home.TabIndex][deviceKV.Key]].SetOnline((int)control.Value);
                                                        break;
                                                    case 1:
                                                        Provider.LIGHT_LIST[home.TabIndex][Provider.LIGHT_ID_INDEX[home.TabIndex][deviceKV.Key]].SetStatus((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 5:
                                            
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        Provider.RGB_LIST[home.TabIndex][Provider.RGB_ID_INDEX[home.TabIndex][deviceKV.Key]].SetOnline((int)control.Value);
                                                        break;
                                                    case 1:
                                                        Provider.RGB_LIST[home.TabIndex][Provider.RGB_ID_INDEX[home.TabIndex][deviceKV.Key]].SetRGB((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 13:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 1:
                                                        Provider.Homes[home.TabIndex].SetDNDMUR((int)control.Value);
                                                        break;
                                                }
                                            }
                                            break;
                                        case 20:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        //Provider.POWER_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                                        Provider.Homes[home.TabIndex].EnablePowerConsumption(true);
                                                        break;
                                                    case 1:
                                                        //Log.Warning("ctrl", "ctrl 1");
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetVoltage(control.Value);
                                                        break;
                                                    case 2:
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetCurrent(control.Value);
                                                        break;
                                                    case 3:
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetWatt(control.Value);
                                                        break;
                                                    case 4:
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetPowerFactor(control.Value);
                                                        break;
                                                    case 5:
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetEnergy(control.Value);
                                                        break;
                                                    case 6:
                                                        Provider.POWER_LIST[home.TabIndex][Provider.POWER_ID_INDEX[home.TabIndex][deviceKV.Key]].SetFrequency(control.Value);
                                                        //Log.Warning("fq", "type 20 d=:" + (int)deviceControlResult.V);
                                                        break;
                                                }
                                                /*if (controlKV.Key>9)
                                                {
                                                    //power.Energy = control.Value;
                                                    power.SetEnergy(control.Value);
                                                }*/
                                            }
                                            break;
                                    }
                                }
                                k++;
                            }
                        }
                        catch (Exception er)
                        {
                            Log.Warning("errror on friend again", "" + er);
                        }
                    }

                    //sss.Children.Add(homeController);
                    //Navigation.PushModalAsync(homeController);
                    // Navigation.PushModalAsync(new RoomPage(0));
                    //TabbedPage tabbedPage = new TabbedPage();
                    /*List<StackLayout> sss = new List<StackLayout>
                    {
                        MainContentPage,
                        pp1,
                        pp2
                    };

                    mainCarouselView.ItemsSource = sss;*/

                    //mainFrame.Content = MainContentPage;
                    //tabFrame.Children.Add(tabbedPage);
                });
            }
            catch (Exception error)
            {
                Log.Warning("OnFriendInformation Ex", ""+error);
            }
            
        }

        private void Client_OnLogin(IOTClient client, LoginResult loginResult)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                Mymember = loginResult.MemberID;
                Provider.MyMemberID = Mymember;
                Provider.MyMemberName = loginResult.Name;
                header.Text = "Hi "+ loginResult.Name;
                //throw new NotImplementedException(); 
                Log.Warning("logon main aaaaa", "");
                if (loginResult.Status == LoginStatus.Success)
                {
                    Provider.reqFriendInfo();
                }
                else
                {
                    var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                    System.IO.File.WriteAllText(filename, "");

                    DisplayAlert("Error", "Username or password incorrect", "Ok");
                    Log.Warning("Error", "Username or password incorrect");
                    await Navigation.PushModalAsync(new LoginPage());
                }
            });
        }

        protected override bool OnBackButtonPressed()
        {
            //Application.Current.Quit();
            exitApp =  1;
            Environment.Exit(0);
            return base.OnBackButtonPressed();
        }
    }
}
