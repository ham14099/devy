﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ArchiV4
{
    public class ThemeColor
    {
        public Color Header { get; set; }
        public Color Body { get; set; }
        public Color Active { get; set; }
    }
}
