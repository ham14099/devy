﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.PancakeView;
using Xamarin.Forms.Xaml;
using ZXing;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RGBPopupControl : PopupPage
    {
        private SKBitmap bitmap = new SKBitmap(500, 300);
        SKCanvasView canvasView;
        RGB Rgb;
        public RGBPopupControl(RGB rgb)
        {
            InitializeComponent();
            rgbName.Text = rgb.Name;
            Rgb = rgb;
            // Load the bitmap from a resource
            string resourceID = "ArchiV4.Media.colorwheel.png";
            Assembly assembly = GetType().GetTypeInfo().Assembly;

            using (Stream stream = assembly.GetManifestResourceStream(resourceID))
            {
                bitmap = SKBitmap.Decode(stream);
            }

            // Create the SKCanvasView and set the PaintSurface handler
            canvasView = new SKCanvasView() { Scale = 1, HeightRequest = bitmap.Height / In4.ScreenDensity, WidthRequest = bitmap.Width / In4.ScreenDensity, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };
            canvasView.EnableTouchEvents = true;
            canvasView.PaintSurface += OnCanvasViewPaintSurface;
            canvasView.Touch += CanvasView_Touch;
            ColorPickPanel.Children.Add(canvasView);
            ColorPickerArea.Border = new Border { Color = Color.FromHex("#494949"), DrawingStyle = BorderDrawingStyle.Outside, Thickness = 1 };
            ColorPickerArea.CornerRadius = canvasView.HeightRequest / 2;
            ColorPickerArea.HeightRequest = canvasView.HeightRequest;
            ColorPickerArea.WidthRequest = canvasView.WidthRequest;
            SKPoint loc = ConvertToPixel(new SKPoint((float)250, (float)250));
            SetPicker((double)loc.X, (double)loc.Y);
        }
        private void CanvasView_Touch(object sender, SKTouchEventArgs e)
        {
            switch (e.ActionType)
            {
                case SKTouchAction.Pressed:
                    /*if (!inProgressPaths.ContainsKey(args.Id))
                    {
                        SKPath path = new SKPath();
                        path.MoveTo(ConvertToPixel(args.Location));
                        inProgressPaths.Add(args.Id, path);
                        canvasView.InvalidateSurface();
                    }*/
                    try
                    {
                        //var aa = (sender as SKCanvasView);
                        var loc = ConvertToPixel(e.Location);
                        Log.Warning("e.Location X", "" + e.Location.X);
                        Log.Warning("e.Location Y", "" + e.Location.Y);
                        Log.Warning("Pressed X", "" + loc.X);
                        Log.Warning("Pressed Y", "" + loc.Y);
                        SetPicker((double)loc.X, (double)loc.Y);
                        int rr = bitmap.GetPixel((int)loc.X, (int)loc.Y).Red;
                        int gg = bitmap.GetPixel((int)loc.X, (int)loc.Y).Green;
                        int bb = bitmap.GetPixel((int)loc.X, (int)loc.Y).Blue;
                        Log.Warning("Pressed R", "" + rr);
                        Log.Warning("Pressed G", "" + gg);
                        Log.Warning("Pressed B", "" + bb);
                        SetRgb(rr, gg, bb);
                        canvasView.InvalidateSurface();
                    }
                    catch (Exception error)
                    {
                        Log.Warning("error CanvasView_Touch", "" + error);
                    }
                    break;
                case SKTouchAction.Moved:
                    try
                    {
                        //var aa = (sender as SKCanvasView);
                        var loc = ConvertToPixel(e.Location);
                        Log.Warning("Moved X", "" + loc.X);
                        Log.Warning("Moved Y", "" + loc.Y);
                        SetPicker((double)loc.X, (double)loc.Y);
                        int rr = bitmap.GetPixel((int)loc.X, (int)loc.Y).Red;
                        int gg = bitmap.GetPixel((int)loc.X, (int)loc.Y).Green;
                        int bb = bitmap.GetPixel((int)loc.X, (int)loc.Y).Blue;
                        Log.Warning("Moved R", "" + rr);
                        Log.Warning("Moved G", "" + gg);
                        Log.Warning("Moved B", "" + bb);
                        SetRgb(rr, gg, bb);
                        canvasView.InvalidateSurface();
                    }
                    catch (Exception error)
                    {
                        Log.Warning("error CanvasView_Touch", "" + error);
                    }
                    break;
                case SKTouchAction.Released:
                    /*if (inProgressPaths.ContainsKey(args.Id))
                    {
                        completedPaths.Add(inProgressPaths[args.Id]);
                        inProgressPaths.Remove(args.Id);
                        canvasView.InvalidateSurface();
                    }*/

                    break;
            }
        }
        SKPoint ConvertToPixel(SKPoint pt)
        {
            /* Log.Warning(" pt.X", "" + pt.X);
             Log.Warning(" pt.Y", "" + pt.Y);
             Log.Warning(" canvasView.CanvasSize.Width", "" + canvasView.CanvasSize.Width);
             Log.Warning(" canvasView.CanvasSize.Height", "" + canvasView.CanvasSize.Height);
             Log.Warning(" canvasView.Width", "" + canvasView.Width);
             Log.Warning(" canvasView.Height", "" + canvasView.Height);*/
            /*return new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
                               (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));*/
            return pt;
        }

        private void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear();

            float x = (info.Width - bitmap.Width) / 2;
            float y = (info.Height - bitmap.Height) / 2;

            canvas.DrawBitmap(bitmap, x, y);//x,y

            //Log.Warning("info Width", ""+ info.Width);
            //Log.Warning("info Height", ""+ info.Height);
            //Log.Warning("bitmap Width", ""+ bitmap.Width);
            //Log.Warning("bitmap Height", ""+ bitmap.Height);
        }
        void SetPicker(double x, double y)
        {

            pick.TranslationX = (x / In4.ScreenDensity) - pick.Width / 2;
            pick.TranslationY = (y / In4.ScreenDensity) - pick.Width / 2;
        }

        private async void closeBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void powerBtn_Clicked(object sender, EventArgs e)
        {

        }
        private void SetRgb(int r, int g, int b)
        {
            pick.BackgroundColor = Color.FromRgb(r,g, b);

            Rgb.RGBA.R = r;
            Rgb.RGBA.G = g;
            Rgb.RGBA.B = b;
            Provider.SendDeviceControl(Rgb.MemberID, Rgb.DeviceID, 1, RGBToInt(Rgb.RGBA));
        }
        int RGBToInt(ARGB aRGB)
        {
            int res = 0;
            res = ((aRGB.R + 100) * 1000000);
            res += (aRGB.G + 100) * 1000;
            res += aRGB.B + 100;
            return res;
        }
    }
}