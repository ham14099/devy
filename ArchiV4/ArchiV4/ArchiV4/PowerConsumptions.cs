﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchiV4
{
    public class PowerConsumptions
    {
        //public string Power { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public float Unit { get; set; }
    }
}
