﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DimmerPopupControl : PopupPage
    {
        static double dimWidth = 300;
        static double acWidth = 300;
        double ac_stepper = 100;
        double d_stepper = dimWidth / 100;
        Dimmer Dimmers;
        public DimmerPopupControl(Dimmer dimmer)
        {
            InitializeComponent();
            dimName.Text = dimmer.Name;
            Dimmers = dimmer;
            ac_stepper = acWidth / 100;
            SetDim(dimmer.Dimvalue);

            dimmer.DimChangeHandler += Dimmer_DimChangeHandler;
            dimmer.OnlineHandler += DevicesOnlineHandler;
        }
        private void DevicesOnlineHandler(object sender, int e)
        {
            controlPanel.IsEnabled = e == 1 ? true : false;

            powerBtn.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            dimSlideCover.BackgroundColor = e == 1 ? Color.FromHex("#f8f8f8") : Color.FromHex("#d9d9d9");
            dimValLbl.TextColor = e == 1 ? Color.FromHex("#0094fb") : Color.FromHex("#d9d9d9");
            //mainPancake.BackgroundColor = e == 1 ? Color.White : Color.FromHex("#d0d0d0");
        }
        private void Dimmer_DimChangeHandler(object sender, int e)
        {
            SetDim(e);
        }

        private void SetDim(int dim)
        {
            dimValLbl.Text = dim + "";
            dimSlider.Value = dim;
            dimSlideCover.WidthRequest = acWidth - (dim * ac_stepper);
            powerIcon.Source = Dimmers.Dimvalue > 0 ? "on_64x64.png" : "off_64x64.png";
        }

        private async void closeBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void acSlider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            dimSlideCover.WidthRequest = acWidth - ((sender as Slider).Value * ac_stepper);
            dimValLbl.Text = (int)(sender as Slider).Value + "";
        }

        private void acSlider_DragCompleted(object sender, EventArgs e)
        {
            Dimmers.Dimvalue = (int)(sender as Slider).Value;
            SetDim(Dimmers.Dimvalue);
            
            Provider.SendDeviceControl(Dimmers.MemberID, Dimmers.DeviceID, 1, Dimmers.Dimvalue);
        }

        private void powerBtn_Clicked(object sender, EventArgs e)
        {
            Dimmers.Dimvalue = (Dimmers.Dimvalue > 0 ? 0 : 100);
            SetDim(Dimmers.Dimvalue);
            Provider.SendDeviceControl(Dimmers.MemberID, Dimmers.DeviceID, 1, Dimmers.Dimvalue);
        }
    }
}