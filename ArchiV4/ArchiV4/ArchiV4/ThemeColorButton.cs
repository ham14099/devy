﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class ThemeColorButton:PancakeView
    {
        
        private Frame HeaderFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private Frame BGHeaderFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private Frame BodyFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private Frame ActiveFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private Frame TextItemActiveFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private Frame TextItemFrame = new Frame
        {
            Padding = 0,
            HeightRequest = 20,
            CornerRadius = 0,
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            InputTransparent = true
        };
        private StackLayout colorStack = new StackLayout
        {
            Spacing = 0,
            InputTransparent = true
        };
        private ImageButton palateBtn = new ImageButton
        {
            HorizontalOptions = LayoutOptions.Fill,
            VerticalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White
        };
        private Grid gridLayout = new Grid();

        public event EventHandler ButtonClicked;

        public ThemeColorButton()
        {
            HeightRequest = 80;
            WidthRequest = 50;
            CornerRadius = 5;
            this.Border = new Border { Thickness = 1, Color = Color.FromHex("#505050"), DrawingStyle = BorderDrawingStyle.Outside };
            /*BorderColor = Color.FromHex("#505050");
            BorderThickness = 1;
            BorderDrawingStyle = BorderDrawingStyle.Outside;*/

            colorStack.Children.Add(HeaderFrame);
            colorStack.Children.Add(BGHeaderFrame);
            colorStack.Children.Add(BodyFrame);
            colorStack.Children.Add(ActiveFrame);
            colorStack.Children.Add(TextItemActiveFrame);
            colorStack.Children.Add(TextItemFrame);

            palateBtn.Clicked += PalateBtn_Clicked;
            gridLayout.Children.Add(palateBtn);
            gridLayout.Children.Add(colorStack);
            
            Content = gridLayout;
        }

        private void PalateBtn_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetPress(bool press)
        {
            this.Border = new Border { Thickness = (press==true?4:1), Color = Color.FromHex("#505050"), DrawingStyle = BorderDrawingStyle.Outside };
        }
        public void SetColor(string header,string bgheader, string body, string active, string textitemactive, string textitems)
        {
            HeaderFrame.BackgroundColor = Color.FromHex(header);
            BGHeaderFrame.BackgroundColor = Color.FromHex(bgheader);
            BodyFrame.BackgroundColor = Color.FromHex(body);
            ActiveFrame.BackgroundColor = Color.FromHex(active);
            TextItemActiveFrame.BackgroundColor = Color.FromHex(textitemactive);
            TextItemFrame.BackgroundColor = Color.FromHex(textitems);
        }
    }
}
