﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushNoti
{
    public interface IDevice
    {
        string GetIdentifier();
    }
}
