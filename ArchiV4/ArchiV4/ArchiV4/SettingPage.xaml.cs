﻿using NucksooIOTCommon;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage : ContentPage
    {
        public SettingPage()
        {
            InitializeComponent();
            

            if (Device.OS == TargetPlatform.iOS)
            {
                container.Margin = new Thickness(0, 20, 0, 0);
            }
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void logoutBtn_Clicked(object sender, EventArgs e)
        {
            var res = await DisplayAlert("Logout","Are you sure","Logout","Cancel");

            if (res == true)
            {
                //Provider.Client.Logout();
                Provider.DisConnect();
                var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                System.IO.File.WriteAllText(filename, "");
                
                Log.Warning("(3X0001222)", "File Writer");

                //await Navigation.PushModalAsync(new LoginPage());
                //Application.Current.Quit();
                Environment.Exit(0);
            }
        }

        private void themeSetting_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ThemeSettingPage());
        }

        private async void profileBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new ProfileViewPopup());
        }

        private void privacyBtn_Clicked(object sender, EventArgs e)
        {
            Uri uri = new Uri("https://archismarthome.com/policy.php#policy");
            OpenBrowser(uri);
        }
        public async void OpenBrowser(Uri uri)
        {
            //await Browser.OpenAsync(uri);
            await Browser.OpenAsync(uri, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.AliceBlue,
                PreferredControlColor = Color.DarkBlue
            });
        }

        private void termsBtn_Clicked(object sender, EventArgs e)
        {
            Uri uri = new Uri("https://archismarthome.com/policy.php#terms");
            OpenBrowser(uri);
        }

        private void helpBtn_Clicked(object sender, EventArgs e)
        {
            Uri uri = new Uri("https://archismarthome.com/help.php");
            OpenBrowser(uri);
        }

        private void contactBtn_Clicked(object sender, EventArgs e)
        {
            Uri uri = new Uri("https://archismarthome.com/help.php#contact");
            OpenBrowser(uri);
        }

        private async void aboutBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new AboutViewPopup());
        }
    }
}