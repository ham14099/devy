﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchiV4
{
    public class EnergyDetail
    {
        public double Energy { get; set; }
        public int Index { get; set; }
    }
}
