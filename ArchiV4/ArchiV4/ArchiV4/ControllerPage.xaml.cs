﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ControllerPage : ContentPage
    {
        int index;
        public ControllerPage(int homeIndex,int roomIndex)
        {
            InitializeComponent();
            index = homeIndex;
            homeName.Text = Provider.HomeControllers[homeIndex].roomLayouts[roomIndex].Name;
            //DisplayAlert("HomeControllers[homeIndex].roomLayouts", "="+ Provider.HomeControllers[homeIndex].roomLayouts.Count, "OK");
            controlLayout.Content = Provider.HomeControllers[homeIndex].roomLayouts[roomIndex];
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}