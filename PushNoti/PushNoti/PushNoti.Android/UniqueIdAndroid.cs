﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PushNoti.Droid;
using Xamarin.Forms;
using static Android.Gms.Common.Apis.GoogleApi;

[assembly: Xamarin.Forms.Dependency(typeof(UniqueIdAndroid))]
namespace PushNoti.Droid
{
    public class UniqueIdAndroid : IDevice
    {
        [Obsolete]
        public string GetIdentifier()
        {
            //return Settings.DefaultSettings.JniIdentityHashCode.ToString();

            return Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            //return Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Global.DeviceProvisioned);
            //return Settings.Secure.GetString(Forms.Context.ContentResolver, Settings.Secure.AndroidId);
        }
    }
}