﻿using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace GateWayMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        IOTClient Client = new IOTClient();
        public LoginPage()
        {
            InitializeComponent();
            Client.OnLogin += Client_OnLogin;
        }
        private void Client_OnLogin(IOTClient client, LoginResult loginResult)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    Log.Warning("logon[login form]", "");
                    if (loginResult.Status == LoginStatus.Success)
                    {

                        var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                        System.IO.File.WriteAllText(filename, "iot,archiiotserver.ddns.net, 20133," + userTxt.Text.ToString() + "," + passTxt.Text.ToString());
                        Log.Warning("(3X0001)", "File Writer");
                        //Navigation.PopModalAsync();
                        //Provider.reqFriendInfo();
                        //Client.Logout();
                        Client.Disconnect();
                        Log.Warning("Yep", "Great");
                        //Provider.Client.Logout();
                        //Provider.DisConnect();

                        //await Navigation.PushModalAsync(new MainPage());
                        //Provider.reqFriendInfo();
                        await Navigation.PushModalAsync(new MainPage());
                        //await Navigation.PopModalAsync();
                        //Provider.Connect(new string[] { "iot", "archiiotserver.ddns.net", 20133 + "", userTxt.Text.ToString(), passTxt.Text.ToString() });
                        //DisplayAlert("Yep", "Great", "Ok");

                    }
                    else if (loginResult.Status != LoginStatus.Success)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            passTxt.Text = string.Empty;

                            Client.Logout();
                            Client.Disconnect();
                            DisplayAlert("Error", "Username or password incorrect!!!", "Ok");
                            Log.Warning("Error", "Username or password incorrect!!!");
                        });
                    }
                }
                catch (Exception error)
                {
                    Log.Warning("Error onlogin[login form]", "" + error);
                }

            });
        }

        private void LoginBtn_Clicked(object sender, EventArgs e)
        {
            if (userTxt.Text == null)
            {
                DisplayAlert("Warning", "Enter Username", "Ok");
                Log.Warning("Warning", "Enter Username");
            }
            else if (passTxt.Text == null)
            {
                DisplayAlert("Warning", "Enter Password", "Ok");
                Log.Warning("Warning", "Enter Password");
            }
            else
            {
                //Client = new IOTClient();
                Client.Connect("archiiotserver.ddns.net", 20133, userTxt.Text.ToString(), passTxt.Text.ToString());

            }
        }
        private void forgetBtn_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("ummm", "forget", "Close");
        }
    }
}