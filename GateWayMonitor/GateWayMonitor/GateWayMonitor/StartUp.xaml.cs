﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace GateWayMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartUp : ContentPage
    {
        public StartUp()
        {
            InitializeComponent();
            var filename = "";
            string dataread = "";
            try
            {
                filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                using (var reader = new StreamReader(filename))
                {
                    dataread = reader.ReadToEnd();
                    string[] data = dataread.Split(',');
                    if (data.Length > 4)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //Provider.Client = new IOTClient();

                            /*Log.Warning("main", "lng >4:" + dataread);
                            var res = Provider.Connect(data);
                            Log.Warning("main", "cnct");
                            if (res == true)
                            {
                                Log.Warning("main", "cncted");
                                //Provider.reqFriendInfo();
                                await Navigation.PushModalAsync(new MainPage());
                            }*/

                            await Navigation.PushModalAsync(new MainPage());

                        });
                    }
                    else if (dataread.Trim() == "")
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //DisplayAlert("title", "invalid data", "CLOSE");
                            await Navigation.PushModalAsync(new LoginPage());
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //DisplayAlert("title", "found but invalid", "CLOSE");
                            await Navigation.PushModalAsync(new LoginPage());
                        });
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Log.Warning("Error(1X0001)", "Error : " + e.Message);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //DisplayAlert("title", "not found", "OK");
                    await Navigation.PushModalAsync(new LoginPage());
                    //await Navigation.PushModalAsync(new HomePage());
                });
            }
            catch (Exception error)
            {
                Log.Warning("Error(1X0002)", "Error : " + error);
            }
        }
    }
}