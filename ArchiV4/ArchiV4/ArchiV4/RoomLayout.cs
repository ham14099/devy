﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ArchiV4
{
    public class RoomLayout:StackLayout
    {
        public string Name { get; set; }
        public int Icon { get; set; }
        public List<Aircondition> Airconditions = new List<Aircondition>();
        public List<Curtain> Curtains = new List<Curtain>();
        public List<Dimmer> Dimmers = new List<Dimmer>();
        public List<Light> Lights = new List<Light>();
        public List<RGB> RGBs = new List<RGB>();
        public List<SceneButton> Scenes = new List<SceneButton>();
        public int DevicesQuantity = 0;
        public int SceneQuantity = 0;
        private int countButton = 0;
        private int row = 0;
        private int col = 0;


        ScrollView scrollGrid = new ScrollView
        {
            Orientation = ScrollOrientation.Vertical,
            VerticalScrollBarVisibility = ScrollBarVisibility.Never,
            Padding = 0,
        };

        Grid grid = new Grid
        {
            ColumnSpacing = 10,
            RowSpacing = 10,
            Margin = new Thickness(10,0,10,0),
            BackgroundColor = Color.Transparent
        };

        Label roomName = new Label
        {
            TextColor = Color.FromHex("#11376d"),
            HorizontalOptions = LayoutOptions.Center,
            FontSize = 20,
            Style = (Style)Application.Current.Resources["FontTextBold"],
            Margin = new Thickness(0, 10, 0, 10),
            MaxLines = 1,
            LineBreakMode = LineBreakMode.TailTruncation
        };

        /*Frame seperator = new Frame
        {
            HorizontalOptions = LayoutOptions.Fill,
            HeightRequest = 1,
            BackgroundColor = Color.FromHex("#494949"),
            Padding = 0,
            CornerRadius = 1,
            Margin = new Thickness(20,10,20,10),
            HasShadow = false
        };*/

        ScrollView sceneScroll = new ScrollView
        {
            Orientation = ScrollOrientation.Horizontal,
            HorizontalScrollBarVisibility = ScrollBarVisibility.Never,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HeightRequest = In4.SceneButtonWidth/2
        };

        FlexLayout flex = new FlexLayout
        {

            VerticalOptions = LayoutOptions.FillAndExpand,
            HeightRequest = In4.SceneButtonWidth / 2
        };

        StackLayout sceneLayout = new StackLayout
        {
            Orientation = StackOrientation.Horizontal,
            Padding = new Thickness(0,0,10,0),
            VerticalOptions = LayoutOptions.FillAndExpand,
            Spacing = 0            
        };

        Label sceneLbl = new Label
        {
            Text = "Scene",
            HorizontalOptions = LayoutOptions.Start,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(10, 0, 0, 0),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = (Color)Application.Current.Resources["TextItem"],
            Style = (Style)Application.Current.Resources["FontTextBold"],
        };
        Label deviceLbl = new Label
        {
            Text = "Devices",
            HorizontalOptions = LayoutOptions.Start,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(10, 10, 0, 0),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = (Color)Application.Current.Resources["TextItem"],
            Style = (Style)Application.Current.Resources["FontTextBold"],
        };

        public RoomLayout()
        {
            BackgroundColor = Color.Transparent;
            Spacing = 0;
            Padding = 0;
            Margin = new Thickness(0,0,0,0);
            HorizontalOptions = LayoutOptions.Fill;

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());

            if (In4.DevicePlatform == DeviceIdiom.Tablet)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            roomName.SetDynamicResource(Label.TextColorProperty, "TextItemActive");
            sceneLbl.SetDynamicResource(Label.TextColorProperty, "TextItemActive");
            deviceLbl.SetDynamicResource(Label.TextColorProperty, "TextItemActive");

            //Children.Add(seperator);
            Children.Add(roomName);
            Children.Add(sceneLbl);
            flex.Children.Add(sceneScroll);
            //--- add scens here ---
            sceneScroll.Content = sceneLayout;

            Children.Add(flex);
            Children.Add(deviceLbl);
            scrollGrid.Content = grid;
            Children.Add(scrollGrid);
            IsVisible = false;
        }

        public void SetName(string name)
        {
            roomName.Text = name;
        }

        public void AddAircondition(Aircondition aircondition)
        {
            checkRow();
            Airconditions.Add(aircondition);
            grid.Children.Add(aircondition, col, row);
            col++;
        }

        public void AddCurtain(Curtain curtain)
        {
            checkRow();
            Curtains.Add(curtain);
            grid.Children.Add(curtain, col, row);
            col++;
        }

        public void AddDimmer(Dimmer dimmer)
        {
            checkRow();
            Dimmers.Add(dimmer);
            grid.Children.Add(dimmer, col, row);
            col++;
        }

        /*public void ShowSeperator(bool show)
        {
            seperator.IsVisible = show;
        }*/

        public void AddScene(SceneButton sceneButton)
        {
            sceneLayout.Children.Add(sceneButton);
            Scenes.Add(sceneButton);
        }
        

        public void AddLight(Light light)
        {
            checkRow();
            Lights.Add(light);
            grid.Children.Add(light, col, row);
            col++;
        }

        public void AddRGB(RGB rgb)
        {
            checkRow();
            RGBs.Add(rgb);
            grid.Children.Add(rgb, col, row);
            col++;
        }

        private void checkRow()
        {
            if (countButton % In4.sceneMaxForRow == 0 && countButton > 1)
            {
                row++;
                grid.RowDefinitions.Add(new RowDefinition());
            }

            if (col == In4.sceneMaxForRow)
            {
                col = 0;
            }
            countButton++;
        }
        public void SceneLblShow(bool show)
        {
            if (show == false) {
                Children.Remove(sceneLbl); 
                Children.Remove(flex);
                deviceLbl.Margin = new Thickness(10,0,0,0);
            }
        }
        public void DeviceLblShow(bool show)
        {
            if (show == false) {
                Children.Remove(deviceLbl); 
                Children.Remove(scrollGrid);
            }
        }
    }
}
