﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class Curtain: PancakeView
    {
        public int MemberID { get; set; }
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Status1 { get; set; }
        public int Status2 { get; set; }
        public int Room { get; set; }
        public event EventHandler ButtonClicked;

        public event EventHandler<int> Curtain1StatusHandler;
        public event EventHandler<int> Curtain2StatusHandler;
        public void OnCurtain1ChangeHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnCurtain1Change(Status1);
            }
            catch (Exception ex)
            {
                OnCurtain1Change(Status1);
            }
        }
        public void OnCurtain2ChangeHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnCurtain2Change(Status2);
            }
            catch (Exception ex)
            {
                OnCurtain2Change(Status2);
            }
        }
        
        protected virtual void OnCurtain1Change(int status1)
        {
            Curtain1StatusHandler?.Invoke(this, status1);
        }
        protected virtual void OnCurtain2Change(int status2)
        {
            Curtain2StatusHandler?.Invoke(this, status2);
        }
        
        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        Image bgBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth/2,
            WidthRequest = In4.SceneButtonWidth/2,
            Source = "curtain_close_64x64.png",
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(0,20,0,0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Center,
        };


        public event EventHandler<int> OnlineHandler;
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }
        public void SetOnline(int online)
        {
            Online = online;
            buttonFrm.BackgroundColor = Online == 1 ? Color.White : (Color)Application.Current.Resources["Active"];
            curname.TextColor = Online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];
            buttonFrm.IsEnabled = Online == 1 ? true : false;
            OnOnlineHandler();
        }


        Label curname = new Label
        {
            Text = "Name",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.FromHex("#494949"),
            Style = (Style)Application.Current.Resources["FontText"],
        };

        StackLayout statusContainer = new StackLayout
        {
            VerticalOptions = LayoutOptions.Center,
            InputTransparent = true,
        };
        Label statusLbl1 = new Label
        {
            Text = "STOP",
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            TextColor = Color.FromHex("#05e1a4"),
            Style = (Style)Application.Current.Resources["FontText"],
        };
        Label statusLbl2 = new Label
        {
            Text = "STOP",
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            TextColor = Color.FromHex("#05e1a4"),
            Style = (Style)Application.Current.Resources["FontText"],
        };
        

        public Curtain()
        {
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;


            statusContainer.Children.Add(statusLbl1);
            statusContainer.Children.Add(statusLbl2);

            gridFrm.Children.Add(buttonFrm);
            gridFrm.Children.Add(bgBtn);
            //gridFrm.Children.Add(statusContainer);
            gridFrm.Children.Add(curname);
            Content = gridFrm;
        }
        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            curname.Text = name;
        }

        public void SetStatus1(int status)
        {
            Status1 = status;
            /*switch (Status1)
            {
                case 0:
                    statusLbl1.Text = "STOP";
                    break;
                case 1:
                    statusLbl1.Text = "OPEN";
                    break;
                case 2:
                    statusLbl1.Text = "CLOSE";
                    break;
            }*/
            OnCurtain1ChangeHandler();
        }

        public void SetStatus2(int status)
        {
            Status2 = status;
            /*switch (Status2)
            {
                case 0:
                    statusLbl2.Text = "STOP";
                    break;
                case 1:
                    statusLbl2.Text = "OPEN";
                    break;
                case 2:
                    statusLbl2.Text = "CLOSE";
                    break;
            }*/
            OnCurtain2ChangeHandler();
        }
        
        public void IsPush(bool push)
        {
            bgBtn.Source = (push == true ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
    }
}
