﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ArchiV4
{
    public class Grapher:StackLayout
    {
        List<Entrys> DataEntery = new List<Entrys>();
        public Color LabelColors { get; set; }
        int count = 0;

        double max = 0;

        public Grapher()
        {
            Orientation = StackOrientation.Horizontal;
            HeightRequest = 150+110;
            HorizontalOptions = LayoutOptions.Start;
        }

        public void Initial()
        {
            for (int i=0;i< DataEntery.Count;i++)
            {
                DataEntery[i].SetGraphHeight(((HeightRequest-110) / max) * DataEntery[i].Value);
            }
        }

        public void AddEntry(double value, string label, Color color, EventHandler handler)
        {
            var d = new Entrys
            {
                Value = value,
                Label = label,
                Colors = color,
                Index = count,
            };
            d.valLbl.Text = ""+value;
            d.bar.BackgroundColor = color;
            d.Alpha.BackgroundColor = color;
            if (value>max)
            {
                max = value;
            }

            d.GraphClicked += handler;

            DataEntery.Add(d);

            StackLayout gStack = new StackLayout
            {
                Spacing = 0,
                BackgroundColor = Color.Transparent
            };
            Grid gGrid = new Grid
            {
                RowSpacing = 0,
            };
            gGrid.RowDefinitions.Add(new RowDefinition { Height=50});
            gGrid.RowDefinitions.Add(new RowDefinition { Height = HeightRequest-110 });
            gGrid.RowDefinitions.Add(new RowDefinition { Height=60});
            Label vL = new Label
            {
                Text = ""+value,
                HorizontalOptions = LayoutOptions.Center,
                Rotation = 90,
                FontSize = 12,
                TranslationX = -19//25-7  //(50/2)-(12/2)
            };
            
            Label nL = new Label
            {
                Text = ""+label,
                HorizontalOptions = LayoutOptions.Center,
                Rotation = 90,
                FontSize = 12,
                TranslationX = -19//25-7  //(50/2)-(12/2)
            };
            gGrid.Children.Add(vL, 0, 0);
            gGrid.Children.Add(d, 0, 1);
            gGrid.Children.Add(nL, 0, 2);
            /*gStack.Children.Add(vL);
            gStack.Children.Add(d);
            gStack.Children.Add(nL);*/

            //Children.Add(gStack);
            Children.Add(gGrid);
            count++;
        }

        public void ClearEntry()
        {
            if (DataEntery.Count>0)
            {
                Children.Clear();
                DataEntery.Clear();
            }
            
            count = 0;
            max = 0;
        }
    }

    public class Entrys : Frame
    {
        public double Value { get; set; }
        public double GraphHeight = 0;
        public int Index { get; set; }
        public string Label { get; set; }
        public Color Colors { get; set; }
        public event EventHandler GraphClicked;

        ImageButton button = new ImageButton
        {
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        Grid barLayout = new Grid
        {
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };

        public Frame bar = new Frame
        {
            HasShadow = false,
            Padding = 0,
            Margin = 0,
            CornerRadius = 0,
            InputTransparent = true,
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Fill,
        };
        public Frame Alpha = new Frame
        {
            HasShadow = false,
            Padding = 0,
            Margin = 0,
            Opacity = 0.2,
            CornerRadius = 0,
            InputTransparent = true,
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        public Label valLbl = new Label
        {
            Text = "0",
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Center,
            Rotation = 90,
            InputTransparent = true,
            Margin = new Thickness(0,5,0,0)
        };
        public Entrys()
        {
            HasShadow = false;
            Padding = 0;
            Margin = 0;
            WidthRequest = 50;
            CornerRadius = 0;
            bar.BackgroundColor = Colors;

            VerticalOptions = LayoutOptions.Fill;
            HorizontalOptions = LayoutOptions.Center;
            button.Clicked += Button_Clicked;
            barLayout.Children.Add(Alpha);
            barLayout.Children.Add(button);
            barLayout.Children.Add(bar);
            //barLayout.Children.Add(valLbl);
            Content = barLayout;
        }
        public void SetGraphHeight(double height)
        {
            bar.HeightRequest = height;
        }
        private void Button_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = GraphClicked;
            handler?.Invoke(this, e);
        }
    }
}
