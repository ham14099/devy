﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Startup : ContentPage
    {
        public Startup()
        {
            InitializeComponent();

            In4.In4Init();
            //--- init theme ---
            var filenameSetting = "";
            string datareadSetting = "";
            filenameSetting = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomesetting.txt");
            try
            {
                using (StreamReader readerSetting = new StreamReader(filenameSetting))
                {
                    datareadSetting = readerSetting.ReadToEnd();
                    //DisplayAlert("What in json read",""+datareadSetting,"Close");
                    if (datareadSetting.Trim() == "")
                    {
                        CreateSetting();
                    }
                    Setting setting = JsonConvert.DeserializeObject<Setting>(@datareadSetting);
                    if (setting.settingv != "" + In4.SettingVersion && datareadSetting.Trim() != "")
                    {
                        CreateSetting();
                    }
                    try
                    {
                        Log.Warning("readfile", "readfile");
                        Log.Warning("readfile", "settingv:" + setting.settingv);
                        Log.Warning("readfile", "theme:" + setting.theme);
                        Log.Warning("readfile", "texttheme:" + setting.texttheme);
                        In4.InitSetting(setting);

                    }
                    catch (Exception error)
                    {
                        //DisplayAlert("ErrorSeting",""+"\n==="+error.Message,"Close");
                        Log.Warning("ErrorSeting", "" + error);
                        CreateSetting();
                    }

                    /*if (data.Length > 4)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await Navigation.PushModalAsync(new HomePage());
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await Navigation.PushModalAsync(new Intro());
                        });
                    }*/
                }
            }
            catch (FileNotFoundException e)
            {
                Log.Warning("Error(1X0001)", "Error : " + e);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //await Navigation.PushModalAsync(new Intro());
                    //--- write file ---
                    CreateSetting();
                });
            }
            catch (Exception error)
            {
                Log.Warning("Error(1X0002)", "Error : " + error.Message);
                CreateSetting();
            }
            //--- check txt file config ---
            var filename = "";
            string dataread = "";
            try
            {
                filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                using (var reader = new StreamReader(filename))
                {
                    dataread = reader.ReadToEnd();
                    string[] data = dataread.Split(',');
                    if (data.Length > 4)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //Provider.Client = new IOTClient();

                            /*Log.Warning("main", "lng >4:" + dataread);
                            var res = Provider.Connect(data);
                            Log.Warning("main", "cnct");
                            if (res == true)
                            {
                                Log.Warning("main", "cncted");
                                //Provider.reqFriendInfo();
                                await Navigation.PushModalAsync(new MainPage());
                            }*/

                            await Navigation.PushModalAsync(new MainPage());

                        });
                    }
                    else if (dataread.Trim() == "")
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //DisplayAlert("title", "invalid data", "CLOSE");
                            await Navigation.PushModalAsync(new LoginPage());
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            //DisplayAlert("title", "found but invalid", "CLOSE");
                            await Navigation.PushModalAsync(new LoginPage());
                        });
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Log.Warning("Error(1X0001)", "Error : " + e.Message);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //DisplayAlert("title", "not found", "OK");
                    await Navigation.PushModalAsync(new LoginPage());
                    //await Navigation.PushModalAsync(new HomePage());
                });
            }
            catch (Exception error)
            {
                Log.Warning("Error(1X0002)", "Error : " + error);
            }
        }

        private void CreateSetting()
        {
            //--- write file ---


            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var filenameSetting = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomesetting.txt");

                    string json = @"{'settingv':'" + In4.SettingVersion + "','theme': 0,'texttheme': 1}";

                    Setting setting = JsonConvert.DeserializeObject<Setting>(json);

                    Log.Warning("createfile", "createfile");
                    Log.Warning("createfile", "settingv:" + setting.settingv);
                    Log.Warning("createfile", "theme:" + setting.theme);
                    Log.Warning("createfile", "texttheme:" + setting.texttheme);
                    In4.InitSetting(setting);
                    //--- init to setting page --- SettingMenu.InitSetting(setting);
                    string content = json.ToString();

                    //DisplayAlert("What in json read2", "" + content, "Close");
                    /*using (var streamWriter = new StreamWriter(filenameSetting, true))
                    {
                        streamWriter.WriteLine(content);
                        //File.WriteAllText(filenameSetting, content);
                    }*/
                    System.IO.File.WriteAllText(filenameSetting, content, Encoding.UTF8);
                }
                catch (Exception error)
                {
                    //DisplayAlert("Error Create Setting", "" + error, "Close");
                }
            });


        }
    }
}