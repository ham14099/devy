﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class RoomButton: Frame
    {
        public string Name { get; set; }
        public int Room { get; set; }
        public int DeviceCount { get; set; }
        public int SceneQuantity { get; set; }


        public event EventHandler ButtonClicked;


        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
            CornerRadius = 0
        };
        Image roomIcon = new Image
        {
            Source = "room_64x64.png",
            HeightRequest = In4.SceneOneOfPercent * 25,
            WidthRequest = In4.SceneOneOfPercent * 25,
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(10, 0, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Start,
        };
        StackLayout labelStack = new StackLayout
        {
            BackgroundColor = Color.Transparent,
            Padding = 0,
            Spacing = 0,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            InputTransparent = true,
            Orientation = StackOrientation.Horizontal
        };
        Label deviceCountlbl = new Label
        {
            Text = "0 Devices",
            HorizontalOptions = LayoutOptions.Start,
            InputTransparent = true,
            Margin = new Thickness(10, 0, 0, 0),
            TextColor = Color.FromHex("#0c56e9"),
            FontSize = 14,
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Label lightname = new Label
        {
            Text = "Name",
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(10, 0, 0, 0),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = (Color)Application.Current.Resources["TextItem"],
            Style = (Style)Application.Current.Resources["FontText"],
        };
        public RoomButton() 
        {
            HeightRequest = In4.SceneButtonWidth / 2;
            Padding = 0;
            HasShadow = false;
            CornerRadius = 0;
            if (In4.DevicePlatform == DeviceIdiom.Tablet)
            {
                WidthRequest = In4.ButtonWidth + (In4.ButtonWidth / 3);
                //roomIcon.HeightRequest = In4.SceneButtonWidth / 2.5;
                //roomIcon.WidthRequest = In4.SceneButtonWidth / 2.5;
            }
            else
            {
                WidthRequest = In4.ButtonWidth;
            }
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            //labelStack.Children.Add(deviceCountlbl);
            labelStack.Children.Add(roomIcon);
            labelStack.Children.Add(lightname);
            gridFrm.Children.Add(labelStack);
            Content = gridFrm;
        }
        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            lightname.Text = name;
        }

        public void SetPress(bool press)
        {
            //buttonFrm.BackgroundColor = press == true ? (Color)Application.Current.Resources["Active"] : Color.White;
            lightname.TextColor = press == true ? (Color)Application.Current.Resources["TextItemActive"] : Color.FromHex("#505050");
            buttonFrm.IsEnabled = press==true?false:true;
        }

        public void SetIcon(int icon)
        {
            switch (icon)
            {
                case 1: roomIcon.Source = "bed_64x64.png";break;
                case 2: roomIcon.Source = "chair_64x64.png";break;
                case 3: roomIcon.Source = "working_64x64.png"; break;
                case 5: roomIcon.Source = "meeting_64x64.png"; break;
                case 10: roomIcon.Source = "toilet_64x64.png"; break;
                case 11: roomIcon.Source = "bath_64x64.png"; break;
                case 12: roomIcon.Source = "garage_64x64.png"; break;
                case 13: roomIcon.Source = "balcony_64x64.png"; break;
                case 14: roomIcon.Source = "nursery_64x64.png"; break;
                case 15: roomIcon.Source = "horse_rocking_64x64.png"; break;
                case 16: roomIcon.Source = "kitchen_64x64.png"; break;
                case 17: roomIcon.Source = "game1_64x64.png"; break;
                case 18: roomIcon.Source = "game2_64x64.png"; break;
                case 19: roomIcon.Source = "wine_cellar_64x64.png"; break;
                case 20: roomIcon.Source = "porch_64x64.png"; break;
                case 21: roomIcon.Source = "library_64x64.png"; break;
                case 22: roomIcon.Source = "elevator_64x64.png"; break;
                case 23: roomIcon.Source = "stair_64x64.png"; break;
                case 24: roomIcon.Source = "window_64x64.png"; break;
                case 25: roomIcon.Source = "door_64x64.png"; break;

                case 30: roomIcon.Source = "power_dark64x64.png"; break;
                default:roomIcon.Source = "room_64x64.png";break;
            }            
        }
        
        public void SetSceneCount(int count)
        {
            SceneQuantity = count;           
        }
        public void SetDevicesCount(int count)
        {
            DeviceCount = count;
            deviceCountlbl.Text = count+" Devices";            
        }
    }
}
