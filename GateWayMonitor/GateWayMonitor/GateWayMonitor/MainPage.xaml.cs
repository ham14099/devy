﻿using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using static NucksooTCP.TCPClient;
using DeviceInfo = Xamarin.Essentials.DeviceInfo;

namespace GateWayMonitor
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        int col = 0, row = 0, k = 0, max = 2, ind = 0;
        int init = 0;
        int airCnt = 0;
        int curCnt = 0;
        int dimCnt = 0;
        int lightCnt = 0;
        string[] data;
        static int exitApp = 0;
        int timerConnect = 0;
        int reTimerConnect = 0;
        public MainPage()
        {
            InitializeComponent();
            Provider.Client.OnLogin += Client_OnLogin;
            Provider.Client.OnFriendInformation += Client_OnFriendInformation;
            Provider.Client.OnDeviceControl += Client_OnDeviceControl;
            Provider.Client.OnFriendStatus += Client_OnFriendStatus;
            Provider.Client.OnDisconnect += Client_OnDisconnect;
            Provider.Client.OnConnect += Client_OnConnect;

            if (Device.OS == TargetPlatform.iOS)
            {
                container.Margin = new Thickness(0, 20, 0, 0);
            }

            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
            gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
            if (DeviceInfo.Idiom == DeviceIdiom.Tablet)
            {
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                max = 5;
            }
            //In4.In4Init();
            //--- check txt file config ---
            var filename = "";
            string dataread = "";
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                    using (var reader = new StreamReader(filename))
                    {
                        dataread = reader.ReadToEnd();
                        data = dataread.Split(',');
                        if (data.Length > 4)
                        {
                            //Log.Warning("main","lng >4:"+ dataread);
                            Thread thread = new Thread(() =>
                            {
                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    Log.Warning("main", "pre cnct");
                                    var res = Provider.Connect(data);
                                    Log.Warning("main", "cnct");
                                    if (res == true)
                                    {
                                        Log.Warning("main", "cncted");
                                        //Provider.reqFriendInfo();
                                    }
                                });
                            });
                            thread.Start();
                            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                            {
                                timerConnect++;
                                Log.Warning("timerConnect", "=*=*=*=*=*=" + timerConnect);
                                if (timerConnect < 20)
                                {
                                    Log.Warning("timerConnect", "=" + timerConnect);
                                    //update the count down timer with 1 second here 
                                    return true;
                                }
                                else if (timerConnect == 101)
                                {
                                    Log.Warning("timerConnect", "==============" + timerConnect);
                                    timerConnect = 0;
                                    return false;
                                }
                                else
                                {
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        await DisplayAlert("Connection", "Timeout", "Close");
                                        Log.Warning("timerConnect", "************" + timerConnect);
                                        timerConnect = 0;
                                        //reThread.Abort();
                                        exitApp = 1;
                                        Environment.Exit(0);
                                    });
                                    //DisplayAlert("Connection","Timeout","Close");
                                    //Log.Warning("timerConnect", "************" + timerConnect);
                                    //timerConnect = 0;
                                    //thread.Abort();
                                }
                                return false;
                            });






                        }
                        else if (dataread.Trim() == "")
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                DisplayAlert("title", "invalid data", "CLOSE");
                                await Navigation.PushModalAsync(new LoginPage());
                            });
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                DisplayAlert("title", "found but invalid", "CLOSE");
                                await Navigation.PushModalAsync(new LoginPage());
                            });
                        }
                    }
                });
            }
            catch (FileNotFoundException e)
            {
                Log.Warning("Error(1X0001)", "Error : " + e.Message);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //DisplayAlert("title", "not found", "OK");
                    await Navigation.PushModalAsync(new LoginPage());
                    //await Navigation.PushModalAsync(new HomePage());
                });
            }
            catch (Exception error)
            {
                Log.Warning("Error(1X0002)", "Error : " + error);
            }
        }

        private async void Client_OnConnect(IOTClient client)
        {
            timerConnect = 100;
            reTimerConnect = 100;
            Log.Warning("on connect", "on connecthandler");
            //await DisplayAlert("Event", "OnConnect:" + client.IsConnected, "Close");
            if (!client.IsConnected)
            {
                await DisplayAlert("Event", "cant Connect", "Close");
                //MessagingDialog("Error", "Can't Connect to server", ExitOnHandler);
            }
            else
            {
                indi.IsRunning = false;
                indi.IsVisible = false;
            }
        }

        private async void Client_OnDisconnect(IOTClient client, ConnectionStatusType disconnecType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                Log.Warning("disconnect", "disconnected:" + disconnecType.ToString());
                try
                {
                    indi.IsRunning = true;
                    indi.IsVisible = true;
                    //DisplayAlert("Disconnect", "Disconnected code:" + exitApp + ":" + data[1] + ":" + data[2] + ":" + data[3] + ":" + data[4] + ":", "Ok");
                    if (exitApp == 0)
                    {

                        //--- try to connect
                        /*if (Provider.Client.IsConnected)
                        {
                            Provider.DisConnect();
                        }
                        */
                        if (!Provider.Client.IsConnected)
                        {
                            //var res = Provider.Connect(data);
                            //Log.Warning("connect", "try to connect");
                            //if (res == true)
                            //{
                            //    Log.Warning("connect", "connected again");
                            //    //Provider.reqFriendInfo();

                            //}
                            Thread reThread = new Thread(() =>
                            {
                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    Log.Warning("remain", "pre cnct:" + data[1]);
                                    var res = Provider.Connect(data);
                                    Log.Warning("remain", "cnct");
                                    if (res == true)
                                    {
                                        Log.Warning("remain", "cncted");
                                        //Provider.reqFriendInfo();
                                    }
                                });
                            });
                            reThread.Start();
                            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                            {
                                reTimerConnect++;
                                Log.Warning("retimerConnect", "=*=*=*=*=*=" + reTimerConnect);
                                if (reTimerConnect < 20)
                                {
                                    Log.Warning("retimerConnect", "=" + reTimerConnect);
                                    //update the count down timer with 1 second here 
                                    return true;
                                }
                                else if (reTimerConnect == 101)
                                {
                                    Log.Warning("retimerConnect", "==============" + reTimerConnect);
                                    reTimerConnect = 0;
                                    return false;
                                }
                                else
                                {
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        await DisplayAlert("Connection", "reTimeout", "Close");
                                        Log.Warning("retimerConnect", "************" + reTimerConnect);
                                        reTimerConnect = 0;
                                        //reThread.Abort();
                                        exitApp = 1;
                                        Environment.Exit(0);
                                    });

                                }
                                return false;
                            });
                        }
                        //Log.Warning("main", "lng >4:" + dataread);

                        //var res = Provider.Connect(data);
                        //Log.Warning("connect", "try to connect");
                        //if (res == true)
                        //{
                        //    Log.Warning("connect", "connected again");
                        //    //Provider.reqFriendInfo();

                        //}
                        //DisplayAlert("Disconnect", "Connect again", "Ok");
                    }
                    else
                    {
                        //DisplayAlert("Disconnect", "Disconnected:" + disconnecType.ToString(), "Ok");
                    }
                }
                catch (Exception er)
                {
                    Log.Warning("OnDisconnect error", "" + er);
                }


            });
        }

        private void Client_OnFriendStatus(IOTClient client, FriendStatus friendStatus)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    Provider.Homes[Provider.MEMBER_ID_INDEX[friendStatus.MemberID]].IsOnline(friendStatus.Status == OnlineStatus.Online ? 1 : 0);
                }
                catch (Exception error)
                {
                    Log.Warning("Error [friend status]", "" + error);
                }
            });

        }

        private void Client_OnDeviceControl(IOTClient client, DeviceControlResult deviceControlResult, IOTClient.OnDeviceControlType commandType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    int idx = Provider.MEMBER_ID_INDEX[deviceControlResult.Member];
                    switch (Provider.DEVICE_DEVICEID_TYPE[idx][deviceControlResult.Device])
                    {
                        case 1:
                            try
                            {

                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetStatus((int)deviceControlResult.V);
                                        break;
                                    case 2:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetTemp((int)deviceControlResult.V);
                                        break;
                                    case 3:
                                        Provider.AIR_LIST[idx][Provider.AIR_ID_INDEX[idx][deviceControlResult.Device]].SetSpeed((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                        case 2:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetStatus1((int)deviceControlResult.V);
                                        break;
                                    case 2:
                                        Provider.CURTAIN_LIST[idx][Provider.CURTAIN_ID_INDEX[idx][deviceControlResult.Device]].SetStatus2((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception error)
                            {

                            }
                            break;
                        case 3:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.DIMMER_LIST[idx][Provider.DIM_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.DIMMER_LIST[idx][Provider.DIM_ID_INDEX[idx][deviceControlResult.Device]].SetDim((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception error)
                            {

                            }
                            break;
                        case 4:
                            try
                            {
                                switch (deviceControlResult.Ctrl)
                                {
                                    case 0:
                                        Provider.LIGHT_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetOnline((int)deviceControlResult.V);
                                        break;
                                    case 1:
                                        Provider.LIGHT_LIST[idx][Provider.LIGHT_ID_INDEX[idx][deviceControlResult.Device]].SetStatus((int)deviceControlResult.V);
                                        break;
                                }
                            }
                            catch (Exception erree)
                            {

                            }
                            break;
                    }
                }
                catch (KeyNotFoundException knfEx)
                {
                    Log.Warning("KeyNotFoundException(on control)", "" + knfEx);
                }
                catch (Exception error)
                {
                    Log.Warning("error (on control)", "" + error);

                }

            });
        }
        private void Client_OnFriendInformation(IOTClient client, FriendInformationResult friendInformation)
        {
            Log.Warning("frnd", "");
            try
            {
                var info = (FriendInformation)friendInformation;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (init == 0)//--- init first times ----
                    {
                        try
                        {
                            foreach (var memberKV in info.Member)
                            {
                                airCnt = 0;
                                curCnt = 0;
                                dimCnt = 0;
                                lightCnt = 0;
                                var member = memberKV.Value;
                                //btn.Text = member.Name;
                                Home home = new Home
                                {
                                    Name = member.Name,
                                    MemberID = memberKV.Key,
                                    Status = member.Status == OnlineStatus.Online ? 1 : 0,
                                    //ImageIcon = Convert.ToInt32(member.Img),
                                    ImageIcon = (member.Img == "" ? 1 : Convert.ToInt32(member.Img)),
                                    TabIndex = k
                                };
                                home.SetName(member.Name);
                                home.IsOnline(home.Status);
                                home.SetImageIcon(home.ImageIcon);
                                home.ButtonClicked += async (sender, arg) =>
                                {
                                    //await Navigation.PushModalAsync(new ContentPage());
                                    //await Navigation.PushModalAsync(new RoomPage(home.TabIndex));
                                    //await Navigation.PushModalAsync(new ControllerPage(home.TabIndex));
                                    await Navigation.PushModalAsync(new RoomCategory(home.TabIndex));
                                    //await Navigation.PushModalAsync(Provider.HomeControllers[home.TabIndex]);
                                    //DisplayAlert("Alert", "Add", "OK");
                                };
                                Provider.MEMBER_ID_INDEX.Add(memberKV.Key, k);

                                Dictionary<int, int> DEV_TYPE = new Dictionary<int, int>();

                                List<Room> ROOM = new List<Room>();

                                List<Aircondition> AIR_DEV = new List<Aircondition>();
                                Dictionary<int, int> AIR_IDX = new Dictionary<int, int>();

                                List<Curtain> CUR_DEV = new List<Curtain>();
                                Dictionary<int, int> Cur_IDX = new Dictionary<int, int>();

                                List<Dimmer> DIM_DEV = new List<Dimmer>();
                                Dictionary<int, int> DIM_IDX = new Dictionary<int, int>();

                                List<Light> LIGHT_DEV = new List<Light>();
                                Dictionary<int, int> LIGHT_IDX = new Dictionary<int, int>();

                                foreach (var deviceKV in memberKV.Value.Device)
                                {

                                    DEV_TYPE.Add(deviceKV.Key, deviceKV.Value.DeviceStyleID);
                                    switch (deviceKV.Value.DeviceStyleID)
                                    {
                                        case 1:
                                            Aircondition air = new Aircondition
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };
                                            air.SetName(air.Name);

                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        air.Online = (int)control.Value;
                                                        air.SetOnline(air.Online);
                                                        break;
                                                    case 1:
                                                        air.Status = (int)control.Value;
                                                        break;
                                                    case 2:
                                                        air.Temperature = (int)control.Value;
                                                        air.SetTemp(air.Temperature);
                                                        break;
                                                    case 3:
                                                        air.Speed = (int)control.Value;
                                                        air.SetSpeed(air.Speed);
                                                        break;
                                                    case 4:
                                                        air.TempMin = (int)control.Value;
                                                        break;
                                                    case 5:
                                                        air.TempMax = (int)control.Value;
                                                        break;
                                                    case 100:
                                                        air.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            air.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new AircinditionPopupControl(air));
                                            };
                                            AIR_DEV.Add(air);
                                            AIR_IDX.Add(deviceKV.Key, airCnt);
                                            airCnt++;
                                            break;
                                        case 2:
                                            Curtain curtain = new Curtain
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };
                                            curtain.SetName(curtain.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        curtain.Online = (int)control.Value;
                                                        curtain.SetOnline(curtain.Online);
                                                        break;
                                                    case 1:
                                                        curtain.Status1 = (int)control.Value;
                                                        curtain.SetStatus1(curtain.Status1);
                                                        break;
                                                    case 2:
                                                        curtain.Status2 = (int)control.Value;
                                                        curtain.SetStatus2(curtain.Status2);
                                                        break;
                                                    case 100:
                                                        curtain.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            curtain.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new CurtainPopupControl(curtain));
                                            };
                                            CUR_DEV.Add(curtain);
                                            Cur_IDX.Add(deviceKV.Key, curCnt);
                                            curCnt++;
                                            break;
                                        case 3:
                                            Dimmer dimmer = new Dimmer
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                                MemberID = memberKV.Key,
                                            };

                                            dimmer.SetName(dimmer.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        dimmer.Online = (int)control.Value;
                                                        dimmer.SetOnline(dimmer.Online);
                                                        break;
                                                    case 1:
                                                        dimmer.Dimvalue = (int)control.Value;
                                                        dimmer.SetDim(dimmer.Dimvalue);
                                                        break;
                                                    case 100:
                                                        dimmer.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            dimmer.ButtonClicked += async (obj, arg) =>
                                            {
                                                await PopupNavigation.Instance.PushAsync(new DimmerPopupControl(dimmer));
                                            };
                                            DIM_DEV.Add(dimmer);
                                            DIM_IDX.Add(deviceKV.Key, dimCnt);
                                            dimCnt++;
                                            break;
                                        case 4:
                                            Light light = new Light
                                            {
                                                Name = deviceKV.Value.DeviceName,
                                                DeviceID = deviceKV.Key,
                                            };
                                            light.ButtonClicked += delegate
                                            {
                                                light.Status ^= 1;
                                                light.SetStatus(light.Status);
                                                Provider.SendDeviceControl(home.MemberID, light.DeviceID, 1, light.Status);
                                            };
                                            light.SetName(light.Name);
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                switch (controlKV.Key)
                                                {
                                                    case 0:
                                                        light.Online = (int)control.Value;
                                                        light.SetOnline(light.Online);
                                                        break;
                                                    case 1:
                                                        light.Status = (int)control.Value;
                                                        light.SetStatus(light.Status);
                                                        break;
                                                    case 100:
                                                        light.Room = (int)control.Value;
                                                        break;
                                                }
                                            }
                                            LIGHT_DEV.Add(light);
                                            LIGHT_IDX.Add(deviceKV.Key, lightCnt);
                                            lightCnt++;
                                            break;
                                        case 100:
                                            foreach (var controlKV in deviceKV.Value.Control)
                                            {
                                                var control = controlKV.Value;
                                                ROOM.Add(new Room { RoomID = (int)controlKV.Key, Name = control.Label, RoomIcon = (int)control.Value });
                                            }
                                            break;
                                    }
                                }

                                //--- Add to provider ---
                                Provider.DEVICE_DEVICEID_TYPE.Add(DEV_TYPE);

                                Provider.AIR_LIST.Add(AIR_DEV);
                                Provider.AIR_ID_INDEX.Add(AIR_IDX);

                                Provider.CURTAIN_LIST.Add(CUR_DEV);
                                Provider.CURTAIN_ID_INDEX.Add(Cur_IDX);

                                Provider.DIMMER_LIST.Add(DIM_DEV);
                                Provider.DIM_ID_INDEX.Add(DIM_IDX);

                                Provider.LIGHT_LIST.Add(LIGHT_DEV);
                                Provider.LIGHT_ID_INDEX.Add(LIGHT_IDX);

                                Provider.ROOM_LIST.Add(ROOM);




                                if (k % max == 0 && k > 1)
                                {
                                    row++;
                                    gridLayout.RowDefinitions.Add(new RowDefinition());
                                }

                                if (col == max)
                                {
                                    col = 0;
                                }

                                Provider.Homes.Add(home);
                                gridLayout.Children.Add(home, col, row);
                                k++;
                                col++;

                            }


                            Provider.Analyze();
                        }
                        catch (Exception er)
                        {
                            Log.Warning("errror on friend", "" + er);
                        }
                        init = 1;
                    }
                    else//--- update after offline ---
                    {

                    }



                    //sss.Children.Add(homeController);
                    //Navigation.PushModalAsync(homeController);
                    // Navigation.PushModalAsync(new RoomPage(0));
                    //TabbedPage tabbedPage = new TabbedPage();
                    /*List<StackLayout> sss = new List<StackLayout>
                    {
                        MainContentPage,
                        pp1,
                        pp2
                    };

                    mainCarouselView.ItemsSource = sss;*/

                    //mainFrame.Content = MainContentPage;
                    //tabFrame.Children.Add(tabbedPage);
                });
            }
            catch (Exception error)
            {
                Log.Warning("OnFriendInformation Ex", "" + error);
            }

        }

        private void Client_OnLogin(IOTClient client, LoginResult loginResult)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                //throw new NotImplementedException(); 
                Log.Warning("logon main aaaaa", "");
                if (loginResult.Status == LoginStatus.Success)
                {
                    Provider.reqFriendInfo();


                }
                else
                {
                    var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                    System.IO.File.WriteAllText(filename, "");

                    DisplayAlert("Error", "Username or password incorrect", "Ok");
                    Log.Warning("Error", "Username or password incorrect");
                    await Navigation.PushModalAsync(new LoginPage());
                }
            });



        }

        protected override bool OnBackButtonPressed()
        {
            //Application.Current.Quit();
            exitApp = 1;
            Environment.Exit(0);
            return base.OnBackButtonPressed();
        }



    }
}
