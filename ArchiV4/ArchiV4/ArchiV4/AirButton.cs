﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class AirButton : Frame
    {
        public int Temp { get; set; }
        public int Speed { get; set; }
        public int Status { get; set; }

        public string Name { get; set; }

        public event EventHandler ButtonClicked;

        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.Transparent
        };
        Image bgBtn = new Image
        {
            HeightRequest = In4.SceneButtonWidth,
            WidthRequest = In4.SceneButtonWidth,
            Source = "btnsquare128x128_nin.png",
            Aspect = Aspect.AspectFill,
            InputTransparent = true
        };

        Label temp = new Label
        {
            Text = 25 + "°",
            FontSize = 14,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
            Margin = new Thickness(20, 15, 0, 0),
            InputTransparent = true,
            TextColor = Color.White
        };
        Label speedlbl = new Label
        {
            //Text = ""+ home.Rooms[index].Airconditions[k].Speed,
            FontSize = 14,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
            Margin = new Thickness(20, 30, 0, 0),
            InputTransparent = true,
            TextColor = Color.White
        };


        Label airname = new Label
        {
            Text = "",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 15),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.White
        };

        //PancakeView 

        public AirButton()
        {

            /*buttonFrm.Clicked += async (sender, arg) =>
            {
                buttonsLight[buttonFrm.TabIndex].Source = "btnsquare128x128.png";
            };*/

            SetSpeed(Speed);
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;
            BackgroundColor = Color.Transparent;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            gridFrm.Children.Add(bgBtn);
            gridFrm.Children.Add(temp);
            gridFrm.Children.Add(speedlbl);
            gridFrm.Children.Add(airname);
            Content = gridFrm;

        }

        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            airname.Text = name;

        }
        public void SetStatus(int status)
        {
            Status = status;
            if (status == 0)
            {
                speedlbl.Text = "OFF";
            }
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
        public void SetTemp(int tempVal)
        {
            Temp = tempVal;
            temp.Text = tempVal + "°";
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }

        public void SetSpeed(int speedVal)
        {
            Speed = speedVal;

            if (Speed == 1)
            {
                speedlbl.Text = "LOW";
            }
            else if (Speed == 2)
            {
                speedlbl.Text = "MED";
            }
            else if (Speed == 3)
            {
                speedlbl.Text = "HIGH";
            }
            //speedlbl.Text = speedVal + "%";
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }

        public void IsPush(bool push)
        {
            bgBtn.Source = (push == true ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
    }
}
