﻿using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        IOTClient Client = new IOTClient();
        public LoginPage()
        {
            InitializeComponent();
            Client.OnLogin += Client_OnLogin;
        }


        private void Client_OnLogin(IOTClient client, LoginResult loginResult)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    Log.Warning("logon[login form]", "");
                    if (loginResult.Status == LoginStatus.Success)
                    {

                        var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                        System.IO.File.WriteAllText(filename, "iot,archiiotserver.ddns.net, 20133," + userTxt.Text.ToString() + "," + passTxt.Text.ToString());
                        Log.Warning("(3X0001)", "File Writer");
                        //Navigation.PopModalAsync();
                        //Provider.reqFriendInfo();
                        //Client.Logout();
                        Client.Disconnect();
                        Log.Warning("Yep", "Great");
                        //Provider.Client.Logout();
                        //Provider.DisConnect();

                        //await Navigation.PushModalAsync(new MainPage());
                        //Provider.reqFriendInfo();
                        await Navigation.PushModalAsync(new MainPage());
                        //await Navigation.PopModalAsync();
                        //Provider.Connect(new string[] { "iot", "archiiotserver.ddns.net", 20133 + "", userTxt.Text.ToString(), passTxt.Text.ToString() });
                        //DisplayAlert("Yep", "Great", "Ok");

                    }
                    else if (loginResult.Status != LoginStatus.Success)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            passTxt.Text = string.Empty;
                            
                            Client.Logout();
                            Client.Disconnect();
                            DisplayAlert("Error", "Username or password incorrect!!!", "Ok");
                            Log.Warning("Error", "Username or password incorrect!!!");
                        });
                    }
                }
                catch (Exception error)
                {
                    Log.Warning("Error onlogin[login form]", ""+error);
                }
                
            });
        }

        private async void LoginBtn_Clicked(object sender, EventArgs e)
        {
            //DisplayAlert("Warning", "u:"+(userTxt.Text==null?55:66)+"\np"+(passTxt.Text == null ? 77 : 88), "Ok");
            
            if (userTxt.Text == null)
            {
                DisplayAlert("Warning", "Enter Username", "Ok");
                Log.Warning("Warning", "Enter Username");
            }
            else if (passTxt.Text == null)
            {
                DisplayAlert("Warning", "Enter Password", "Ok");
                Log.Warning("Warning", "Enter Password");
            }
            else
            {
                //Client = new IOTClient();
                Client.Connect("archiiotserver.ddns.net", 20133, userTxt.Text.ToString(), passTxt.Text.ToString() );
                
            }

            // await Task.Run(async () =>
            // {
            /*Log.Warning("error", "Connecting1");
            var res = Provider.Connect(new string[] { "iot", "archiiotserver.ddns.net", 20133 + "", userTxt.Text.ToString(), passTxt.Text.ToString() });
            Log.Warning("error", "Connecting2");
            if (res == true)
            {
                Log.Warning("error", "Connecting3");
                Device.BeginInvokeOnMainThread(async () =>
                {
                    Log.Warning("error", "Connecting4");
                    //--- connected 
                    try
                    {
                        Provider.Client.Disconnect();
                        //scan.IsScanning = false;
                        Log.Warning("error", "Connecting5");
                        var filename = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "smarthomeserver.txt");
                        System.IO.File.WriteAllText(filename, "iot,archiiotserver.ddns.net, 20133," + userTxt.Text + "," + passTxt.Text);

                        Log.Warning("(3X0001)", "File Writer");
                        //await Navigation.PushModalAsync(new MainPage());
                        Navigation.PushModalAsync(new MainPage());
                    }
                    catch (Exception error)
                    {
                        Log.Warning("Error(3X0002)", "Error : " + error);
                    }
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //--- not connect
                    DisplayAlert("res", "failed", "Close");
                    // MessagingDialog("Error", "Scan Again");
                });
            }*/
            //});

        }

        private void forgetBtn_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Forget password", "Please contact Legal entity to reset password.", "Close");
        }

        int signupClick = 0;
        private async void Button_Clicked(object sender, EventArgs e)
        {
            /*if (signupClick==0)
            {
                signupClick = 1;
                await Navigation.PushModalAsync(new SignupPage());
                signupClick = 0;
            }*/

            
            DisplayAlert("Signup", "Please contact Legal entity.", "Close");
        }
    }
}