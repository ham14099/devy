﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignupPage : ContentPage
    {
        public SignupPage()
        {
            InitializeComponent();
            //htmlLbl.Text = "I have read and agreed with the <font color=\"#fd7125\">terms of use and Privacy policy</font>";
        }

        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (nameTxt.Text==null || nameTxt.Text.Trim()=="")
            {
                DisplayAlert("Warning","Enter your name.","Close");
            }
            else if (emailTxt.Text == null || emailTxt.Text.Trim()=="")
            {
                DisplayAlert("Warning","Enter your Email.","Close");
            }
            else if (userTxt.Text == null || userTxt.Text.Trim()=="")
            {
                DisplayAlert("Warning","Enter username.","Close");
            }
            else if (passTxt.Text == null || passTxt.Text.Trim()=="")
            {
                DisplayAlert("Warning","Enter password.","Close");
            }
            else if (passTxt.Text.Length < 6)
            {
                DisplayAlert("Warning", "Password must be at least 6 characters.", "Close");
            }
            else if (confirmpassTxt.Text == null || confirmpassTxt.Text.Trim()=="")
            {
                DisplayAlert("Warning","Confirm pasword.","Close");
            }
            else if (confirmpassTxt.Text.Length < 6)
            {
                DisplayAlert("Warning", "Password must be at least 6 characters.", "Close");
            }
            else if (!checkBox.IsChecked)
            {
                DisplayAlert("Warning","Please agree terms of use and privacy policy to continue.","Close");
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Uri uri = new Uri("https://archismarthome.com/policy.php#policy");
            OpenBrowser(uri);
        }
        public async void OpenBrowser(Uri uri)
        {
            //await Browser.OpenAsync(uri);
            await Browser.OpenAsync(uri, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.AliceBlue,
                PreferredControlColor = Color.DarkBlue
            });
        }
    }
}