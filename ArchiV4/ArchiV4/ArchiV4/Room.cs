﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ArchiV4
{
    public class Room
    {
        public int RoomID { get; set; }
        public string Name { get; set; }
        public int RoomIcon { get; set; }


        public int AirCount { get; set; }
        public int CurtainCount { get; set; }
        public int DimmerCount { get; set; }
        public int LightCoutn { get; set; }
        public int DevicesCount { get; set; }
        public int SceneQuantity { get; set; }


        public Room()
        {

        }
    }
}
