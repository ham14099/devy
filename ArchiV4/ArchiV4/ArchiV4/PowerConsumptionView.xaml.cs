﻿using Microcharts;
using Newtonsoft.Json;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.ChartEntry;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PowerConsumptionView : ContentView
    {
        int i = 0;
        readonly List<Color> Colors = new List<Color>
        {
            Color.FromRgb(255,0,0),
            Color.FromRgb(255,90,0),
            Color.FromRgb(255,213,0),
            Color.FromRgb(99,255,0),
            Color.FromRgb(0,255,0),
            Color.FromRgb(0,184,255),
            Color.FromRgb(0, 0, 255),
            Color.FromRgb(96,0,168),
            Color.FromRgb(180, 0, 80),
            Color.FromRgb(255,0,77),
            Color.FromRgb(255,0,0),
            Color.FromRgb(255,90,0),
            Color.FromRgb(255,213,0),
            Color.FromRgb(99,255,0),
            Color.FromRgb(0,255,0),
            Color.FromRgb(0,184,255),
            Color.FromRgb(0, 0, 255),
            Color.FromRgb(96,0,168),
            Color.FromRgb(180, 0, 80),
            Color.FromRgb(255,0,77),
            Color.FromRgb(255,0,0),
            Color.FromRgb(255,90,0),
            Color.FromRgb(255,213,0),
            Color.FromRgb(99,255,0),
            Color.FromRgb(0,255,0),
            Color.FromRgb(0,184,255),
            Color.FromRgb(0, 0, 255),
            Color.FromRgb(96,0,168),
            Color.FromRgb(180, 0, 80),
            Color.FromRgb(255,0,77),
            Color.FromHex("#ff00ff"),
            Color.FromHex("#7fffd4"),
            Color.FromHex("#b12000"),
            Color.FromHex("#abd5d9"),
            Color.FromHex("#fabab5"),
            Color.FromHex("#fae39c"),
            Color.FromHex("#5ea833"),
            Color.FromHex("#279289"),
            Color.FromHex("#416284"),
            Color.FromHex("#f09a52"),
            Color.FromHex("#f25f3a"),
            Color.FromHex("#419474"),

            Color.FromHex("#ff00ff"),
            Color.FromHex("#7fffd4"),
            Color.FromHex("#b12000"),
            Color.FromHex("#abd5d9"),
            Color.FromHex("#fabab5"),
            Color.FromHex("#fae39c"),
            Color.FromHex("#5ea833"),
            Color.FromHex("#279289"),
            Color.FromHex("#416284"),
            Color.FromHex("#f09a52"),
            Color.FromHex("#f25f3a"),
            Color.FromHex("#419474"),
        };

        int hourStartIndex = 10;


        //readonly List<ChartEntry> powerHour = new List<ChartEntry>();
        List<ChartEntry> powerHour = new List<ChartEntry>();
        List<ChartEntry> powerDay = new List<ChartEntry>();
        List<ChartEntry> powerMonth = new List<ChartEntry>();
        Power powers = new Power();

        List<VisualElement> tabContents = new List<VisualElement>();

        Grapher grapher = new Grapher()
        {
            LabelColors = Color.FromHex("#606060")
        };
        Grapher DayGrapher = new Grapher()
        {
            LabelColors = Color.FromHex("#606060")
        };
        Grapher HourGrapher = new Grapher()
        {
            LabelColors = Color.FromHex("#606060")
        };

        PowerAttibute jsonDeserialized;
        int selectionIndex = 0;

        public PowerConsumptionView(int homeIndex)
        {
            InitializeComponent();
            tabContents.Add(MonthGraphLayout);
            tabContents.Add(DayGraphLayout);
            tabContents.Add(HourGraphLayout);


            /*for (i=0;i<24;i++)
            {
                powerHour[i].Color = SKColor.Parse(Colors[i].ToHex());
                powerHour[i].Label = i + "";
            }
            for (i=0;i<12;i++)
            {
                powerChart[i].Color = SKColor.Parse(Colors[i].ToHex());
                powerChart[i].Label = (i+1) + "";
            }*/

            try
            {
                if (Provider.POWER_LIST[homeIndex].Count > 0)
                {
                    powers = Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]];
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].OnlineHandler += PowerConsumption_OnlineHandler;
                    powers.VoltageHandler += Powers_VoltageHandler;
                    powers.CurrentHandler += Powers_CurrentHandler;
                    powers.WattHandler += Powers_WattHandler;
                    powers.EnergyHandler += PowerConsumption_EnergyHandler;
                    powers.FrequencyHandler += PowerConsumption_FrequencyHandler;
                    powers.PowerFactorHandler += Powers_PowerFactorHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].FrequencyHandler += PowerConsumption_FrequencyHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].VoltageHandler += Powers_VoltageHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].CurrentHandler += Powers_CurrentHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].WattHandler += Powers_WattHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].EnergyHandler += PowerConsumption_EnergyHandler;
                    //Provider.POWER_LIST[homeIndex][Provider.POWER_ID_INDEX[homeIndex][7000]].FrequencyHandler += PowerConsumption_FrequencyHandler;
                    voltageLbl.Text = powers.Voltage == 0 ? "0" : powers.Voltage.ToString("N1") + "";
                    currentLbl.Text = powers.Current == 0 ? "0" : powers.Current.ToString("N2") + "";
                    wattLbl.Text = powers.Watt == 0 ? "0" : powers.Watt.ToString("N2") + "";
                    energyLbl.Text = powers.Energy == 0 ? "0" : powers.Energy.ToString("N2") + "";
                    pfLbl.Text = powers.PowerFactor == 0 ? "0" : powers.PowerFactor.ToString("N2") + "";
                    fqLbl.Text = powers.Frequency == 0 ? "0" : powers.Frequency.ToString("N2") + "";
                }
            }
            catch (Exception err)
            {
                Log.Warning("error", "" + err);
            }

            try
            {
                string html = "";
                string url = @"https://archismarthome.com/api/power/power.php?m=" + Provider.HomeControllers[homeIndex].MemberID;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();

                }
                Log.Warning("http", html);

                //List<PowerConsumptions> power = JsonConvert.DeserializeObject<List<PowerConsumptions>>(@html);
                if (html != "not found")
                {
                    PowerAttibute myDeserializedClass = JsonConvert.DeserializeObject<PowerAttibute>(@html);

                    jsonDeserialized = myDeserializedClass;


                    for (int att = 0; att < myDeserializedClass.Detail.Count; att++)
                    {
                        Log.Warning("Year", "" + myDeserializedClass.Detail[att].Year);
                        Log.Warning("Month", "" + myDeserializedClass.Detail[att].Month);
                        Log.Warning("MonthUnit", "" + myDeserializedClass.Detail[att].MonthUnit);
                        /*powerMonth.Add(new Entry((float)myDeserializedClass.Detail[att].MonthUnit)
                        {
                            Label = ToMonth(myDeserializedClass.Detail[att].Month) + " " + (myDeserializedClass.Detail[att].Year-2000),
                            ValueLabel = myDeserializedClass.Detail[att].MonthUnit + "",
                            Color = SKColor.Parse(Colors[att].ToHex()),
                            TextColor = SKColor.Parse(Colors[att].ToHex())
                        });*/

                        grapher.AddEntry(myDeserializedClass.Detail[att].MonthUnit,
                            ToMonth(myDeserializedClass.Detail[att].Month) + " " + (myDeserializedClass.Detail[att].Year - 2000),
                            Colors[att],
                           graph_Clicked);


                        /*try
                        {
                            for (int dd = 0; dd < myDeserializedClass.Detail[att].PowerDay.Count; dd++)
                            {

                                Log.Warning("Day", "" + myDeserializedClass.Detail[att].PowerDay[dd].Day);
                                Log.Warning("DayUnit", "" + myDeserializedClass.Detail[att].PowerDay[dd].DayUnit);
                                powerDay.Add(new Entry((float)myDeserializedClass.Detail[att].PowerDay[dd].DayUnit)
                                {
                                    Label = myDeserializedClass.Detail[att].PowerDay[dd].Day + "",
                                    ValueLabel = myDeserializedClass.Detail[att].PowerDay[dd].DayUnit + "",
                                    Color = SKColor.Parse(Colors[dd].ToHex()),
                                    TextColor = SKColor.Parse(Colors[dd].ToHex())
                                });
                                DayGrapher.AddEntry(myDeserializedClass.Detail[att].PowerDay[dd].DayUnit,
                                    myDeserializedClass.Detail[att].PowerDay[dd].Day+"",
                                    Colors[dd],
                                   DayGraph_Clicked);
                                try
                                {
                                    for (int hh = 0; hh < myDeserializedClass.Detail[att].PowerDay[dd].PowerHour.Count; hh++)
                                    {
                                        Log.Warning("Hour", "" + myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].Hour);
                                        Log.Warning("HourUnit", "" + myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].HourUnit);
                                        
                                        powerHour.Add(new Entry((float)myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].HourUnit) { 
                                            Label = myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].Hour+"", 
                                            ValueLabel = myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].HourUnit + "", 
                                            Color = SKColor.Parse(Colors[hh].ToHex()),
                                            TextColor = SKColor.Parse(Colors[hh].ToHex())
                                        });
                                        HourGrapher.AddEntry(myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].HourUnit,
                                            myDeserializedClass.Detail[att].PowerDay[dd].PowerHour[hh].Hour + "",
                                            Colors[hh],
                                           HourGraph_Clicked);

                                    }
                                }
                                catch (Exception errh)
                                {
                                    //Log.Warning("Error Hour", "" + errh);
                                }

                            }
                        }
                        catch (Exception errd)
                        {
                            //Log.Warning("Error Day", "" + errd);
                        }*/

                    }
                }

                /*if (power.Count > 0)
                {
                    for (i = 0; i < power.Count; i++)
                    {
                        powerChart[i]=new Entry(power[i].Unit) { Label = ToMonth(power[i].Month) + " " + power[i].Year + "   ", ValueLabel = power[i].Unit + "", Color = SKColor.Parse(Colors[i].ToHex()) };

                    }                    
                }*/
            }
            catch (Exception error)
            {
                Log.Warning("Error http req", "" + error);
            }

            /*chartsMonth.Chart = new BarChart
            {
                Entries = powerDay,
                LabelTextSize = 30,
                ValueLabelOrientation = Orientation.Vertical,
                LabelOrientation = Orientation.Vertical,
                LabelColor = SKColor.Parse("#494949"),

            };*/


            /*for (i = 0; i < 24; i++)
            {
                //int ran = new Random().Next(0, 10);
                Log.Warning("powerHour energy", "" + powers.EnergyTime[i]);
                powerHour[i] = new Entry((float)powers.EnergyTime[i]) { Label = i + "", ValueLabel = (float)powers.EnergyTime[i] + "", Color = SKColor.Parse(Colors[i].ToHex()), TextColor = SKColor.Parse(Colors[23 - i].ToHex()) };
                
            }*/

            /*chartsHour.Chart = new LineChart
            {
                Entries = powerHour,
                LabelTextSize = 30,
                //ValueLabelOrientation = Orientation.Vertical, 
                //LabelOrientation = Orientation.Vertical,
                LabelColor = SKColor.Parse("#606060"),
                IsAnimated = false,
                BackgroundColor = SKColor.Parse(Color.Transparent.ToHex()),
                //LineMode = LineMode.Spline,
                //PointMode = PointMode.Circle
            };*/

            grapher.Initial();
            graphScroll.Content = grapher;

            //DayGrapher.Initial();
            //dayGraphScroll.Content = DayGrapher;

            //HourGrapher.Initial();
            //hourGraphScroll.Content = HourGrapher;
        }

        private async Task ShowSelection(int newTab)
        {
            // navigate the selection pill
            //var selectdTabLabel = tabHeaders[newTab];
            //_ = SelectionUnderline.TranslateTo(selectdTabLabel.Bounds.X, 0, 150, easing: Easing.SinInOut);

            // update the style of the header to show it's selcted
            //var unselectedStyle = (Style)Application.Current.Resources["TabLabel"];
            //var selectedStyle = (Style)Application.Current.Resources["SelectedTabLabel"];
            //tabHeaders[selectionIndex].Style = unselectedStyle;
            //selectdTabLabel.Style = selectedStyle;

            /// reveal the contents
            /// 

            /*mBtn.BackgroundColor = newTab == 0 ? Color.FromHex("#295DEB") : Color.White;
            mBtn.TextColor = newTab == 0 ? Color.White : Color.FromHex("#606060");

            dBtn.BackgroundColor = newTab == 1 ? Color.FromHex("#295DEB") : Color.White;
            dBtn.TextColor = newTab == 1 ? Color.White : Color.FromHex("#606060");

            hBtn.BackgroundColor = newTab == 2 ? Color.FromHex("#295DEB") : Color.White;
            hBtn.TextColor = newTab == 2 ? Color.White : Color.FromHex("#606060");
            */


            await tabContents[selectionIndex].FadeTo(0);
            tabContents[selectionIndex].IsVisible = false;
            //tabContents[selectionIndex].Opacity = 0;
            //tabContents[newTab].Opacity = 1;
            tabContents[newTab].IsVisible = true;
            _ = tabContents[newTab].FadeTo(1);

            selectionIndex = newTab;

        }

        private void PowerConsumption_EnergyHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                energyLbl.Text = e == 0 ? "0" : e.ToString("N2") + "";
            });

        }

        private void PowerConsumption_FrequencyHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                fqLbl.Text = e == 0 ? "0" : e.ToString("N2") + "";
            });
        }


        private void Powers_PowerFactorHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                pfLbl.Text = e == 0 ? "0" : e.ToString("N2") + "";
            });
        }

        private void Powers_WattHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                wattLbl.Text = e == 0 ? "0" : e.ToString("N2") + "";
            });
        }

        private void Powers_CurrentHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                currentLbl.Text = e == 0 ? "0" : e.ToString("N2") + "";
            });
        }

        private void Powers_VoltageHandler(object sender, double e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                voltageLbl.Text = e == 0 ? "0" : e.ToString("N1") + "";
            });
        }

        private string ToMonth(int month)
        {
            string mn = "";
            switch (month)
            {
                case 1: mn = "Jan"; break;
                case 2: mn = "Feb"; break;
                case 3: mn = "Mar"; break;
                case 4: mn = "Apr"; break;
                case 5: mn = "May"; break;
                case 6: mn = "Jun"; break;
                case 7: mn = "Jul"; break;
                case 8: mn = "Aug"; break;
                case 9: mn = "Sep"; break;
                case 10: mn = "Oct"; break;
                case 11: mn = "Nov"; break;
                case 12: mn = "Dec"; break;
            }
            return mn;
        }

        private async void backBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
        int monthIndex = 0;
        int dayIndex = 0;
        private async void graph_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PopModalAsync();
            try
            {

                var gp = sender as Entrys;
                monthIndex = gp.Index;
                //DisplayAlert(""+gp.Value,"" + gp.Index, "Ok");
                DayGrapher.ClearEntry();
                int dcount = 0;
                try
                {
                    dcount = jsonDeserialized.Detail[gp.Index].PowerDay.Count;
                }
                catch (Exception error)
                {

                }
                dateLbl.Text = "Daily Usage (" + ToMonth(jsonDeserialized.Detail[gp.Index].Month) + " " + jsonDeserialized.Detail[gp.Index].Year + ")";
                if (dcount > 0)
                {
                    for (int dd = 0; dd < dcount; dd++)
                    {

                        Log.Warning("Day", "" + jsonDeserialized.Detail[gp.Index].PowerDay[dd].Day);
                        Log.Warning("DayUnit", "" + jsonDeserialized.Detail[gp.Index].PowerDay[dd].DayUnit);

                        DayGrapher.AddEntry(jsonDeserialized.Detail[gp.Index].PowerDay[dd].DayUnit,
                            jsonDeserialized.Detail[gp.Index].PowerDay[dd].Day + "",
                            Colors[dd],
                           DayGraph_Clicked);
                    }

                    DayGrapher.Initial();
                    dayGraphScroll.Content = DayGrapher;
                    await ShowSelection(1);
                }
                else
                {
                    //DisplayAlert("Warning", "No data.", "Close");
                }
            }
            catch (Exception error)
            {
                Log.Warning("Error Expand Day", "" + error);
            }


        }
        private async void DayGraph_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PopModalAsync();
            try
            {

                var gp = sender as Entrys;
                dayIndex = gp.Index;
                //HourGrapher.ClearEntry();
                if (powerHour.Count > 0)
                {
                    powerHour.Clear();
                }

                //DisplayAlert(""+gp.Value,"" + gp.Index, "Ok");
                int hcount = 0;
                try
                {
                    hcount = jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour.Count;
                }
                catch (Exception error)
                {

                }

                dateHLbl.Text = "Hourly Usage (" + jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].Day + " " + ToMonth(jsonDeserialized.Detail[monthIndex].Month) + " " + jsonDeserialized.Detail[monthIndex].Year + ")";

                if (hcount > 0)
                {
                    for (int hh = 0; hh < hcount; hh++)
                    {
                        Log.Warning("Hour", "" + jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].Hour);
                        Log.Warning("HourUnit", "" + jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].HourUnit);

                        powerHour.Add(new Entry((float)jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].HourUnit)
                        {
                            Label = jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].Hour + "",
                            ValueLabel = jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].HourUnit + "",
                            Color = SKColor.Parse(Colors[hh].ToHex()),
                            TextColor = SKColor.Parse(Colors[hh].ToHex())
                        });
                        /*HourGrapher.AddEntry(jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].HourUnit,
                            jsonDeserialized.Detail[monthIndex].PowerDay[dayIndex].PowerHour[hh].Hour + "",
                            Colors[hh],
                           HourGraph_Clicked);*/

                    }
                    //HourGrapher.Initial();
                    //hourGraphScroll.Content = HourGrapher;
                    chartsHour.WidthRequest = hcount * 50;
                    chartsHour.Chart = new LineChart
                    {
                        Entries = powerHour,
                        LabelTextSize = 30,
                        //ValueLabelOrientation = Orientation.Vertical, 
                        //LabelOrientation = Orientation.Vertical,
                        LabelColor = SKColor.Parse("#606060"),
                        IsAnimated = false,
                        BackgroundColor = SKColor.Parse(Color.Transparent.ToHex()),
                        //LineMode = LineMode.Spline,
                        //PointMode = PointMode.Circle
                    };

                    await ShowSelection(2);
                }
                else
                {
                    //DisplayAlert("Warning", "No data.", "Close");
                }
            }
            catch (Exception error)
            {
                Log.Warning("Error Expand Hour", "" + error);
            }


        }
        private async void HourGraph_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PopModalAsync();
            var gp = sender as Entrys;
            //DisplayAlert("" + gp.Value, "" + gp.Index, "Ok");
        }

        private async void mBtn_Clicked(object sender, EventArgs e)
        {
            //gLbl.Text = "M";

            /*mBtn.BackgroundColor = Color.FromHex("#295DEB");
            mBtn.TextColor = Color.White;

            dBtn.BackgroundColor = Color.White;
            dBtn.TextColor = Color.FromHex("#606060");

            hBtn.BackgroundColor = Color.White;
            hBtn.TextColor = Color.FromHex("#606060");*/

            //dBtn.IsEnabled = false;
            if (selectionIndex > 0)
            {
                await ShowSelection(0);
            }

        }

        private async void dBtn_Clicked(object sender, EventArgs e)
        {
            //gLbl.Text = "D";

            /*dBtn.BackgroundColor = Color.FromHex("#295DEB");
            dBtn.TextColor = Color.White;

            mBtn.BackgroundColor = Color.White;
            mBtn.TextColor = Color.FromHex("#606060");

            hBtn.BackgroundColor = Color.White;
            hBtn.TextColor = Color.FromHex("#606060");*/
            if (selectionIndex > 1)
            {
                await ShowSelection(1);
            }
        }

        private async void hBtn_Clicked(object sender, EventArgs e)
        {
            //gLbl.Text = "H";

            /*hBtn.BackgroundColor = Color.FromHex("#295DEB");
            hBtn.TextColor = Color.White;

            dBtn.BackgroundColor = Color.White;
            dBtn.TextColor = Color.FromHex("#606060");

            mBtn.BackgroundColor = Color.White;
            mBtn.TextColor = Color.FromHex("#606060");*/

            //await ShowSelection(2);
        }

        private async void dayBackBtn_Clicked(object sender, EventArgs e)
        {
            await ShowSelection(0);
        }

        private async void hourBackBtn_Clicked(object sender, EventArgs e)
        {
            await ShowSelection(1);
        }
    }
}