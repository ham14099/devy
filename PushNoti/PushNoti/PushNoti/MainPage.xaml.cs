﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using DeviceInfo = Xamarin.Essentials.DeviceInfo;

namespace PushNoti
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        [Obsolete]
        public MainPage()
        {
            InitializeComponent();
            if (Device.OS == TargetPlatform.Android)
            {
                OneSignal.Current.StartInit("f1e00f21-fb66-428f-917d-0d7ac8a31280")._notificationReceivedDelegate += aaa;
            }
            else if(Device.OS == TargetPlatform.iOS)
            {
                OneSignal.Current.StartInit("6f7464f6-383f-40ac-8b6b-8d877c9f5279")._notificationReceivedDelegate += aaa;
            }
            
            
            try
            {
                var res = OneSignal.Current.GetTriggerValueForKey("aaa");
                Log.Warning("aaa=", "" + res);
                
            }
            catch (Exception error)
            {
                Log.Warning("Error", "" + error);
            }
            //string deviceIdentifier = DependencyService.Get<IDevice>().GetIdentifier();
            //Log.Warning("IdenID=",""+ deviceIdentifier);
            /*Log.Warning("DeviceInfo.Manufacturer=", ""+ DeviceInfo.Manufacturer);
            Log.Warning("DeviceInfo.OS=", ""+ Device.OS);
            Log.Warning("DeviceInfo.Model=", ""+ DeviceInfo.Model);
            Log.Warning("DeviceInfo.Name=", ""+ DeviceInfo.Name);
            Log.Warning("DeviceInfo.Platform=", ""+ DeviceInfo.Platform);
            Log.Warning("DeviceInfo.DeviceType=", ""+ DeviceInfo.DeviceType);
            Log.Warning("DeviceInfo.Idiom=", ""+ DeviceInfo.Idiom);
            Log.Warning("DeviceInfo.VersionString=", ""+ DeviceInfo.VersionString);
            Log.Warning("DeviceInfo.Version=", ""+ DeviceInfo.Version);*/

                
        }

        private void aaa(OSNotification notification)
        {
            try
            {
                Log.Warning("notification=", "" + notification.payload.additionalData["aaa"]);
            }
            catch (KeyNotFoundException keyError)
            {

            }
            catch (Exception error)
            {

            }
            
        }
    }
}
