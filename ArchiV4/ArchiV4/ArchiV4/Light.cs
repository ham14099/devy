﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace ArchiV4
{
    public class Light: PancakeView
    {
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Room { get; set; }

        public event EventHandler ButtonClicked;

        /*public event EventHandler<int> OnlineHandler;
        public void OnOnlineHandler()
        {
            try
            {
                //Console.WriteLine("EventHandler Air[Status]");
                OnOnline(Online);
            }
            catch (Exception ex)
            {
                OnOnline(Online);
            }
        }
        protected virtual void OnOnline(int status)
        {
            OnlineHandler?.Invoke(this, status);
        }*/
        public void SetOnline(int online)
        {
            Online = online;
            buttonFrm.BackgroundColor = Online == 1 ? Color.White : (Color)Application.Current.Resources["Active"];
            lightname.TextColor = Online == 1 ? Color.FromHex("#505050") : (Color)Application.Current.Resources["TextItemActive"];
            buttonFrm.IsEnabled = Online == 1 ? true : false;
            //OnOnlineHandler();
        }
        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        Image lightStatus = new Image
        {
            Source = "bulb_off64x64.png",
            HeightRequest = In4.SceneOneOfPercent * 35,
            WidthRequest = In4.SceneOneOfPercent * 35,
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(10, 10, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.Start,
        };

        Label statuslbl = new Label
        {
            Text = "OFF",
            VerticalOptions = LayoutOptions.Start,
            HorizontalOptions = LayoutOptions.End,
            InputTransparent = true,
            Margin = new Thickness(0, 15, 15, 0),
            TextColor = Color.FromHex("#ff0037"),
            FontSize = 18,
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Label lightname = new Label
        {
            Text = "Name",
            VerticalOptions = LayoutOptions.End,
            HorizontalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(0, 0, 0, 5),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = Color.FromHex("#494949"),
            Style = (Style)Application.Current.Resources["FontText"],
        };
        public Light()
        {
            HeightRequest = In4.SceneButtonWidth;
            WidthRequest = In4.SceneButtonWidth;
            Padding = 0;
            CornerRadius = 5;

            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            gridFrm.Children.Add(lightStatus);
            gridFrm.Children.Add(statuslbl);
            gridFrm.Children.Add(lightname);
            Content = gridFrm;
        }
        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            lightname.Text = name;

        }
        public void SetStatus(int status)
        {
            Status = status;
            statuslbl.Text = (status==1?"ON":"OFF");
            //statuslbl.TextColor = (status == 1 ? Color.FromHex("#05e1a4") : Color.FromHex("#ff0037"));
            statuslbl.TextColor = (status == 1 ? Color.FromHex("#39B742") : Color.FromHex("#FF9A36"));
            lightStatus.Source = status == 1 ? "bulb_on64x64.png" : "bulb_off64x64.png";
            //buttonFrm.BackgroundColor = status == 1 ? Color.White:Color.FromHex("#E8D9C4");
            //bgBtn.Source = (status == 1 ? "btnsquare128x128.png" : "btnsquare128x128_nin.png");
        }
    }
}
