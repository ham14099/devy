﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Markup;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoomCategory : ContentPage
    {
        int col = 0, row = 0, k = 0, max = 3, ind = 0, i = 0;
        List<RoomButton> RoomButtonList = new List<RoomButton>();
        private int PressIndex = 0;
        private int HomeIndex;
        //--- for tab bar ---
        int selectionIndex = 0;
        List<RoomButton> tabHeaders = new List<RoomButton>();
        List<VisualElement> tabContents = new List<VisualElement>();
        //--- End tab bar ---
        public RoomCategory(int homeIndex)
        {
            InitializeComponent();
            HomeIndex = homeIndex;
            
            Provider.Homes[homeIndex].OnlineHandler += RoomCategory_OnlineHandler;
            Provider.Homes[homeIndex].PowerOnlineHandler += RoomCategory_PowerOnlineHandler;
            Provider.Homes[homeIndex].DNDMURChangeHandler += RoomCategory_DNDMURChangeHandler;

            dndmurG.IsVisible = Provider.Homes[homeIndex].DNDMURVisible;
            switch (Provider.Homes[homeIndex].DNDMUR)
            {
                case 0:
                    dndmurImg.Source = "bell.png";
                    break;
                case 1:
                    dndmurImg.Source = "bell_disable.png";
                    break;
                case 2:
                    dndmurImg.Source = "mur_enable.png";
                    break;
            }


            try
            {
                SelectionUnderline.WidthRequest = In4.SceneButtonWidth;
                //--- for tab bar ---
                for (i = 0; i < Provider.ROOM_LIST[homeIndex].Count; i++)
                {
                    Provider.HomeControllers[homeIndex].roomLayouts[i].IsVisible = false;
                    theLayout.Children.Add(Provider.HomeControllers[homeIndex].roomLayouts[i]);
                    tabContents.Add(Provider.HomeControllers[homeIndex].roomLayouts[i]);
                }
                /*
                tabContents.Add(CastContent);
                tabContents.Add(NewsContent);
                tabContents.Add(CriticsContent);
                tabContents.Add(MediaContent);*/
                //--- End tab bar ---

                if (Device.OS == TargetPlatform.iOS)
                {
                    container.Margin = new Thickness(0, 20, 0, 0);
                }

                homeName.Text = Provider.HomeControllers[homeIndex].Name;
                //powerConG.IsVisible = Provider.Homes[homeIndex].PowerConsump;
                waitText.Text = "Wait " + Provider.HomeControllers[homeIndex].Name + " connecting...";

                
                /*gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                if (In4.DevicePlatform == DeviceIdiom.Tablet)
                {
                    gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                    gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
                    max = 5;
                }*/
                int k = 0;
                for (i = 0; i < Provider.ROOM_LIST[homeIndex].Count; i++)
                {
                    /*if (k % max == 0 && k > 1)
                    {
                        row++;
                        gridLayout.RowDefinitions.Add(new RowDefinition());
                    }

                    if (col == max)
                    {
                        col = 0;
                    }*/

                    //Button roomButton = new Button
                    //{
                    //    Text = Provider.ROOM_LIST[homeIndex][i].Name,
                    //    TabIndex = i
                    //};
                    RoomButton roomButton = new RoomButton
                    {
                        TabIndex = i,
                        Name = Provider.ROOM_LIST[homeIndex][i].Name,
                    };
                    if (i == 0)
                    {
                        roomButton.SetPress(true);
                    }
                    roomButton.SetName(roomButton.Name);
                    roomButton.SetIcon(Provider.ROOM_LIST[homeIndex][i].RoomIcon);
                    roomButton.SetDevicesCount(Provider.ROOM_LIST[homeIndex][i].DevicesCount);
                    roomButton.SetSceneCount(Provider.ROOM_LIST[homeIndex][i].SceneQuantity);

                    roomButton.ButtonClicked += async (sender, arg) =>
                    {
                        Room_ButtonClicked(roomButton.TabIndex);
                    };
                    /*roomButton.ButtonClicked += async (sender, arg) =>
                    {

                        //await Navigation.PushModalAsync(new ControllerPage(homeIndex, roomButton.TabIndex));
                        controlFrame.Content = Provider.HomeControllers[homeIndex].roomLayouts[roomButton.TabIndex];
                        checkButtonPress(roomButton.TabIndex);
                    };*/

                    //gridLayout.Children.Add(roomButton, col, row);
                    if (roomButton.DeviceCount > 0||roomButton.SceneQuantity > 0)
                    {
                        // gdLayout.ColumnDefinitions.Add(new ColumnDefinition { Width = In4.SceneButtonWidth });
                        gdLayout.Children.Add(roomButton, k, 0);
                        //gdLayout.Children.Add(roomButton);
                        k++;
                        //sss.Children.Add(roomButton);
                    }

                    tabHeaders.Add(roomButton);

                    //RoomButtonList.Add(roomButton);
                    //k++;
                    //col++;

                }
                //--- If powerconsumption is available ---
                RoomButton bttn = new RoomButton
                {
                    TabIndex = i,
                };
                bttn.SetName("power");
                bttn.SetIcon(30);
                bttn.SetDevicesCount(1);
                bttn.SetSceneCount(0);

                bttn.ButtonClicked += async (sender, arg) =>
                {
                    Room_ButtonClicked(bttn.TabIndex);
                };
                tabHeaders.Add(bttn);
                gdLayout.Children.Add(bttn,k,0);
                k++;
                PowerConsumptionView pcv = new PowerConsumptionView(homeIndex);
                theLayout.Children.Add(pcv);
                tabContents.Add(pcv);
                //-------------------------------------------------------------------------

                //barr.SetValue(Grid.ColumnSpanProperty,k);
                SelectionUnderline.SetValue(Grid.ColumnSpanProperty, k);
                if (Provider.ROOM_LIST[homeIndex].Count > 0)
                {
                    Device.BeginInvokeOnMainThread(async () => {
                        await ShowSelection(0);
                    });

                    //controlFrame.Content = Provider.HomeControllers[homeIndex].roomLayouts[0];
                }
            }
            catch (Exception error)
            {
                Log.Warning("Error roommmmmmm",""+error);
            }
            //DisplayAlert("headertop.Height", ""+ headertop.Height, "ok");
            
        }

        private void RoomCategory_DNDMURChangeHandler(object sender, int e)
        {
            switch (e)
            {
                case 0:
                    dndmurImg.Source = "bell.png";
                    break;
                case 1:
                    dndmurImg.Source = "bell_disable.png";
                    break;
                case 2:
                    dndmurImg.Source = "mur_enable.png";
                    break;
            }

        }



        private void RoomCategory_PowerOnlineHandler(object sender, bool e)
        {
            //powerConG.IsEnabled = e;
        }

        private async void RoomButton_ButtonClicked(object sender, EventArgs e)
        {
            //var tabIndex = tabHeaders.IndexOf((RoomButton)sender);
            var tabIndex = (sender as RoomButton).TabIndex;

            checkButtonPress(tabIndex);
            await ShowSelection(tabIndex);
        }

        private async void powerConBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PowerConsumption(HomeIndex));
        }

        private async void dndmurBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new DNDMURPopup(HomeIndex));
        }

        private async void Room_ButtonClicked(int index)
        {
            //var tabIndex = tabHeaders.IndexOf((RoomButton)sender);
            var tabIndex = index;

            checkButtonPress(tabIndex);
            await ShowSelection(tabIndex);
        }

        private async void RoomCategory_OnlineHandler(object sender, int e)
        {
            indiLayout.IsVisible = e == 0?true:false;
            theLayout.IsEnabled = e == 0?false:true;
            //sss.IsEnabled = e == 0? false : true;
            if (PopupNavigation.Instance.PopupStack.Count > 0 && e == 0)
            {
                await PopupNavigation.Instance.PopAllAsync();
            }
            
        }

        private void checkButtonPress(int btnIndex)
        {
            /*for (int i=0;i< RoomButtonList.Count;i++)
            {
                if (RoomButtonList[i].BackgroundColor!= Color.White)
                {
                    
                }
                RoomButtonList[PressIndex].SetPress(false);
            }*/
            tabHeaders[PressIndex].SetPress(false);
            PressIndex = btnIndex;
            tabHeaders[btnIndex].SetPress(true);
        }
        private void backBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async Task ShowSelection(int newTab)
        {
            // navigate the selection pill
            var selectdTabLabel = tabHeaders[newTab];
            _ = SelectionUnderline.TranslateTo(selectdTabLabel.Bounds.X, 0, 150, easing: Easing.SinInOut);

            // update the style of the header to show it's selcted
            //var unselectedStyle = (Style)Application.Current.Resources["TabLabel"];
            //var selectedStyle = (Style)Application.Current.Resources["SelectedTabLabel"];
            //tabHeaders[selectionIndex].Style = unselectedStyle;
            //selectdTabLabel.Style = selectedStyle;

            /// reveal the contents
            await tabContents[selectionIndex].FadeTo(0);
            tabContents[selectionIndex].IsVisible = false;
            //tabContents[selectionIndex].Opacity = 0;
            //tabContents[newTab].Opacity = 1;
            tabContents[newTab].IsVisible = true;
            _ = tabContents[newTab].FadeTo(1);

            selectionIndex = newTab;

        }

    }
}