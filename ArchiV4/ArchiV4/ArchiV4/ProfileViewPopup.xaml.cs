﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileViewPopup : PopupPage
    {

        int selectionIndex = 0;

        List<Button> tabHeaders = new List<Button>();
        List<VisualElement> tabContents = new List<VisualElement>();


        public ProfileViewPopup()
        {
            InitializeComponent();
            memberID.Text = "Member ID : "+Provider.MyMemberID;
            memberName.Text = "Name : "+Provider.MyMemberName;

            /*tabHeaders.Add(btn1);
            tabHeaders.Add(btn2);
            tabHeaders.Add(btn3);
            tabHeaders.Add(btn4);
            tabHeaders.Add(btn5);


            tabContents.Add(InfoContent);
            tabContents.Add(CastContent);
            tabContents.Add(NewsContent);
            tabContents.Add(CriticsContent);
            tabContents.Add(MediaContent);*/
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
        /*private async Task ShowSelection(int newTab)
        {
            if (newTab == selectionIndex) return;

            // navigate the selection pill
            var selectdTabLabel = tabHeaders[newTab];
            _ = SelectionUnderline.TranslateTo(selectdTabLabel.Bounds.X, 0, 150, easing: Easing.SinInOut);

            // update the style of the header to show it's selcted
            var unselectedStyle = (Style)Application.Current.Resources["TabLabel"];
            var selectedStyle = (Style)Application.Current.Resources["SelectedTabLabel"];
            tabHeaders[selectionIndex].Style = unselectedStyle;
            selectdTabLabel.Style = selectedStyle;

            /// reveal the contents
            await tabContents[selectionIndex].FadeTo(0);
            tabContents[selectionIndex].IsVisible = false;
            //tabContents[selectionIndex].Opacity = 0;
            //tabContents[newTab].Opacity = 1;
            tabContents[newTab].IsVisible = true;
            _ = tabContents[newTab].FadeTo(1);

            selectionIndex = newTab;

        }


        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var tabIndex = tabHeaders.IndexOf((Button)sender);
            await ShowSelection(tabIndex);
        }


        private async void btn_Clicked(object sender, EventArgs e)
        {
            var tabIndex = tabHeaders.IndexOf((Button)sender);
            await ShowSelection(tabIndex);
        }*/
    }
}