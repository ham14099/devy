﻿using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;
using static NucksooTCP.TCPClient;

namespace GateWayMonitor
{
    public static class Provider
    {
        public static IOTClient Client = new IOTClient();

        public static List<Home> Homes = new List<Home>();

        public static int timerConnect = 0;

        public static Dictionary<int, int> MEMBER_ID_INDEX = new Dictionary<int, int>();
        public static List<Dictionary<int, int>> DEVICE_DEVICEID_TYPE = new List<Dictionary<int, int>>();
        public static List<List<Room>> ROOM_LIST = new List<List<Room>>();

        public static List<HomeControllerContent> HomeControllers = new List<HomeControllerContent>();

        // Home/Device
        //     - id
        //     - index
        //     - type
        //     - room
        //     - member
        public static List<Dictionary<int, int>> AIR_ID_INDEX = new List<Dictionary<int, int>>();
        public static List<Dictionary<int, int>> CURTAIN_ID_INDEX = new List<Dictionary<int, int>>();
        public static List<Dictionary<int, int>> DIM_ID_INDEX = new List<Dictionary<int, int>>();
        public static List<Dictionary<int, int>> LIGHT_ID_INDEX = new List<Dictionary<int, int>>();

        public static List<List<Aircondition>> AIR_LIST = new List<List<Aircondition>>();
        public static List<List<Curtain>> CURTAIN_LIST = new List<List<Curtain>>();
        public static List<List<Dimmer>> DIMMER_LIST = new List<List<Dimmer>>();
        public static List<List<Light>> LIGHT_LIST = new List<List<Light>>();

        //public static List<Dictionary<int, int>> AIR_ID_INDEX = new List<Dictionary<int, int>>();

        public static bool Connect(string[] connection)
        {

            Log.Warning("provider connect", "call connect");
            try
            {

                //Client.TcpClient.ClientSocket.ReceiveTimeout = 10000;
                //Client.TcpClient.ClientSocket.SendTimeout = 10000;
                var res = Client.Connect(connection[1], Convert.ToInt32(connection[2]), connection[3], connection[4]);
                if (res.Status == ConnectionStatusType.Connected)
                {
                    Log.Warning("provider connect", "connect");
                    return true;

                }
                else
                {
                    Log.Warning("provider connect", "no connect");
                    Client.Disconnect();
                    return false;
                }
            }
            catch (Exception error)
            {
                Log.Warning("connect [provider]", "" + error);
                return false;
            }
        }

        public static void reqFriendInfo()
        {
            var rawDF = NucksooIOTCommon.Service.GenTCPParam(NucksooIOTCommon.Service.CommandEnum.GetFriendInformation, "");
            Client.Send(rawDF);
        }

        public static void DisConnect()
        {
            Client.Disconnect();

        }

        public static void SendDeviceControl(int memberID, int deviceID, int controlID, double val)
        {
            Client.SendDeviceControl(new DeviceControlParam { Member = memberID, Device = (short)deviceID, Ctrl = (short)controlID, V = val });
        }

        public static void Analyze()
        {//-----------------------------------------------------
         //--- add device to room
            /*
                    room name
             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******


             */

            //------------------------------------------------------
            /*RoomLayout roomLayout = new RoomLayout
            {
                Name = "Bedroom",
            };
            roomLayout.SetName(roomLayout.Name);
            roomLayout.AddAircondition(AIR_LIST[0][0]);
            roomLayout.AddAircondition(AIR_LIST[0][1]);
            roomLayout.AddAircondition(AIR_LIST[0][2]);

            roomLayout.AddCurtain(CURTAIN_LIST[0][0]);

            roomLayout.AddDimmer(DIMMER_LIST[0][0]);

            roomLayout.AddLight(LIGHT_LIST[0][0]);
            roomLayout.AddLight(LIGHT_LIST[0][1]);
            roomLayout.AddLight(LIGHT_LIST[0][2]);
            roomLayout.AddLight(LIGHT_LIST[0][3]);
            roomLayout.AddLight(LIGHT_LIST[0][4]);
            //-------------------------------
            RoomLayout roomLayout2 = new RoomLayout
            {
                Name = "Living",
            };
            roomLayout2.SetName(roomLayout2.Name);
            roomLayout2.AddAircondition(AIR_LIST[1][2]);

            roomLayout2.AddCurtain(CURTAIN_LIST[1][0]);

            roomLayout2.AddLight(LIGHT_LIST[1][3]);
            roomLayout2.AddLight(LIGHT_LIST[1][4]);
            */
            //--- add room to HomeControllerContent

            /*
                   home anme

                   room1 name
             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

                   room2 name
             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

                   room3 name
             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******

             ****** ****** ******
             *    * *    * *    *
             *    * *    * *    *
             ****** ****** ******



            */
            /*HomeControllerContent homeController = new HomeControllerContent
            {
                Name = "NottingHamSmith"
            };
            homeController.SetName(homeController.Name);
            homeController.AddRoom(roomLayout);
            homeController.AddRoom(roomLayout2);*/

            for (int i = 0; i < Homes.Count; i++)
            {
                //--- new home obj
                HomeControllerContent homeCtrl = new HomeControllerContent(Homes[i])
                {
                    Name = Homes[i].Name,
                    MemberID = Homes[i].MemberID,
                    Online = Homes[i].Status,
                };
                homeCtrl.SetName(homeCtrl.Name);

                //Log.Warning("h",""+i);
                //--- Air ---
                for (int r = 0; r < ROOM_LIST[i].Count; r++)
                {
                    RoomLayout room = new RoomLayout
                    {
                        Name = ROOM_LIST[i][r].Name,
                        Icon = ROOM_LIST[i][r].RoomIcon
                    };
                    room.SetName(room.Name);

                    for (int a = 0; a < AIR_LIST[i].Count; a++)
                    {
                        if (AIR_LIST[i][a].Room == ROOM_LIST[i][r].RoomID)
                        {
                            //--- add devices 
                            room.AddAircondition(AIR_LIST[i][a]);
                            room.DevicesQuantity++;
                            //Homes[i].Rooms[r].Airconditions.Add(Homes[i].Airconditions[a]);
                            //Log.Warning("Match", "============================");
                        }
                    }
                    for (int a = 0; a < CURTAIN_LIST[i].Count; a++)
                    {
                        if (CURTAIN_LIST[i][a].Room == ROOM_LIST[i][r].RoomID)
                        {
                            //--- add devices 
                            room.AddCurtain(CURTAIN_LIST[i][a]);

                            room.DevicesQuantity++;

                            //Homes[i].Rooms[r].Airconditions.Add(Homes[i].Airconditions[a]);
                            //Log.Warning("Match", "============================");
                        }
                    }
                    for (int a = 0; a < DIMMER_LIST[i].Count; a++)
                    {
                        if (DIMMER_LIST[i][a].Room == ROOM_LIST[i][r].RoomID)
                        {
                            //--- add devices 
                            room.AddDimmer(DIMMER_LIST[i][a]);

                            room.DevicesQuantity++;

                            //Homes[i].Rooms[r].Airconditions.Add(Homes[i].Airconditions[a]);
                            //Log.Warning("Match", "============================");
                        }
                    }
                    for (int a = 0; a < LIGHT_LIST[i].Count; a++)
                    {
                        if (LIGHT_LIST[i][a].Room == ROOM_LIST[i][r].RoomID)
                        {
                            //--- add devices 
                            room.AddLight(LIGHT_LIST[i][a]);

                            room.DevicesQuantity++;

                            //Homes[i].Rooms[r].Airconditions.Add(Homes[i].Airconditions[a]);
                            //Log.Warning("Match", "============================");
                        }
                    }

                    ROOM_LIST[i][r].DevicesCount = room.DevicesQuantity;
                    //--- room has device more than 1
                    //if (room.DevicesQuantity>0)
                    //{
                    //    homeCtrl.AddRoom(room);
                    //}
                    homeCtrl.AddRoom(room);
                }

                HomeControllers.Add(homeCtrl);
            }
        }
    }
}
