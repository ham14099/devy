﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using NucksooIOTCommon;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace PushNoti
{
    public partial class App : Application
    {
        IOTClient Client = new IOTClient();
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();

            //Remove this method to stop OneSignal Debugging  
            //OneSignal.Current.SetLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
            OneSignal.Current.StartInit("f1e00f21-fb66-428f-917d-0d7ac8a31280")
            .InFocusDisplaying(OSInFocusDisplayOption.Notification)
            .Settings(new Dictionary<string, bool>() {
    { IOSSettings.kOSSettingsKeyAutoPrompt, false },
    { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } })
            .EndInit();
            
            //OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

            // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 7)
            OneSignal.Current.RegisterForPushNotifications();
            OneSignal.Current.StartInit("f1e00f21-fb66-428f-917d-0d7ac8a31280")._notificationReceivedDelegate += aaa;
        }

        private void aaa(OSNotification notification)
        {
            Log.Warning("noti=",""+ notification.payload.additionalData["aaa"]);
            //Client.Connect("archiiotserver.ddns.net", 20133, "ham", "123456");
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
