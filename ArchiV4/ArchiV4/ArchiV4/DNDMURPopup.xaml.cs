﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ArchiV4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DNDMURPopup : PopupPage
    {
        int INDEX = 0;
        int dnd_state = 0;
        int mur_state = 0;
        public DNDMURPopup(int index)
        {
            InitializeComponent();
            INDEX = index;
            switch (Provider.Homes[INDEX].DNDMUR)
            {
                case 0:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_disable.png";
                    dnd_state = 0;
                    mur_state = 0;
                    break;
                case 1:
                    bellImg.Source = "bell_disable.png";
                    murImg.Source = "mur_disable.png";
                    dnd_state = 1;
                    mur_state = 0;
                    break;
                case 2:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_enable.png";
                    dnd_state = 0;
                    mur_state = 1;
                    break;
            }
            Provider.Homes[INDEX].DNDMURChangeHandler += DNDMURPopup_DNDMURChangeHandler;
        }

        private void DNDMURPopup_DNDMURChangeHandler(object sender, int e)
        {
            switch (e)
            {
                case 0:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_disable.png"; 
                    dnd_state = 0; 
                    mur_state = 0;
                    break;
                case 1:
                    bellImg.Source = "bell_disable.png";
                    murImg.Source = "mur_disable.png"; 
                    dnd_state = 1;
                    mur_state = 0;
                    break;
                case 2:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_enable.png";
                    dnd_state = 0;
                    mur_state = 1;
                    break;
            }
        }

        void SetButton(int i)
        {
            switch (i)
            {
                case 0:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_disable.png";
                    dnd_state = 0;
                    mur_state = 0;
                    break;
                case 1:
                    bellImg.Source = "bell_disable.png";
                    murImg.Source = "mur_disable.png";
                    dnd_state = 1;
                    mur_state = 0;
                    break;
                case 2:
                    bellImg.Source = "bell_enable.png";
                    murImg.Source = "mur_enable.png";
                    dnd_state = 0;
                    mur_state = 1;
                    break;
            }
            Provider.SendDeviceControl(Provider.Homes[INDEX].MemberID, 6002, 1,i);
        }

        private async void closeBtn_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private void bellBtn_Clicked(object sender, EventArgs e)
        {
            if (dnd_state==0)
            {
                SetButton(1);
            }
            else if (dnd_state == 1)
            {
                SetButton(0);
            }

        }

        private void murBtn_Clicked(object sender, EventArgs e)
        {
            if (mur_state == 0)
            {
                SetButton(2);
            }
            else if (mur_state == 1)
            {
                SetButton(0);
            }
        }
    }
}