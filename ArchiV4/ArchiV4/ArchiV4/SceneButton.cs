﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ArchiV4
{
    public class SceneButton:Frame
    {
        public int DeviceID { get; set; }
        public int Online { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int SceneID { get; set; }
        public int Type { get; set; }

        public int Room { get; set; }

        public event EventHandler ButtonClicked;


        Grid gridFrm = new Grid
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
        };
        ImageButton buttonFrm = new ImageButton
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            BackgroundColor = Color.White,
        };
        Image sceneIcon = new Image
        {
            Source = "room_64x64.png",
            HeightRequest = In4.SceneOneOfPercent * 20,
            WidthRequest = In4.SceneOneOfPercent * 20,
            Aspect = Aspect.AspectFit,
            InputTransparent = true,
            Margin = new Thickness(10, 0, 0, 0),
            BackgroundColor = Color.Transparent,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Start,
        };
        StackLayout labelStack = new StackLayout
        {
            BackgroundColor = Color.Transparent,
            Padding = 0,
            Spacing = 0,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            InputTransparent = true,
            Orientation = StackOrientation.Horizontal
        };
        Label deviceCountlbl = new Label
        {
            Text = "0 Devices",
            HorizontalOptions = LayoutOptions.Start,
            InputTransparent = true,
            Margin = new Thickness(10, 0, 0, 0),
            TextColor = Color.FromHex("#0c56e9"),
            FontSize = 14,
            Style = (Style)Application.Current.Resources["FontText"],
        };

        Label sceneName = new Label
        {
            Text = "Name",
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            InputTransparent = true,
            MaxLines = 1,
            FontSize = 14,
            Margin = new Thickness(10, 0, 0, 0),
            LineBreakMode = LineBreakMode.TailTruncation,
            TextColor = (Color)Application.Current.Resources["TextItem"],
            Style = (Style)Application.Current.Resources["FontText"],
        };

        


        public SceneButton()
        {
            HeightRequest = In4.SceneButtonWidth/2;
            WidthRequest = 150;
            Padding = 0;
            HasShadow = false;
            CornerRadius = 5;
            VerticalOptions = LayoutOptions.Center;
            Margin = new Thickness(10,0,0,0);
            /*if (In4.DevicePlatform == DeviceIdiom.Tablet)
            {
                sceneIcon.HeightRequest = In4.SceneButtonWidth / 2.5;
                sceneIcon.WidthRequest = In4.SceneButtonWidth / 2.5;
            }*/
            //BorderColor = Color.FromHex("#11376d");
            //BorderThickness = 1;
            BackgroundColor = Color.White;
            buttonFrm.Clicked += ButtonFrm_Clicked;
            gridFrm.Children.Add(buttonFrm);
            //gridFrm.Children.Add(sceneIcon);
            //labelStack.Children.Add(deviceCountlbl);
            labelStack.Children.Add(sceneIcon);
            labelStack.Children.Add(sceneName);
            gridFrm.Children.Add(labelStack);
            Content = gridFrm;
        }
        private void ButtonFrm_Clicked(object sender, EventArgs e)
        {
            EventHandler handler = ButtonClicked;
            handler?.Invoke(this, e);
        }

        public void SetName(string name)
        {
            sceneName.Text = name;
        }

        public void SetPress(bool press)
        {
            buttonFrm.BackgroundColor = press == true ? (Color)Application.Current.Resources["Active"] : Color.White;
            sceneName.TextColor = press == true ? (Color)Application.Current.Resources["TextItemActive"] : Color.FromHex("#505050");
        }

        public void SetIcon(int icon)
        {
            switch (icon)
            {
                case 1: sceneIcon.Source = "home_64x64.png"; break;
                case 2: sceneIcon.Source = "hotel_64x64.png"; break;
                case 3: sceneIcon.Source = "bed_64x64.png"; break;
                case 4: sceneIcon.Source = "chair_64x64.png"; break;
                case 5: sceneIcon.Source = "meeting_64x64.png"; break;
                case 6: sceneIcon.Source = "working_64x64.png"; break;
                case 7: sceneIcon.Source = "party_64x64.png"; break;
                default: sceneIcon.Source = "room_64x64.png"; break;
            }

        }

        public void SetEnable(bool enable)
        {
            IsVisible = enable;
        }
    }
}
